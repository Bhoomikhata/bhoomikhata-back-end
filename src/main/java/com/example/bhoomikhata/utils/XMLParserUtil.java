package com.example.bhoomikhata.utils;

import com.example.bhoomikhata.model.LatLngModel;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class XMLParserUtil {

    public static List<LatLngModel> getLatLongForAreaHighlight(InputStream kmlFileName, String location) {
        List<LatLngModel> latLngs = new ArrayList<>();

        // Instantiate the Factory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        try {
            // parse XML file
            DocumentBuilder db = dbf.newDocumentBuilder();

            Document doc = db.parse(kmlFileName);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("Placemark");
            for (int index = 0; index < nodeList.getLength(); index++) {
                String locationName = nodeList.item(index).getChildNodes().item(7).getChildNodes().item(0)
                        .getChildNodes().item(11).getTextContent();
                locationName = locationName.replaceAll("\\s+", "").toUpperCase();
                location = location.replaceAll("\\s+", "").toUpperCase();
                if (locationName.contains(location)) {
                    NodeList nodeList1 = nodeList.item(index).getChildNodes().item(9).getChildNodes();
                    for (int i = 0; i < nodeList1.getLength(); i++) {
                        String[] latLongString = nodeList1.item(i).getTextContent().split(" ");
                        for (String s : latLongString) {
                            String[] latLngArr = s.split(",");
                            LatLngModel latLng = new LatLngModel(Double.parseDouble(latLngArr[1].trim()),
                                    Double.parseDouble(latLngArr[0].trim()));
                            latLngs.add(latLng);
                        }
                    }
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return latLngs;
    }
}
