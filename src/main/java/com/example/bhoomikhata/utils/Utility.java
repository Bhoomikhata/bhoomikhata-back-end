package com.example.bhoomikhata.utils;

import java.time.OffsetDateTime;

import com.example.bhoomikhata.model.BussinesModel;
import com.example.bhoomikhata.model.ContactModel;
import com.example.bhoomikhata.model.IdentityDocModel;
import com.example.bhoomikhata.model.OfferModel;
import com.example.bhoomikhata.model.PaymentModeModel;
import com.example.bhoomikhata.model.TransactionModel;

public class Utility {

    private Utility() {
    }

    public static BussinesModel setCreatedAndUpdatedDate(BussinesModel bussinesModel) {
        bussinesModel.setCreatedDate(OffsetDateTime.now());
        bussinesModel.setUpdatedDate(OffsetDateTime.now());
        return bussinesModel;
    }

    public static ContactModel setCreatedAndUpdatedDate(ContactModel contactModel) {
        contactModel.setCreatedDate(OffsetDateTime.now());
        contactModel.setUpdatedDate(OffsetDateTime.now());
        return contactModel;
    }

    public static TransactionModel setCreatedAndUpdatedDate(TransactionModel transactionModel) {
        transactionModel.setCreatedDate(OffsetDateTime.now());
        transactionModel.setUpdatedDate(OffsetDateTime.now());
        return transactionModel;
    }

    public static IdentityDocModel setCreatedAndUpdatedDate(IdentityDocModel identityDocModel) {
        identityDocModel.setCreatedDate(OffsetDateTime.now());
        identityDocModel.setUpdatedDate(OffsetDateTime.now());
        return identityDocModel;
    }

    public static PaymentModeModel setCreatedAndUpdatedDate(PaymentModeModel paymentModeModel) {
        paymentModeModel.setCreatedDate(OffsetDateTime.now());
        paymentModeModel.setUpdatedDate(OffsetDateTime.now());
        return paymentModeModel;
    }

    public static Float calculateNumberOfCoinsOrCashback(Float formula, Integer transactionAmount) {
        return formula * transactionAmount;
    }

}
