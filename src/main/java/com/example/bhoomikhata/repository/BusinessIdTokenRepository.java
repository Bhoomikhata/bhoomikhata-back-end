package com.example.bhoomikhata.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.bhoomikhata.model.BusinessIdTokenModel;

@Repository
public interface BusinessIdTokenRepository extends MongoRepository<BusinessIdTokenModel, String> {

}
