package com.example.bhoomikhata.repository;

import java.util.List;

import com.example.bhoomikhata.model.UserOfferModel;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserOfferRepository extends MongoRepository<UserOfferModel, String> {

}
