package com.example.bhoomikhata.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.bhoomikhata.model.OrderModel;

public interface OrderRepository extends MongoRepository<OrderModel, String> {
	
	List<OrderModel> findByBussinessId(String bussinessId);
	List<OrderModel> findByCustomerId(String customerId);

}
