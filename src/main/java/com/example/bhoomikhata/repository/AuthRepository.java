package com.example.bhoomikhata.repository;

import com.example.bhoomikhata.model.AuthModel;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface AuthRepository extends MongoRepository<AuthModel, String> {

}
