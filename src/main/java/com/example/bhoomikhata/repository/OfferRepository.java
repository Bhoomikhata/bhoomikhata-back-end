package com.example.bhoomikhata.repository;

import java.util.List;

import com.example.bhoomikhata.model.OfferModel;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OfferRepository extends MongoRepository<OfferModel, String> {

}
