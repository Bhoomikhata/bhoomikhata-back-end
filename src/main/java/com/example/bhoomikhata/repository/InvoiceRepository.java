package com.example.bhoomikhata.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.bhoomikhata.model.InvoiceModel;

public interface InvoiceRepository extends MongoRepository<InvoiceModel, String> {

}
