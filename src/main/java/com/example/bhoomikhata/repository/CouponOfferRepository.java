package com.example.bhoomikhata.repository;

import com.example.bhoomikhata.model.CouponOffers;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface CouponOfferRepository extends MongoRepository<CouponOffers, String> {

}
