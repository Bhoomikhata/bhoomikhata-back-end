package com.example.bhoomikhata.repository;

import com.example.bhoomikhata.model.CoinOffers;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface CoinOfferRepository extends MongoRepository<CoinOffers, String> {

}
