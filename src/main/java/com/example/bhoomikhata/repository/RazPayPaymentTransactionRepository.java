package com.example.bhoomikhata.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.bhoomikhata.model.RazPayTransactionModelResponse;

@Repository
public interface RazPayPaymentTransactionRepository extends MongoRepository<RazPayTransactionModelResponse, String> {
	List<RazPayTransactionModelResponse> findByBusinessId(String businessId);
}
