package com.example.bhoomikhata.repository;

import java.util.List;

import com.example.bhoomikhata.model.ContactModel;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ContactRepository extends MongoRepository<ContactModel, String> {

    public List<ContactModel> findByMobileNumber(String mobileNumber);

    List<ContactModel> findByBussinessId(String bussinessId);
}
