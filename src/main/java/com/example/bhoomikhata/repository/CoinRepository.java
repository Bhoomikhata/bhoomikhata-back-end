package com.example.bhoomikhata.repository;

import java.util.List;

import com.example.bhoomikhata.model.CoinModel;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface CoinRepository extends MongoRepository<CoinModel, String> {
    List<CoinModel> findByContactId(String contactId);

    List<CoinModel> findByBussinessId(String bussinessId);

    List<CoinModel> deleteByContactId(String contactId);

    List<CoinModel> deleteByBussinessId(String bussinessId);
}
