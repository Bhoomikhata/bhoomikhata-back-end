package com.example.bhoomikhata.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.bhoomikhata.model.FilesStorageModel;

@Repository
public interface FilesStorageRepository extends MongoRepository<FilesStorageModel, String> {
	List<FilesStorageModel> findByIsForSupport(Boolean isForSupport);
}
