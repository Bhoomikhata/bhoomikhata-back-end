package com.example.bhoomikhata.repository;

import com.example.bhoomikhata.model.CashbackOffers;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface CashbackOfferRepository extends MongoRepository<CashbackOffers, String> {

}
