package com.example.bhoomikhata.repository;

import java.util.List;

import com.example.bhoomikhata.model.CashbackModel;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface CashbackRepository extends MongoRepository<CashbackModel, String> {
    List<CashbackModel> findByContactId(String contactId);

    List<CashbackModel> findByBussinessId(String bussinessId);

    List<CashbackModel> deleteByContactId(String contactId);

    List<CashbackModel> deleteByBussinessId(String bussinessId);
}
