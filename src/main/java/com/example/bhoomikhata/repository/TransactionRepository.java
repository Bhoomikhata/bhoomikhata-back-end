package com.example.bhoomikhata.repository;

import com.example.bhoomikhata.model.TransactionModel;

import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;

public interface TransactionRepository extends MongoRepository<TransactionModel, String> {
    public List<TransactionModel> findByPayeeId(String payeeId);

    public List<TransactionModel> findByPayerId(String payerId);

    List<TransactionModel> findByBussinessId(String bussinessId);

    List<TransactionModel> findByContactId(String contactId);
}
