package com.example.bhoomikhata.repository;

import java.util.List;

import com.example.bhoomikhata.model.CouponModel;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface CouponRepository extends MongoRepository<CouponModel, String> {
    List<CouponModel> findByContactId(String contactId);

    List<CouponModel> findByBussinessId(String bussinessId);

    List<CouponModel> deleteByContactId(String contactId);

    List<CouponModel> deleteByBussinessId(String bussinessId);
}
