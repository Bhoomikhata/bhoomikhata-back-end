package com.example.bhoomikhata.repository;

import com.example.bhoomikhata.model.BussinesModel;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProfileRepository extends MongoRepository<BussinesModel, String> {

}
