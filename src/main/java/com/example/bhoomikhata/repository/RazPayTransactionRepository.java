package com.example.bhoomikhata.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.bhoomikhata.model.RazPayTransactionModel;

@Repository
public interface RazPayTransactionRepository extends MongoRepository<RazPayTransactionModel, String> {
	RazPayTransactionModel findByRazorPayOrderId(String razorPayOrderId);
}
