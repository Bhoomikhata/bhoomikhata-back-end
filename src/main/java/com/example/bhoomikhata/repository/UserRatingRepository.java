package com.example.bhoomikhata.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.bhoomikhata.model.UserRatingModel;

@Repository
public interface UserRatingRepository extends MongoRepository<UserRatingModel, String> {
	List<UserRatingModel> findByRatedTobusinessId(String ratedTobusinessId);
}
