package com.example.bhoomikhata.repository;

import com.example.bhoomikhata.model.OTPModel;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface RegistrationRepository extends MongoRepository<OTPModel, String> {
    public OTPModel findByMobileNumber(String mobileNumber);
}
