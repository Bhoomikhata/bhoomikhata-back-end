package com.example.bhoomikhata.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.bhoomikhata.model.RazPayOrderModel;

public interface RazPayOrderRepository extends MongoRepository<RazPayOrderModel, String> {

}
