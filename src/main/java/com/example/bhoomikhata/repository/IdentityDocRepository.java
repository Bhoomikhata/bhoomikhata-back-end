package com.example.bhoomikhata.repository;

import java.util.List;

import com.example.bhoomikhata.model.IdentityDocModel;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface IdentityDocRepository extends MongoRepository<IdentityDocModel, String> {
    List<IdentityDocModel> findByBussinessId(String bussinessId);
}
