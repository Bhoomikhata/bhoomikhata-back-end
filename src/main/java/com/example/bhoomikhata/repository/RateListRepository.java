package com.example.bhoomikhata.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.bhoomikhata.model.RateListModel;

public interface RateListRepository extends MongoRepository<RateListModel, String> {
	
	List<RateListModel> findByBusinessId(String businessId);

}
