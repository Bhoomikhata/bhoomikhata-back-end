package com.example.bhoomikhata.repository;

import java.util.List;

import com.example.bhoomikhata.model.PaymentModeModel;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface PaymentAccountRepository extends MongoRepository<PaymentModeModel, String> {
    List<PaymentModeModel> findByBussinessId(String bussinessId);
}
