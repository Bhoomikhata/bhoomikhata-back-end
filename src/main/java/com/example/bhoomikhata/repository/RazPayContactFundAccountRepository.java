package com.example.bhoomikhata.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.bhoomikhata.model.RazPayContactFundAccount;

@Repository
public interface RazPayContactFundAccountRepository extends MongoRepository<RazPayContactFundAccount, String> {
	Optional<RazPayContactFundAccount> findByMobileNumber(String mobileNumber);
}
