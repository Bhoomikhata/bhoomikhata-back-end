package com.example.bhoomikhata.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.bhoomikhata.model.InvoiceAndPOModel;

public interface InvoiceAndPORepository extends MongoRepository<InvoiceAndPOModel, String> {

}
