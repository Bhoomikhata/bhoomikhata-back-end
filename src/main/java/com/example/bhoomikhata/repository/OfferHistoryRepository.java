package com.example.bhoomikhata.repository;

import java.util.List;

import com.example.bhoomikhata.model.OfferHistory;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface OfferHistoryRepository extends MongoRepository<OfferHistory, String> {

    List<OfferHistory> findByBussinessId(String bussinessId);
}
