package com.example.bhoomikhata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BhoomikhataApplication {

	public static void main(String[] args) {
		SpringApplication.run(BhoomikhataApplication.class, args);
	}

}
