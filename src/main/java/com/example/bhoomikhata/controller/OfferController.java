package com.example.bhoomikhata.controller;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import com.example.bhoomikhata.model.CashbackModel;
import com.example.bhoomikhata.model.CashbackOffers;
import com.example.bhoomikhata.model.CoinModel;
import com.example.bhoomikhata.model.CoinOffers;
import com.example.bhoomikhata.model.ConversionTypes;
import com.example.bhoomikhata.model.CouponModel;
import com.example.bhoomikhata.model.CouponOffers;
import com.example.bhoomikhata.model.ErrorResponse;
import com.example.bhoomikhata.model.GenerateRewardRequest;
import com.example.bhoomikhata.model.OfferGenerationResponse;
import com.example.bhoomikhata.model.TransactionStatus;
import com.example.bhoomikhata.service.CashbackOfferService;
import com.example.bhoomikhata.service.CashbackService;
import com.example.bhoomikhata.service.CoinOfferService;
import com.example.bhoomikhata.service.CoinService;
import com.example.bhoomikhata.service.CouponOfferService;
import com.example.bhoomikhata.service.CouponService;
import com.example.bhoomikhata.utils.Utility;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/offer")
public class OfferController {

    private CoinService coinService;
    private CashbackService cashbackService;
    private CouponService couponService;
    private CouponOfferService couponOfferService;
    private CashbackOfferService cashbackOfferService;
    private CoinOfferService coinOfferService;

    public OfferController(CoinService coinService, CashbackService cashbackService, CouponService couponService,
            CouponOfferService couponOfferService, CashbackOfferService cashbackOfferService,
            CoinOfferService coinOfferService) {
        this.coinService = coinService;
        this.cashbackService = cashbackService;
        this.couponService = couponService;
        this.couponOfferService = couponOfferService;
        this.cashbackOfferService = cashbackOfferService;
        this.coinOfferService = coinOfferService;
    }

    @PostMapping("/generate/reward")
    public OfferGenerationResponse generateRewards(@RequestBody GenerateRewardRequest rewardRequest) {

        OfferGenerationResponse offerGenerationResponse = new OfferGenerationResponse();
        if (coinService.getTotalRewardsByContactId(rewardRequest.getContactId()) < 1000) {
            offerGenerationResponse.setErrorCode("400");
            offerGenerationResponse.setErrorResponse("INSUFFICIENT_COINS");
            return offerGenerationResponse;
        }
        offerGenerationResponse.setIsJackpot(false);
        CoinModel coinModel = new CoinModel();
        coinModel.setAmount(-1000);
        coinModel.setBussinessId(rewardRequest.getBussinessid());
        coinModel.setContactId(rewardRequest.getContactId());
        coinModel.setCreatedDate(OffsetDateTime.now());
        coinModel.setUpdatedDate(OffsetDateTime.now());
        coinService.saveOrUpdateCoinDetails(coinModel);

        switch (rewardRequest.getConversionTypes()) {
            case COUPON:
                Optional<CouponOffers> couponOffer = couponOfferService.getCouponOfferById(rewardRequest.getOfferId());
                if (couponOffer.isPresent()) {
                    CouponModel couponModel = new CouponModel();
                    couponModel.setBussinessId(rewardRequest.getBussinessid());
                    couponModel.setContactId(rewardRequest.getContactId());
                    couponModel.setCouponCode(couponOffer.get().getCouponCode());
                    couponModel
                            .setCouponExpiryDate(OffsetDateTime.now().plusDays(couponOffer.get().getExpiryDuration()));
                    couponModel.setStatus(TransactionStatus.ISSUED);
                    couponModel.setCreatedDate(OffsetDateTime.now());
                    couponModel.setUpdatedDate(couponOffer.get().getUpdatedDate());
                    couponModel.setRemarks(couponOffer.get().getRemarks());
                    couponModel.setIsJackpot(couponOffer.get().getIsJackpot());
                    couponService.saveOrUpdateCouponDetails(couponModel);
                    offerGenerationResponse.setCouponCode(couponOffer.get().getCouponCode());
                    offerGenerationResponse.setCouponRemarks(couponOffer.get().getRemarks());
                    offerGenerationResponse
                            .setTotalCoin(coinService.getTotalRewardsByContactId(rewardRequest.getContactId()));
                    offerGenerationResponse
                            .setTotalCashBack(
                                    cashbackService.getTotalRewardsByContactId(rewardRequest.getContactId()));
                } else {
                    offerGenerationResponse.setErrorResponse("No Matching Coupon offer found");
                    offerGenerationResponse.setErrorCode("400");
                }
                break;
            case CASHBACKS:
                Optional<CashbackOffers> cashbackOffer = cashbackOfferService
                        .getCashbackOfferById(rewardRequest.getOfferId());
                if (cashbackOffer.isPresent()) {
                    CashbackModel cashbackModel = new CashbackModel();
                    cashbackModel.setAmount(Utility
                            .calculateNumberOfCoinsOrCashback(cashbackOffer.get().getFormula(), 1000).intValue());
                    cashbackModel.setBussinessId(rewardRequest.getBussinessid());
                    cashbackModel.setContactId(rewardRequest.getContactId());
                    cashbackModel.setCreatedDate(OffsetDateTime.now());
                    cashbackModel.setUpdatedDate(OffsetDateTime.now());
                    cashbackModel.setExpiry(OffsetDateTime.now().plusDays(cashbackOffer.get().getExpiryDuration()));
                    cashbackModel.setRemark(cashbackOffer.get().getRemarks());
                    cashbackModel.setTransactionStatus(TransactionStatus.ISSUED);
                    cashbackService.saveOrUpdateCoinDetails(cashbackModel);
                    offerGenerationResponse.setOfferCashBack(cashbackModel.getAmount());
                    offerGenerationResponse.setCashBackRemarks(cashbackOffer.get().getRemarks());
                    offerGenerationResponse
                            .setTotalCashBack(
                                    cashbackService.getTotalRewardsByContactId(rewardRequest.getContactId()));
                    offerGenerationResponse
                            .setTotalCoin(coinService.getTotalRewardsByContactId(rewardRequest.getContactId()));
                } else {
                    offerGenerationResponse.setErrorResponse("No Matching Cashback offer found");
                    offerGenerationResponse.setErrorCode("400");
                }
                break;
            case COIN:
                Optional<CoinOffers> coinOffer = coinOfferService.getCoinOfferById(rewardRequest.getOfferId());
                if (coinOffer.isPresent()) {
                    coinModel = new CoinModel();
                    coinModel.setAmount(Utility
                            .calculateNumberOfCoinsOrCashback(coinOffer.get().getFormula(), 1000).intValue());
                    coinModel.setBussinessId(rewardRequest.getBussinessid());
                    coinModel.setContactId(rewardRequest.getContactId());
                    coinModel.setCreatedDate(OffsetDateTime.now());
                    coinModel.setUpdatedDate(OffsetDateTime.now());
                    coinService.saveOrUpdateCoinDetails(coinModel);
                    offerGenerationResponse.setOfferCoins(Utility
                            .calculateNumberOfCoinsOrCashback(coinOffer.get().getFormula(), 1000).intValue());
                    offerGenerationResponse
                            .setTotalCoin(coinService.getTotalRewardsByContactId(rewardRequest.getContactId()));
                    offerGenerationResponse
                            .setTotalCashBack(
                                    cashbackService.getTotalRewardsByContactId(rewardRequest.getContactId()));
                } else {
                    offerGenerationResponse.setErrorResponse("No Matching coin offer found");
                    offerGenerationResponse.setErrorCode("400");
                }
                break;
            default:
                offerGenerationResponse.setErrorCode("400");
                offerGenerationResponse.setErrorResponse("INVALID_REWARD");
                return offerGenerationResponse;
        }
        return offerGenerationResponse;

    }

    @PostMapping("/generate/game/reward")
    public OfferGenerationResponse generateGameRewards(@RequestBody GenerateRewardRequest rewardRequest) {
        List<CoinOffers> coinOffers = coinOfferService.getAllRecords();
        List<CoinOffers> validCoinOffers = new ArrayList<>();
        for (CoinOffers cOffers : coinOffers) {
            if (cOffers.getExpiryDate().isAfter(OffsetDateTime.now())) {
                validCoinOffers.add(cOffers);
            }
        }
        List<CouponOffers> couponOffers = couponOfferService.getAllRecords();
        List<CouponOffers> validCouponOffers = new ArrayList<>();
        for (CouponOffers cOffers : couponOffers) {
            if (cOffers.getExpiryDate().isAfter(OffsetDateTime.now())) {
                validCouponOffers.add(cOffers);
            }
        }
        if (validCoinOffers.isEmpty() || validCouponOffers.isEmpty()) {
            OfferGenerationResponse offerGenerationResponse = new OfferGenerationResponse();
            offerGenerationResponse.setErrorResponse("Either Coupon Offer or coin offer is Missing");
            offerGenerationResponse.setErrorCode("404");
            return offerGenerationResponse;
        }

        int coinOfferSize = validCoinOffers.size();
        int couponOfferSize = validCouponOffers.size();
        Random random = new Random();
        int randomGameOfferIndex = random.nextInt(coinOfferSize + 1);

        if (randomGameOfferIndex == coinOfferSize) {
            int couponOfferIndex = random.nextInt(couponOfferSize);
            rewardRequest.setConversionTypes(ConversionTypes.COUPON);
            rewardRequest.setOfferId(validCouponOffers.get(couponOfferIndex).getId());
            return generateRewards(rewardRequest);
        } else {
            rewardRequest.setConversionTypes(ConversionTypes.COIN);
            rewardRequest.setOfferId(validCoinOffers.get(randomGameOfferIndex).getId());
            return generateRewards(rewardRequest);
        }
    }

}
