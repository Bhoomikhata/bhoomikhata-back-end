package com.example.bhoomikhata.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bhoomikhata.model.DocModel;
import com.example.bhoomikhata.model.KycStatus;
import com.example.bhoomikhata.service.SupportBackendService;

@RestController
@RequestMapping("/backend-support")
public class SupportBackendController {

	@Autowired
	private SupportBackendService backendService;

	@GetMapping("/kyc")
	public Map<String, List<DocModel>> getWatingKycForApproval() {
		return backendService.getAllKycWaitingVerification();
	}

	@PutMapping("/verifyKyc/{businessId}/{docId}/{kycStatus}")
	public String verifyAndSetKycStatus(@PathVariable String businessId, @PathVariable String docId,
			@PathVariable KycStatus kycStatus) {
		return backendService.verifyAndSetKycStatus(businessId, docId, kycStatus);
	}

}
