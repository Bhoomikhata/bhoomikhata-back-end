package com.example.bhoomikhata.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.example.bhoomikhata.service.TransactionService;
import com.example.bhoomikhata.model.TransactionModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransactionController {

    private TransactionService transactionService;

    @Autowired
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @GetMapping("/getTransactionDetails/{id}")
    public Optional<TransactionModel> getTransactionDetails(@PathVariable String id) {
        return transactionService.getTransactionDetails(id);
    }

    @GetMapping("/getAllTransactionDetails")
    public List<TransactionModel> getAllTransactionDetails() {
        return transactionService.getAllTransactionDetails();
    }

    @DeleteMapping("/deleteTransactionDetailsById/{id}")
    public Boolean deleteTransactionDetailsById(@PathVariable String id) {
        return transactionService.deleteTransactionDetailsById(id);
    }

    @PostMapping("/createTransactionDetails")
    public TransactionModel createTransactionDetails(@RequestBody TransactionModel transactionModel) {
        return transactionService.createOrUpdateTransactionDetails(transactionModel);
    }

	/*
	 * @PutMapping("/updateTransactionDetails") public TransactionModel
	 * updateTransactionDetails(@RequestBody TransactionModel transactionModel) {
	 * return transactionService.createOrUpdateTransactionDetails(transactionModel);
	 * }
	 */
    
    @PatchMapping("/patchTransactionDetails")
    public TransactionModel patchTransactionDetails(@RequestBody TransactionModel transactionModel) {
        return transactionService.patchTransactionDetails(transactionModel);
    }

    @DeleteMapping("/deleteTransactionDetails/bussiness/{bussinessId}/contact/{contactId}")
    public Boolean deleteTransactionDetails(@PathVariable String bussinessId, @PathVariable String contactId) {
        return transactionService.deleteTransactionDetails(bussinessId, contactId);
    }

    @DeleteMapping("/deleteAllTransactionDetails")
    public Boolean deleteAllTransactionDetails() {
        return transactionService.deleteAllTransactions();
    }

    @GetMapping("/getTransactionDetailsByPayee/{payeeId}")
    public List<TransactionModel> getTransactionDetailsByPayee(@PathVariable String payeeId) {
        return transactionService.getTransactionDetailsByPayee(payeeId);
    }

    @GetMapping("/getTransactionDetailsByPayer/{payerId}")
    public List<TransactionModel> getTransactionDetailsByPayer(@PathVariable String payerId) {

        return transactionService.getTransactionDetailsByPayer(payerId);
    }

    @GetMapping("/getTransactionDetails/payer/{payerId}/payee/{payeeId}")
    public Set<TransactionModel> getTransactionDetailsByPayerAndPayee(@PathVariable String payerId,
            @PathVariable String payeeId) {
        Set<TransactionModel> set = new HashSet<>();
        set.addAll(getTransactionDetailsByPayer(payerId));
        set.addAll(transactionService.getTransactionDetailsByPayee(payerId));
        return set;
    }

    @GetMapping("/getTransactionDetailsByBussinessId/{bussinessId}")
    public List<TransactionModel> getTransactionDetailsByBussinessid(@PathVariable String bussinessId) {
        return transactionService.getTransactionDetailsByBussinessId(bussinessId);
    }

    @GetMapping("/getTransactionDetailsByContactId/{contactId}")
    public List<TransactionModel> getTransactionDetailsByContactId(@PathVariable String contactId) {

        return transactionService.getTransactionDetailsByContactId(contactId);
    }

    @GetMapping("/getTransactionDetails/bussiness/{bussinessId}/contact/{contactId}")
    public List<TransactionModel> getTransactionDetailsByBussinessAndContact(@PathVariable String bussinessId,
            @PathVariable String contactId) {
        return transactionService.getTransactionDetails(bussinessId, contactId);
    }
}
