package com.example.bhoomikhata.controller;

import java.util.List;
import java.util.Optional;

import com.example.bhoomikhata.model.CashbackOffers;
import com.example.bhoomikhata.service.CashbackOfferService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cashbackOffer")
public class CashbackOfferController {

    private CashbackOfferService cashbackOfferService;

    @Autowired
    public CashbackOfferController(CashbackOfferService cashbackOfferService) {
        this.cashbackOfferService = cashbackOfferService;
    }

    @GetMapping("/getCashbackOffersById/{id}")
    public Optional<CashbackOffers> getCashbackOffersById(@PathVariable String id) {
        return cashbackOfferService.getCashbackOfferById(id);
    }

    @PostMapping("/saveOrUpdateCashbackOffersDetails")
    public CashbackOffers saveOrUpdateCashbackOffersDetails(@RequestBody CashbackOffers cashbackOffers) {
        return cashbackOfferService.saveOrUpdateCoinOfferDetails(cashbackOffers);
    }

    @DeleteMapping("/deleteCashbackOffersById")
    public Boolean deleteCashbackOffersById(@PathVariable String id) {

        return cashbackOfferService.deleteCashbackOfferById(id);

    }

    @GetMapping("/getAllRecords")
    public List<CashbackOffers> getAllRecords() {
        return cashbackOfferService.getAllRecords();
    }

    @DeleteMapping("/deleteAllRecords")
    public boolean deleteAllRecords() {
        return cashbackOfferService.deleteAllRecords();
    }
}
