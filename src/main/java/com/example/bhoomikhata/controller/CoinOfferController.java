package com.example.bhoomikhata.controller;

import java.util.List;
import java.util.Optional;

import com.example.bhoomikhata.model.CoinOffers;
import com.example.bhoomikhata.service.CoinOfferService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/coinOffer")
public class CoinOfferController {

    private CoinOfferService coinOfferService;

    @Autowired
    public CoinOfferController(CoinOfferService coinOfferService) {
        this.coinOfferService = coinOfferService;
    }

    @GetMapping("/getCoinOffersById/{id}")
    public Optional<CoinOffers> getCoinOffersById(@PathVariable String id) {
        return coinOfferService.getCoinOfferById(id);
    }

    @PostMapping("/saveOrUpdateCoinOffersDetails")
    public CoinOffers saveOrUpdateCoinOffersDetails(@RequestBody CoinOffers coinOffers) {
        return coinOfferService.saveOrUpdateCoinOfferDetails(coinOffers);
    }

    @DeleteMapping("/deleteCoinOffersById")
    public Boolean deleteCoinOffersById(@PathVariable String id) {
        return coinOfferService.deleteCoinOfferById(id);
    }

    @GetMapping("/getAllRecords")
    public List<CoinOffers> getAllRecords() {
        return coinOfferService.getAllRecords();
    }

    @DeleteMapping("/deleteAllRecords")
    public boolean deleteAllRecords() {
        return coinOfferService.deleteAllRecords();
    }
}
