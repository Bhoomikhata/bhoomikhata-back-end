package com.example.bhoomikhata.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bhoomikhata.model.InvoiceModel;
import com.example.bhoomikhata.service.InvoiceService;

@RestController
@RequestMapping("/invoice")
public class InvoiceController {

	@Autowired
	private InvoiceService invoiceService;

	@GetMapping("/getInvoiceByOrderId/{orderId}")
	public InvoiceModel getInvoiceByOrderId(@PathVariable String orderId) {
		return invoiceService.generateInvoice(orderId);
	}

	@GetMapping("/getAllInvoices")
	public List<InvoiceModel> getAllInvoice() {
		return invoiceService.getAllInvoice();
	}

	@DeleteMapping("/deleteAllInvoice")
	public void deleteAllInvoice() {
		invoiceService.deleteAllInvoice();
	}
}
