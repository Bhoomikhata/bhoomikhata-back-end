package com.example.bhoomikhata.controller;

import java.util.List;
import java.util.Optional;

import javax.annotation.security.RolesAllowed;

import com.example.bhoomikhata.model.AccessKeyModel;
import com.example.bhoomikhata.model.AuthModel;
import com.example.bhoomikhata.service.AuthService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

    @Autowired
    private AuthService authService;

    @PostMapping("/addCredential")
    @RolesAllowed("admin")
    public String saveCredential(@RequestBody AuthModel authModel) {
        return authService.saveAuthModel(authModel);
    }

    @GetMapping("/getCredentials/{id}")
    @RolesAllowed("user")
    public Optional<AuthModel> getCredential(@PathVariable String id) {
        return authService.getAuthModel(id);
    }

    @PutMapping("/updateCredentials")
    @RolesAllowed("admin")
    public String updateCredential(@RequestBody AuthModel authModel) {
        return authService.updateAuthModel(authModel);
    }

    @DeleteMapping("/deleteCredentials/{id}")
    @RolesAllowed("admin")
    public String deleteCredential(@PathVariable String id) {
        return authService.deleteAuthModel(id);
    }

    @GetMapping("/getCredentials")
    @RolesAllowed("admin")
    public List<AuthModel> getCredential() {
        return authService.getAllAuthModel();
    }

    @PostMapping("/getAccessKey")
    public String getAccessKey(@RequestBody AccessKeyModel accessKeyModel) {
        return authService.getAccessKey(accessKeyModel);
    }

}
