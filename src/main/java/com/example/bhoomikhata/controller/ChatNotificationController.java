package com.example.bhoomikhata.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bhoomikhata.model.BusinessIdTokenModel;
import com.example.bhoomikhata.model.ChatNotificationModel;
import com.example.bhoomikhata.service.BusinessIdTokenService;
import com.example.bhoomikhata.service.ChatNotificationService;
import com.google.firebase.messaging.FirebaseMessagingException;

@RestController
@RequestMapping("/chatNotification")
public class ChatNotificationController {

	@Autowired
	private ChatNotificationService chatService;

	@Autowired
	private BusinessIdTokenService tokenService;

	// Testing...............
	@GetMapping("/sendMessage/{token}")
	public String sendChatMessageToToken(@PathVariable String token) throws FirebaseMessagingException {
		return chatService.sendChatMessageToToken(token);
	}

	@PostMapping("/sendMessage")
	public String sendChatMessage(@RequestBody ChatNotificationModel chatMessage) throws FirebaseMessagingException {
		return chatService.sendChatMessage(chatMessage);
	}

	@PostMapping("/registerToken")
	public ResponseEntity<BusinessIdTokenModel> saveBusinessIdTokenPair(
			@RequestBody BusinessIdTokenModel idTokenModel) {
		if (idTokenModel.getBusinessId().length() == 0 || idTokenModel.getTokenId().length() == 0
				|| idTokenModel.getBusinessId().equalsIgnoreCase("string")
				|| idTokenModel.getTokenId().equalsIgnoreCase("string")) {
			idTokenModel.setBusinessId(null);
			idTokenModel.setTokenId(null);
			return ResponseEntity.badRequest().body(idTokenModel);
		}
		return ResponseEntity.ok(tokenService.saveToken(idTokenModel));
	}

	@GetMapping("/showAllTokenList")
	public List<BusinessIdTokenModel> getAllTokenList() {
		return tokenService.getAllTokenList();
	}

}
