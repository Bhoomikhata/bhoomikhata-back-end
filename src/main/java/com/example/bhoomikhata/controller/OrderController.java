package com.example.bhoomikhata.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bhoomikhata.model.OrderDetails;
import com.example.bhoomikhata.model.OrderDto;
import com.example.bhoomikhata.model.OrderModel;
import com.example.bhoomikhata.service.OrderService;

@RestController
@RequestMapping("/order")
public class OrderController {

	@Autowired
	private OrderService orderService;

	@PostMapping("/newOrder")
	public OrderModel saveOrder(@RequestBody OrderDto order) {
		return orderService.saveOrder(order);
	}

	@GetMapping("/getAllOrder")
	public List<OrderModel> getAllOrder() {
		return orderService.getAllOrder();
	}

	@GetMapping("/getOrder/{orderId}")
	public Optional<OrderModel> getOrderById(@PathVariable String orderId) {
		return orderService.getOrderById(orderId);
	}

	@DeleteMapping("/deleteOrder/{orderId}")
	public Boolean deleteOrder(@PathVariable String orderId) {
		return orderService.deleteOrder(orderId);
	}

	@DeleteMapping("/deleteAllOrder")
	public Boolean deleteAllOrder() {
		return orderService.deleteAllOrder();
	}

	@GetMapping("/getOrder/customerid/{customerId}/businessid/{businessId}")
	public List<OrderModel> getByBusinessIdAndCustomerId(@PathVariable String customerId,
			@PathVariable String businessId) {
		return orderService.getByBusinessIdAndCustomerId(customerId, businessId);
	}

	@GetMapping("/getOrderByBusinessId/{businessId}")
	public List<OrderModel> getOrderByBusinessId(@PathVariable String businessId) {
		return orderService.getByBusinessId(businessId);
	}

	@GetMapping("/getOrderByCustomerId/{customerId}")
	public List<OrderModel> getOrderByCustomerId(@PathVariable String customerId) {
		return orderService.getByCustomerId(customerId);
	}

	@PutMapping("/udpateOrder/{orderId}")
	public OrderModel updateOrderById(@PathVariable String orderId, @RequestBody List<OrderDetails> order) {
		return orderService.updateOrderById(orderId,order);
	}
	
	/*
	 * @PutMapping("/returnOrder/{orderId}") public OrderModel
	 * returnOrderById(@PathVariable String orderId) { return
	 * orderService.returnOrder(orderId); }
	 */

}
