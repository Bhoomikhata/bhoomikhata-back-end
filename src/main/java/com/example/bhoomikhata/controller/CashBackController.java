package com.example.bhoomikhata.controller;

import java.util.List;

import com.example.bhoomikhata.model.CashbackModel;
import com.example.bhoomikhata.service.CashbackService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cashback")
public class CashBackController {

    CashbackService cashbackService;

    @Autowired
    public CashBackController(CashbackService cashbackService) {
        this.cashbackService = cashbackService;
    }

    @GetMapping("/getCashBackbyId/{id}")
    public CashbackModel getCashBackById(@PathVariable String id) {
        return cashbackService.getCashBackById(id);
    }

    @GetMapping("/getCashBackbyContactId/{id}")
    public List<CashbackModel> getCashBackByContactId(@PathVariable String id) {
        return cashbackService.getCashBackbyContactId(id);
    }

    @GetMapping("/getCashBackByBussinessId/{id}")
    public List<CashbackModel> getCashBackByBussinessId(@PathVariable String id) {
        return cashbackService.getCashBackbyBussinessId(id);
    }

    @PostMapping("/saveOrUpdateCashBack")
    public CashbackModel saveOrUpdateCashBack(@RequestBody CashbackModel cashbackModel) {
        return cashbackService.saveOrUpdateCoinDetails(cashbackModel);
    }

    @DeleteMapping("/deleteCashBackById")
    public Boolean deleteCashBackById(@PathVariable String id) {

        return cashbackService.deleteCashBackById(id);
    }

    @DeleteMapping("/deleteCashBackByContactId")
    public Boolean deleteCashBackByContactId(@PathVariable String id) {

        return cashbackService.deleteCashBackByContactId(id);

    }

    @DeleteMapping("/deleteCashBackByBussinessId")
    public Boolean deleteCashBackByBussinessId(@PathVariable String id) {

        return cashbackService.deleteCashBackByBussinessId(id);

    }

    @GetMapping("/getAllRecords")
    public List<CashbackModel> getAllRecords() {
        return cashbackService.getAllRecords();
    }

    @DeleteMapping("/deleteAllRecords")
    public boolean deleteAllRecords() {
        return cashbackService.deleteAllRecords();
    }

}
