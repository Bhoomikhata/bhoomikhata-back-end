package com.example.bhoomikhata.controller;

import java.util.List;
import java.util.Optional;

import com.example.bhoomikhata.model.ContactModel;
import com.example.bhoomikhata.model.UserRating;
import com.example.bhoomikhata.model.UserRatingDto;
import com.example.bhoomikhata.service.ContactService;
import com.example.bhoomikhata.service.RatingsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/contact")
public class ContactController {

	@Autowired
	private RatingsService ratingsService;
	
    private ContactService contactService;

    @Autowired
    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    @GetMapping("/getContactProfile/{id}")
    public Optional<ContactModel> getContactProfile(@PathVariable String id) {

        return contactService.getContactProfile(id);
    }

    @GetMapping("/getContactProfileByMobile/{mobileNumber}")
    public List<ContactModel> getContactProfileByMobile(@PathVariable String mobileNumber) {

        return contactService.getContactProfileByMobile(mobileNumber);
    }

    @PostMapping("/createContactProfile")
    public ContactModel createContactProfile(@RequestBody ContactModel contactModel) {
        return contactService.createOrUpdateContactProfile(contactModel);
    }

    @PutMapping("/updateContactProfile")
    public ContactModel updateContactProfile(@RequestBody ContactModel contactModel) {
        return contactService.createOrUpdateContactProfile(contactModel);
    }

    @DeleteMapping("/deleteContactProfile/contact/{contactId}")
    public Boolean deleteContactProfile(@PathVariable String contactId) {
        return contactService.deleteContactProfile(contactId);
    }

    @DeleteMapping("/deleteAllContactProfile")
    public Boolean deleteAllContactProfile() {
        return contactService.deleteAllContactProfile();
    }

    @GetMapping("/getAllContactProfiles")
    public List<ContactModel> getAllContactProfile() {

        return contactService.getAllContactProfile();
    }

    @GetMapping("/getAllContactProfiles/bussinessId/{bussinessId}")
    public List<ContactModel> getAllContactByBussinessIdProfile(@PathVariable String bussinessId) {

        return contactService.getContactProfileByBussinessId(bussinessId);
    }

    
    @PostMapping("/giveRating")
	public String giveRating(@RequestBody UserRatingDto rating) {
		return ratingsService.giveUserRating(rating);
	}
	
	@GetMapping("/getRating/{businessId}/{customerId}")
	public UserRating getRatingForContactProfile(@PathVariable String businessId, @PathVariable String customerId) {
		return ratingsService.getUserRatingForContactProfile(businessId, customerId);
	}
}
