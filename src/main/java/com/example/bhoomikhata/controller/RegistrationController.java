package com.example.bhoomikhata.controller;

import com.example.bhoomikhata.model.OTPModel;
import com.example.bhoomikhata.service.RegistrationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController
public class RegistrationController {

    private RegistrationService registrationService;

    @Autowired
    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @GetMapping("/generateOTP/mobile/{mobileNumber}/countryCode/{countryCode}")
    public boolean generateOTP(@PathVariable String mobileNumber, @PathVariable String countryCode) {
        return registrationService.generateOTP(mobileNumber, countryCode);
    }

    @GetMapping("/generateSMS/mobile/{mobileNumber}/countryCode/{countryCode}/message/{message}")
    public boolean generateSMS(@PathVariable String mobileNumber, @PathVariable String countryCode,
            @PathVariable String message) {
        return registrationService.sendMessage(mobileNumber, countryCode, message);
    }

    @GetMapping("twillio/generateSMS/mobile/{mobileNumber}/countryCode/{countryCode}/message/{message}")
    public boolean generateTwillioSMS(@PathVariable String mobileNumber, @PathVariable String countryCode,
            @PathVariable String message) {
        return registrationService.sendTwillioMessage(mobileNumber, countryCode, message);
    }

    @GetMapping("/getOTP/{mobileNumber}")
    public OTPModel getOTP(@PathVariable String mobileNumber) {
        return registrationService.getOTP(mobileNumber);
    }

    @PostMapping("/saveOTP")
    public OTPModel saveOTP(@RequestBody OTPModel otpModel) {
        return registrationService.saveOTP(otpModel);
    }

    @PutMapping("/updateOTP")
    public OTPModel updateOTP(@RequestBody OTPModel otpModel) {
        return registrationService.updateOTP(otpModel);
    }

    @GetMapping("/isOTPExpired/{mobileNumber}")
    public boolean isOTPExpired(@PathVariable String mobileNumber) {
        return registrationService.isOTPExpired(mobileNumber);
    }
}
