package com.example.bhoomikhata.controller;

import java.util.Optional;

import com.example.bhoomikhata.model.IdentityDocModel;
import com.example.bhoomikhata.service.IdentityDocService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IdentityDocController {

    private IdentityDocService identityDocService;

    @Autowired
    public IdentityDocController(IdentityDocService identityDocService) {
        this.identityDocService = identityDocService;
    }

    @GetMapping("/getIdentityDocDetails/{id}")
    public Optional<IdentityDocModel> getIdentityDocDetails(@PathVariable String id) {
        return identityDocService.getDocDetails(id);
    }

    @PostMapping("/createIdentityDocDetails")
    public IdentityDocModel createIdentityDocDetails(@RequestBody IdentityDocModel identityDocModel) {
        return identityDocService.createAndUpdateDocDetails(identityDocModel);
    }

    @PutMapping("/updateIdentityDocDetails")
    public IdentityDocModel updateIdentityDocDetails(@RequestBody IdentityDocModel identityDocModel) {
        return identityDocService.createAndUpdateDocDetails(identityDocModel);
    }

    @DeleteMapping("/deleteIdentityDocDetails/{id}")
    public void deleteIdentityDocDetails(@PathVariable String id) {
        identityDocService.deleteDocDetails(id);
    }
}
