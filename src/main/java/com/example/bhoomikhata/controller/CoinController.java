package com.example.bhoomikhata.controller;

import java.util.List;
import java.util.Optional;

import com.example.bhoomikhata.model.CoinModel;
import com.example.bhoomikhata.service.CoinService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/coin")
public class CoinController {

    CoinService coinService;

    @Autowired
    public CoinController(CoinService coinService) {
        this.coinService = coinService;
    }

    @GetMapping("/getCoinbyId/{id}")
    public Optional<CoinModel> getCoinById(@PathVariable String id) {
        return coinService.getCoinById(id);
    }

    @GetMapping("/getCoinbyContactId/{id}")
    public List<CoinModel> getCoinByContactId(@PathVariable String id) {
        return coinService.getCoinbyContactId(id);
    }

    @GetMapping("/getCoinByBussinessId/{id}")
    public List<CoinModel> getCoinByBussinessId(@PathVariable String id) {
        return coinService.getCoinbyBussinessId(id);
    }

    @PostMapping("/saveOrUpdateCoin")
    public CoinModel saveOrUpdateCoin(@RequestBody CoinModel CoinModel) {
        return coinService.saveOrUpdateCoinDetails(CoinModel);
    }

    @DeleteMapping("/deleteCoinById")
    public Boolean deleteCoinById(@PathVariable String id) {

        return coinService.deleteCoinById(id);

    }

    @DeleteMapping("/deleteCoinByContactId")
    public Boolean deleteCoinByContactId(@PathVariable String id) {

        return coinService.deleteCoinByContactId(id);
    }

    @DeleteMapping("/deleteCoinByBussinessId")
    public Boolean deleteCoinByBussinessId(@PathVariable String id) {

        return coinService.deleteCoinByBussinessId(id);

    }

    @GetMapping("/getAllRecords")
    public List<CoinModel> getAllRecords() {
        return coinService.getAllRecords();
    }

    @DeleteMapping("/deleteAllRecords")
    public boolean deleteAllRecords() {
        return coinService.deleteAllRecords();
    }
}
