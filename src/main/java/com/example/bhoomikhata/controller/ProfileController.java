package com.example.bhoomikhata.controller;

import java.util.List;
import java.util.Optional;

import com.example.bhoomikhata.model.BussinesModel;
import com.example.bhoomikhata.model.UserRating;
import com.example.bhoomikhata.service.ProfileService;
import com.example.bhoomikhata.service.RatingsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/profile")
public class ProfileController {

	private ProfileService profileService;

	@Autowired
	public ProfileController(ProfileService profileService) {
		this.profileService = profileService;
	}
	
	@Autowired
	private RatingsService ratingsService;

	@GetMapping("/bussiness/{bussinessId}")
	public Optional<BussinesModel> getBusinessProfile(@PathVariable String bussinessId) {

		return profileService.getBusinessProfile(bussinessId);
	}

	@PostMapping("/createBussiness")
	public BussinesModel createBussinesProfile(@RequestBody BussinesModel bussinesModel) {
		return profileService.createOrUpdateBussinesProfile(bussinesModel);
	}

	@PutMapping("/updateBussiness")
	public BussinesModel updateBussinesProfile(@RequestBody BussinesModel bussinesModel) {
		return profileService.createOrUpdateBussinesProfile(bussinesModel);
	}
	
	@PatchMapping("/patchBussiness")
	public BussinesModel patchBussinesProfile(@RequestBody BussinesModel bussinesModel) {
		return profileService.patchBusinessProfile(bussinesModel);
	}

	@DeleteMapping("/deleteBussiness/{bussinessId}")
	public Boolean deleteBussinesProfile(@PathVariable String bussinessId) {

		return profileService.deleteBussinesProfile(bussinessId);
	}

	@DeleteMapping("/deleteAllBussiness")
	public Boolean deleteAllBussinesProfile() {

		return profileService.deleteAllBussinesProfile();
	}

	@GetMapping("/getAllBussinessProfile")
	public List<BussinesModel> getAllBusinessProfile() {

		return profileService.getAllBusinessProfile();
	}
	
	@GetMapping("/getRating/{businessId}")
	public UserRating getRating(@PathVariable String businessId) {
		return ratingsService.getUserRating(businessId);
	}
	
	
	
}
