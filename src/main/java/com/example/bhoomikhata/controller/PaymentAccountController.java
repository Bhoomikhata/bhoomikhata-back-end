package com.example.bhoomikhata.controller;

import java.util.List;
import java.util.Optional;

import com.example.bhoomikhata.model.PaymentModeModel;
import com.example.bhoomikhata.service.PaymentAccountService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentAccountController {

    private PaymentAccountService paymentAccountService;

    @Autowired
    public PaymentAccountController(PaymentAccountService paymentAccountService) {
        this.paymentAccountService = paymentAccountService;
    }

    @GetMapping("/getPaymentAccountDetails/{id}")
    public Optional<PaymentModeModel> getPaymentAccountDetails(@PathVariable String id) {
        return paymentAccountService.getPaymentModeDetails(id);
    }

    @GetMapping("/getAllPaymentAccountDetails")
    public List<PaymentModeModel> getAllPaymentAccountDetails() {
        return paymentAccountService.getAllPaymentModeDetails();
    }

    @GetMapping("/getPaymentAccountDetails/bussiness/{bussinessId}/contact/{contactId}")
    public List<PaymentModeModel> getAllPaymentAccountDetails(@PathVariable String bussinessId,
            @PathVariable String contactId) {
        return paymentAccountService.getPaymentModeDetails(bussinessId, contactId);
    }

    @PostMapping("/createPaymentAccountDetails")
    public PaymentModeModel createPaymentAccountDetails(@RequestBody PaymentModeModel paymentModeModel) {
        return paymentAccountService.createOrUpdatePaymentModeDetails(paymentModeModel);
    }

    @PutMapping("/updatePaymentAccountDetails")
    public PaymentModeModel updatePaymentAccountDetails(@RequestBody PaymentModeModel paymentModeModel) {
        return paymentAccountService.createOrUpdatePaymentModeDetails(paymentModeModel);
    }

    @DeleteMapping("/deletePaymentAccountDetails/{id}/bussiness/{bussinessId}/contact/{contactId}")
    public Boolean deletePaymentAccountDetails(@PathVariable String id, @PathVariable String bussinessId,
            @PathVariable String contactId) {
        return paymentAccountService.deletePaymentModeDetails(bussinessId, contactId);
    }
}
