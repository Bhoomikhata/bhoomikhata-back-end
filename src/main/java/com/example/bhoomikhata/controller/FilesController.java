package com.example.bhoomikhata.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.bhoomikhata.model.DocModel;
import com.example.bhoomikhata.model.DocType;
import com.example.bhoomikhata.service.FilesDeleteSevice;
import com.example.bhoomikhata.service.FilesDownloadService;
import com.example.bhoomikhata.service.FilesUploadService;

@RestController
@RequestMapping("/files")
public class FilesController {

	@Autowired
	private FilesUploadService uploadService;

	@Autowired
	private FilesDownloadService downloadService;

	@Autowired
	private FilesDeleteSevice deleteSevice;

	@PostMapping(value = "/profileimage/{businessId}", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<String> uploadProfileImage(@PathVariable String businessId,
			@RequestBody MultipartFile imageFile) {
		try {
			return ResponseEntity.ok(uploadService.uploadProfileImage(businessId, imageFile));

		} catch (Exception e) {
			return ResponseEntity.internalServerError().body(e.getLocalizedMessage());
		}

	}

	@GetMapping(value = "/profileimage/{businessId}", produces = { MediaType.IMAGE_JPEG_VALUE,
			MediaType.IMAGE_PNG_VALUE })
	public ResponseEntity<InputStreamResource> getProfileImage(@PathVariable String businessId) {
		try {
			return downloadService.getProfileImage(businessId);

		} catch (Exception e) {
			return ResponseEntity.internalServerError().build();
		}
	}

	@PostMapping(value = "/profileKyc", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<String> uploadKycImage(@RequestParam String businessId, @RequestParam DocType documentType,
			@RequestBody MultipartFile imageFile) {
		try {
			return ResponseEntity.ok(uploadService.uploadProfileKycImage(businessId, documentType, imageFile));

		} catch (Exception e) {
			return ResponseEntity.internalServerError().body(e.getLocalizedMessage());
		}

	}

	@GetMapping("/profileKycByBusinessId/{businessId}")
	public ResponseEntity<List<DocModel>> getAllKycDetails(@PathVariable String businessId) {
		try {
			return ResponseEntity.ok(downloadService.getAllKycByBusinessId(businessId));

		} catch (Exception e) {
			return ResponseEntity.internalServerError().build();
		}
	}

	@GetMapping(value = "/profileKycByDocId/{docId}", produces = { MediaType.IMAGE_JPEG_VALUE,
			MediaType.IMAGE_PNG_VALUE })
	public ResponseEntity<InputStreamResource> getKycImageById(@PathVariable String docId) {
		try {
			return downloadService.getKycImageById(docId);

		} catch (Exception e) {
			return ResponseEntity.internalServerError().build();
		}
	}

	@PostMapping(value = "/profileimage-gallary/{businessId}", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<String> uploadProfileGalleryImage(@PathVariable String businessId,
			@RequestBody MultipartFile imageFile) {
		try {
			return ResponseEntity.ok(uploadService.uploadProfileGallaryImage(businessId, imageFile));

		} catch (Exception e) {
			return ResponseEntity.internalServerError().body(e.getLocalizedMessage());
		}

	}

	@GetMapping(value = "/profileimage-gallary/{businessId}/{imageId}", produces = { MediaType.IMAGE_JPEG_VALUE,
			MediaType.IMAGE_PNG_VALUE })
	public ResponseEntity<InputStreamResource> getProfileGalleryImage(@PathVariable String businessId,
			@PathVariable String imageId) {
		try {
			return downloadService.getProfileGallaryImage(businessId, imageId);

		} catch (Exception e) {
			return ResponseEntity.internalServerError().build();
		}
	}

	@GetMapping("/profileimage-gallary/{businessId}")
	public ResponseEntity<List<String>> getAllGalleryImages(@PathVariable String businessId) {
		try {
			return ResponseEntity.ok(downloadService.getAllGalleryImageDetails(businessId));

		} catch (Exception e) {
			return ResponseEntity.internalServerError().body(new ArrayList<String>());
		}
	}

	@DeleteMapping("/profileimage-gallary/{imageId}")
	public String deleteGalleryImageByImageId(@PathVariable String imageId) {
		return deleteSevice.deleteGalleryImageById(imageId);
	}

	@PostMapping(value = "/fileChatTransaction/{transactionId}", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<String> uploadFileForChatTransaction(@PathVariable String transactionId,
			@RequestBody MultipartFile document) {
		try {
			return ResponseEntity.ok(uploadService.uploadFileForChatTransaction(transactionId, document));

		} catch (Exception e) {
			return ResponseEntity.internalServerError().body(e.getLocalizedMessage());
		}
	}

	@GetMapping(value = "/fileChatTransaction/{transactionId}", produces = { MediaType.APPLICATION_OCTET_STREAM_VALUE })
	public ResponseEntity<ByteArrayResource> downloadFileForChatTransaction(@PathVariable String transactionId) {
		try {
			return downloadService.downloadFileForChatTransaction(transactionId);

		} catch (Exception e) {
			return ResponseEntity.internalServerError().build();
		}
	}

}
