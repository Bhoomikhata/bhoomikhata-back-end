package com.example.bhoomikhata.controller;

import java.util.List;
import java.util.Optional;

import com.example.bhoomikhata.model.CouponModel;
import com.example.bhoomikhata.service.CouponService;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/coupon")
public class CouponController {

    private CouponService couponService;

    public CouponController(CouponService couponService) {
        this.couponService = couponService;
    }

    @GetMapping("/getCouponbyId/{id}")
    public Optional<CouponModel> getCouponById(@PathVariable String id) {
        return couponService.getCouponById(id);
    }

    @GetMapping("/getCouponbyContactId/{id}")
    public List<CouponModel> getCouponByContactId(@PathVariable String id) {
        return couponService.getCouponbyContactId(id);
    }

    @GetMapping("/getCouponByBussinessId/{id}")
    public List<CouponModel> getCouponByBussinessId(@PathVariable String id) {
        return couponService.getCouponbyBussinessId(id);
    }

    @PostMapping("/saveOrUpdateCoupon")
    public CouponModel saveOrUpdateCoupon(@RequestBody CouponModel couponModel) {
        return couponService.saveOrUpdateCouponDetails(couponModel);
    }

    @DeleteMapping("/deleteCouponById")
    public Boolean deleteCouponById(@PathVariable String id) {

        return couponService.deleteCouponById(id);

    }

    @DeleteMapping("/deleteCouponByContactId")
    public Boolean deleteCouponByContactId(@PathVariable String id) {
        return couponService.deleteCouponByContactId(id);

    }

    @DeleteMapping("/deleteCouponByBussinessId")
    public Boolean deleteCouponByBussinessId(@PathVariable String id) {

        return couponService.deleteCouponByBussinessId(id);

    }

    @GetMapping("/getAllRecords")
    public List<CouponModel> getAllRecords() {
        return couponService.getAllRecords();
    }

    @DeleteMapping("/deleteAllRecords")
    public boolean deleteAllRecords() {
        return couponService.deleteAllRecords();
    }

}
