package com.example.bhoomikhata.controller;

import java.util.List;
import java.util.Optional;

import com.example.bhoomikhata.model.CouponOffers;
import com.example.bhoomikhata.service.CouponOfferService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/couponOffer")
public class CouponOfferController {
    CouponOfferService couponOfferService;

    @Autowired
    public CouponOfferController(CouponOfferService couponOfferService) {
        this.couponOfferService = couponOfferService;
    }

    @GetMapping("/getCouponOffersById/{id}")
    public Optional<CouponOffers> getCashbackOffersById(@PathVariable String id) {
        return couponOfferService.getCouponOfferById(id);
    }

    @PostMapping("/saveOrUpdateCouponOffersDetails")
    public CouponOffers saveOrUpdateCashbackOffersDetails(@RequestBody CouponOffers cashbackOffers) {
        return couponOfferService.saveOrUpdateCouponOfferDetails(cashbackOffers);
    }

    @DeleteMapping("/deleteCouponOffersById")
    public Boolean deleteCouponOffersById(@PathVariable String id) {

        return couponOfferService.deleteCouponOfferById(id);

    }

    @GetMapping("/getAllRecords")
    public List<CouponOffers> getAllRecords() {
        return couponOfferService.getAllRecords();
    }

    @DeleteMapping("/deleteAllRecords")
    public boolean deleteAllRecords() {
        return couponOfferService.deleteAllRecords();
    }

}
