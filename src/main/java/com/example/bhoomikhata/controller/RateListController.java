package com.example.bhoomikhata.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bhoomikhata.model.RateListModel;
import com.example.bhoomikhata.service.RateListService;

@RestController
@RequestMapping("/rateList")
public class RateListController {
	
	@Autowired
	private RateListService rateListService;
	
	@PostMapping("/saveItem")
	public RateListModel saveRateList(@RequestBody RateListModel item) {		
		return rateListService.saveRateList(item);
	}
	
	@GetMapping("/getRateListByBusinessId/{businessId}")
	public List<RateListModel> getRateListByBusinessId(@PathVariable String businessId){
		return rateListService.getRateListByBusinessId(businessId);
	}
	
	@PutMapping("/updateRateList")
	public RateListModel updateRateList(@RequestBody RateListModel rateListModel) {
		return rateListService.updateRateList(rateListModel);
	}
	
	@DeleteMapping("/deleteAll")
	public void deleteAllRateListModel() {
		rateListService.deleteAllRateList();
	}

    
}
