package com.example.bhoomikhata.controller;

import java.util.List;

import com.example.bhoomikhata.service.CommonService;
import com.example.bhoomikhata.utils.XMLParserUtil;
import com.example.bhoomikhata.model.ContactModel;
import com.example.bhoomikhata.model.ExtendedAddressModel;
import com.example.bhoomikhata.model.ExtendedTransactionModel;
import com.example.bhoomikhata.model.LatLngModel;
import com.example.bhoomikhata.model.TransactionModel;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("/common")
public class CommonController {

    private CommonService commonService;

    public CommonController(CommonService commonService) {
        this.commonService = commonService;
    }

    @GetMapping("/getAllAdressesOfBussiness/{bussinessId}")
    public List<ExtendedAddressModel> getAllAdressesOfBussiness(@PathVariable String bussinessId) {
        return commonService.getAllAdressesOfBussiness(bussinessId);
    }

    @GetMapping("/getAllAdressesOfSuppliers/{bussinessId}")
    public List<ExtendedAddressModel> getAllAdressesOfSuppliers(@PathVariable String bussinessId) {
        return commonService.getAllAdressesOfSuppliers(bussinessId);
    }

    @GetMapping("/getAllAdressesOfBussinessHavingValidMapping/{bussinessId}")
    public List<ExtendedAddressModel> getAllAdressesOfBussinessHavingValidMapping(@PathVariable String bussinessId) {
        return commonService.getAllAdressesOfBussinessHavingValidMapping(bussinessId);
    }

    @GetMapping("/getAllTransactionsOfBussiness/{bussinessId}/contact/{contactId}")
    public ExtendedTransactionModel getAllTransactionsOfBussinessByContact(@PathVariable String bussinessId,
            @PathVariable String contactId) {
        return commonService.getAllTransactionsOfBussinessByContact(bussinessId, contactId);
    }

    @PostMapping("/createTransactionDetails")
    public TransactionModel createTransactionDetails(@RequestBody TransactionModel transactionModel) {
        return commonService.createOrUpdateTransactionDetails(transactionModel);
    }

    @PostMapping("/createContactProfile")
    public ContactModel createContactProfile(@RequestBody ContactModel contactModel) {
        return commonService.createOrUpdateContactProfile(contactModel);
    }

    @GetMapping("/getLatLngForSearch")
    public List<LatLngModel> getLatLngForSearch(@RequestParam String searchString) {

        return XMLParserUtil.getLatLongForAreaHighlight(getClass().getResourceAsStream("/map/ind_area_highlight.kml"),
                searchString);
    }

}
