package com.example.bhoomikhata.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bhoomikhata.model.ContactBankAccount;
import com.example.bhoomikhata.model.ContactUpiAccount;
import com.example.bhoomikhata.model.RazPayOrderDto;
import com.example.bhoomikhata.model.RazPayOrderDtoResponse;
import com.example.bhoomikhata.model.RazPayOrderModel;
import com.example.bhoomikhata.model.RazPayPayOutResponseModel;
import com.example.bhoomikhata.model.RazPayPaymentInDataModel;
import com.example.bhoomikhata.model.RazPayPaymentModel;
import com.example.bhoomikhata.model.RazPayTransactionModel;
import com.example.bhoomikhata.model.RazPayTransactionModelResponse;
import com.example.bhoomikhata.service.RazPayAccountService;
import com.example.bhoomikhata.service.RazPayContactFundAccountService;
import com.example.bhoomikhata.service.RazPayOrderService;
import com.example.bhoomikhata.service.RazPayPayOutService;
import com.example.bhoomikhata.service.RazPayPaymentInService;
import com.example.bhoomikhata.service.RazPayPaymentTransactionService;
import com.example.bhoomikhata.service.RazPayTransactionService;

import io.swagger.v3.oas.annotations.Operation;

//import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/razorpay")
public class RazorPayController {

	@Autowired
	private RazPayOrderService orderService;

	@Autowired
	private RazPayPaymentInService payInService;

	@Autowired
	private RazPayPayOutService payOutService;

	/*
	 * @Autowired private RazPayRefundService refundService;
	 */
	@Autowired
	private RazPayPaymentTransactionService paymentDetailsService;

	@Autowired
	private RazPayTransactionService transactionService;

	@Autowired
	private RazPayContactFundAccountService contactFundAccountService;
	

	@Autowired
	private RazPayAccountService accountService;

	/************************** // Order // *************************/

	@Operation(summary = "For FrontEnd : Create new Payment Order.")
	@PostMapping("/createPaymentOrder")
	public RazPayOrderDtoResponse createOrder(@RequestBody RazPayOrderDto order) {
		return orderService.createRazPayOrder(order);
	}

	@GetMapping("/fetchPaymentOrderById/{id}")
	public RazPayOrderModel fetchOrder(@PathVariable String id) {
		return orderService.fetchOrdersById(id);
	}

	/************************** // Transactions // *************************/

	@GetMapping("/showTransactionLogs")
	public List<RazPayTransactionModel> getAllTransactions() {
		return transactionService.getAllTransactions();
	}

	@GetMapping("/showTransactionLogs/{transactionId}")
	public RazPayTransactionModel getTransactionById(@PathVariable String transactionId) {
		return transactionService.getTransactionById(transactionId);
	}

	@GetMapping("/getUserPaymentDetails/{businessId}")
	public List<RazPayTransactionModelResponse> getAllPaymentDetails(@PathVariable String businessId) {
		return paymentDetailsService.getAllPaymentsByBusinessId(businessId);
	}

	@GetMapping("/getUserPaymentDetails")
	public List<RazPayTransactionModelResponse> getAllPaymentDetails() {
		return paymentDetailsService.getAllPaymentDetails();
	}

	// for test only

	@DeleteMapping("/clearTransactionLogs")
	public boolean deleteTransactionLogs() {
		return transactionService.deleteAllTransactionLogs();
	}

	/************************** // Payment In // *************************/
	@Operation(summary = "For FrontEnd : Varify Signature Of Payment And Initiate Payout.")
	@PostMapping("/makePayment")
	public RazPayTransactionModelResponse savePaymentInSuccessData(@RequestBody RazPayPaymentInDataModel payment) {
		return payInService.savePaymentInSuccessDataWithFundAccountId(payment);
	}

	@Operation(description = "fetch from razorpay.")
	@GetMapping("/fetchPaymentInDetails/{id}")
	public RazPayPaymentModel fetchPaymentInDetails(@PathVariable String id) {
		return payInService.fetchRazorPayPaymentById(id);
	}

	/************************** // Payment OUT // *************************/
	@Operation(description = "fetch from razorpay.")
	@GetMapping("/fetchPaymentOutDetails/{id}")
	public RazPayPayOutResponseModel fetchPayOutDetails(@PathVariable String id) {
		return payOutService.fetchPayout(id);
	}

	/************************** // Refund Payment // *************************/
	/*
	 * @GetMapping("/refund/{refid}") public RazPayTransactionModel
	 * createRefundByTransactionId(@PathVariable String refid) { return
	 * refundService.createRefund(refid); }
	 */

	/**************************
	 * // RazorPay Account Details //
	 *************************/

<<<<<<< HEAD
	@ApiOperation(value = "fetch razorpay contact by customer id.")
=======
	@Operation(description =  "fetch razorpay contact by customer id.")
>>>>>>> Manish
	@GetMapping("/razorPayContactsByCustomerId/{customerId}")
	public ResponseEntity<?> getRazorPayContactByCustomerId(@PathVariable String customerId) {
		try {
			return ResponseEntity.ok(contactFundAccountService.getRazorPayContactByCustomerId(customerId));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}

<<<<<<< HEAD
	@ApiOperation(value = "fetch from razorpay.")
=======
	@Operation(description = "fetch from razorpay.")
>>>>>>> Manish
	@GetMapping("/razorPayContacts/{contactId}")
	public ResponseEntity<?> getRazorPayContactById(@PathVariable String contactId) {
		try {
			return ResponseEntity.ok(accountService.getRazorPayContactById(contactId));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}

<<<<<<< HEAD
	@ApiOperation(value = "fetch all active contacts from razorpay.")
=======
	@Operation(description = "fetch all active contacts from razorpay.")
>>>>>>> Manish
	@GetMapping("/razorPayContacts")
	public ResponseEntity<?> fetchAllRazorPayContacts() {
		try {
			return ResponseEntity.ok(accountService.fetchAllActiveRazorPayContacts());
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}

<<<<<<< HEAD
	@ApiOperation(value = "For FrontEnd : Add Bank account details by businessId.")
=======
	@Operation(summary =  "For FrontEnd : Add Bank account details by businessId.")
>>>>>>> Manish
	@PostMapping("/addBankDetailsByBusinessId")
	public ResponseEntity<?> addBankDetailsByBusinessId(@RequestBody ContactBankAccount bankAccountDetails) {
		try {
			return ResponseEntity.ok(contactFundAccountService.addBankAccountByBusinessId(bankAccountDetails));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}

<<<<<<< HEAD
	@ApiOperation(value = "For FrontEnd : Add UPI details by businessId")
=======
	@Operation(summary =  "For FrontEnd : Add UPI details by businessId")
>>>>>>> Manish
	@PostMapping("/addUpiDetailsByBusinessId")
	public ResponseEntity<?> addUpiDetailsByBusinessId(@RequestBody ContactUpiAccount upiDetails) {
		try {
			return ResponseEntity.ok(contactFundAccountService.addUpiAccountByBusinessId(upiDetails));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}


	@Operation(description = "fetch from razorpay.")
	@GetMapping("/razorPayFundAccounts/{fundAccountId}")
	public ResponseEntity<?> fetchRazorPayFundAccountById(@PathVariable String fundAccountId) {
		try {
			return ResponseEntity.ok(accountService.fetchRpayFundAccountById(fundAccountId));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}

<<<<<<< HEAD
	@ApiOperation(value = "fetch All Fund Accounts of a contact from razorpay for Business Profile.")
=======
	@Operation(description =  "fetch All Fund Accounts of a contact from razorpay for Business Profile.")
>>>>>>> Manish
	@GetMapping("/razorPayFundAccountsForBusinessProfile/{razorPayContactId}")
	public ResponseEntity<?> fetchRazorPayFundAccountsForBusinessProfile(@PathVariable String razorPayContactId) {
		try {
			return ResponseEntity.ok(contactFundAccountService.getAccountDetailsForBusinessProfile(razorPayContactId));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}

<<<<<<< HEAD
	@ApiOperation(value = "fetch from razorpay.")
=======
	@Operation(description = "fetch from razorpay.")
>>>>>>> Manish
	@GetMapping("/razorPayFundAccounts")
	public ResponseEntity<?> fetchRazorPayAllFundAccounts() {
		try {
			return ResponseEntity.ok(accountService.fetchAllRpayFundAccounts());
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}

<<<<<<< HEAD
	@ApiOperation(value = "Validation Bank or Upi Account by fund account ID")
=======
	@Operation(description = "Validation Bank or Upi Account by fund account ID")
>>>>>>> Manish
	@GetMapping("/razorPayFundAccountValidation/{fundAccountId}")
	public ResponseEntity<?> validateRazorPayFundAccountById(@PathVariable String fundAccountId) {
		try {
			return ResponseEntity.ok(accountService.validateBankAccountUpiAccount(fundAccountId));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}

<<<<<<< HEAD
	@ApiOperation(value = "Verify Bank or Upi Account validation.")
=======
	@Operation(description = "Verify Bank or Upi Account validation.")
>>>>>>> Manish
	@GetMapping("/razorPayFundAccountValidationStatusCheck/{fundAccountValidationId}")
	public ResponseEntity<?> varifyAccountValidationByValidationId(@PathVariable String fundAccountValidationId) {
		try {
			return ResponseEntity.ok(accountService.verifyAccountValidation(fundAccountValidationId));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}

<<<<<<< HEAD
	@ApiOperation(value = "Activate or Deactivate Fund account.")
=======
	@Operation(description =  "Activate or Deactivate Fund account.")
>>>>>>> Manish
	@GetMapping("/razorPayFundAccountActivateOrDeactivate/{fundAccountId}/{active}")
	public ResponseEntity<?> fundAccountActivateOrDeactive(@PathVariable String fundAccountId,
			@PathVariable Boolean active) {
		try {
			return ResponseEntity.ok(accountService.activateOrDeactivateFundAccount(fundAccountId, active));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}

}
