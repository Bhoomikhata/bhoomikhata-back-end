package com.example.bhoomikhata.clients;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;

import org.springframework.stereotype.Component;

@Component
public class AwsSNSClient {

        public static final String AWS_ACCESS_KEY_ID = "aws.accessKeyId";
        public static final String AWS_SECRET_KEY = "aws.secretKey";

        public PublishResult sendSingleSMS(String message, String phoneNumber) {
                BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials("AKIAYVTCZOZKYBEIGM5T",
                                "EKLZh2yEsWX0gWTfy/3SFdwChttqE+23qd3s2YUd");
                AmazonSNS snsClient = AmazonSNSClient.builder().withRegion(Regions.AP_SOUTH_1)
                                .withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials)).build();
                Map<String, MessageAttributeValue> smsAttributes = new HashMap<>();
                smsAttributes.put("AWS.SNS.SMS.SenderID",
                                new MessageAttributeValue().withStringValue("BHOOMIKHATA").withDataType("String"));
                smsAttributes.put("AWS.SNS.SMS.SMSType",
                                new MessageAttributeValue().withStringValue("Transactional").withDataType("String"));
                return snsClient.publish(new PublishRequest().withMessage(message).withPhoneNumber(phoneNumber)
                                .withMessageAttributes(smsAttributes));
        }
}
