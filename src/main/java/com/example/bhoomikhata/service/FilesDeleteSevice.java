package com.example.bhoomikhata.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bhoomikhata.model.BussinesModel;
import com.example.bhoomikhata.model.FilesStorageModel;
import com.example.bhoomikhata.repository.FilesStorageRepository;

@Service
public class FilesDeleteSevice {
	@Autowired
	private FilesStorageRepository filesRepository;

	@Autowired
	private ProfileService profileService;

	//** Delete profile Gallery Image **//
	public String deleteGalleryImageById(String imageId) {
		String response = "Image delete failed.";
		boolean result = false;
		Optional<FilesStorageModel> fsmIs = filesRepository.findById(imageId);

		if (fsmIs.isPresent()) {
			String businessId = fsmIs.get().getBusinessId();
			Optional<BussinesModel> bmIs = profileService.getBusinessProfile(businessId);
			if (bmIs.isPresent()) {
				filesRepository.deleteById(imageId);
				BussinesModel bm = bmIs.get();
				List<String> gallery = bm.getImageGallery();
				if (gallery != null)
					gallery.remove(imageId);

				bm.setImageGallery(gallery);
				profileService.createOrUpdateBussinesProfile(bm);
				result = true;
			} else {
				response = "No business profile found for image ID:" + imageId + " in our record.";
			}

		} else {
			response = "Selected Image not found in our record.";
		}

		if (result) {
			response = "This image removed from your gallery.";
		}

		return response;
	}

}
