package com.example.bhoomikhata.service;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;

import org.springframework.stereotype.Service;

@Service
public class RazPayDateTimeService {

	public String getIndianDateTime() {
		ZonedDateTime zonedDtTi = LocalDateTime.now().atZone(ZoneId.of("Asia/Kolkata"));
		DateTimeFormatter formatDT = DateTimeFormatter.ofPattern("yyyy-MM-dd h:mm:ss a");
		return formatDT.format(zonedDtTi);
	}

	public String getIndianDateTime(String unixTime) {
		long unix_seconds = Long.parseLong(unixTime);
		// convert seconds to milliseconds
		Date date = new Date(unix_seconds * 1000L);
		// format of the date for transaction response..
		SimpleDateFormat jdf = new SimpleDateFormat("h:mm:s a, d-MMM-YYYY");
		jdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		return jdf.format(date);
	}
}
