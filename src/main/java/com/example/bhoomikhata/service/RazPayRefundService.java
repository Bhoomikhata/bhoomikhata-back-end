package com.example.bhoomikhata.service;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bhoomikhata.model.RazPayRefundResponseModel;
import com.example.bhoomikhata.model.RazPayTransactionModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.razorpay.RazorpayClient;
import com.razorpay.Refund;

@Service
public class RazPayRefundService {

	@Autowired
	RazorpayClient razorpay;

	@Autowired
	RazPayTransactionService transactionService;

	public RazPayTransactionModel createRefundByTransactionId(String transactionId) {
		RazPayRefundResponseModel refundResponse = new RazPayRefundResponseModel();
		List<String> statusLogRefund = null;
		RazPayTransactionModel tx = transactionService.getTransactionById(transactionId);
		try {
			/*
			 * if (tx == null) return refundResponse;
			 */

			statusLogRefund = tx.getStatusErrorLog();

			JSONObject refundRequest = new JSONObject();
			refundRequest.put("amount", tx.getRazorPayPayInAmount());
			refundRequest.put("speed", "normal");
			/*
			 * JSONObject notes = new JSONObject(); notes.put("notes_key_1",
			 * "Tea, Earl Grey, Hot"); notes.put("notes_key_2", "Tea, Earl Grey… decaf.");
			 * refundRequest.put("notes", notes);
			 */
			refundRequest.put("receipt", tx.getTransactionRefId());

			Refund refund = razorpay.payments.refund(tx.getRazorPayPaymentId(), refundRequest);

			if (refund != null) {
				ObjectMapper mapper = new ObjectMapper();
				refundResponse = mapper.readValue(refund.toJson().toString(), RazPayRefundResponseModel.class);
				tx.setRazorPayPaymentRefundId(refundResponse.getId());
				tx.setRazorPayPaymentRefundStatus(refundResponse.getStatus());
				statusLogRefund.add("SUCCESS : Refund Status : " + refundResponse.getStatus());
			}
		} catch (Exception e) {
			if (tx.getRazorPayPaymentRefundId() == null) {
				RazPayRefundResponseModel rrs = fetchRefundByTransactionId(tx.getTransactionRefId());
				if (rrs != null) {
					tx.setRazorPayPaymentRefundId(rrs.getId());
					tx.setRazorPayPaymentRefundStatus(rrs.getStatus());
				}
			}
			statusLogRefund.add("ERROR : " + e.getMessage());
		} finally {
			if (statusLogRefund != null) {
				tx.setStatusErrorLog(statusLogRefund);
				transactionService.saveTransactionDetails(tx);
			}
		}

		return tx;

	}

	public RazPayRefundResponseModel fetchRefundByTransactionId(String transactionId) {
		RazPayTransactionModel rtx = transactionService.getTransactionById(transactionId);
		String paymentId = rtx.getRazorPayPaymentId();

		JSONObject params = new JSONObject();
		params.put("count", "1");
		RazPayRefundResponseModel rResp = new RazPayRefundResponseModel();
		try {
			List<Refund> rzPayPaymentRespList = razorpay.payments.fetchAllRefunds(paymentId, params);
			ObjectMapper resPmapper = new ObjectMapper();
			rResp = resPmapper.readValue(rzPayPaymentRespList.get(0).toJson().toString(),
					RazPayRefundResponseModel.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return new RazPayRefundResponseModel();
		}

		return rResp;
	}
}
