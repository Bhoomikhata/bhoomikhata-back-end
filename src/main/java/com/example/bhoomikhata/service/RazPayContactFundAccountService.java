package com.example.bhoomikhata.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bhoomikhata.model.AccStatus;
import com.example.bhoomikhata.model.BussinesModel;
import com.example.bhoomikhata.model.ContactBankAccount;
import com.example.bhoomikhata.model.ContactModel;
import com.example.bhoomikhata.model.ContactPaymentAccount;
import com.example.bhoomikhata.model.ContactUpiAccount;
import com.example.bhoomikhata.model.RazPayBankAccountResponseModel;
import com.example.bhoomikhata.model.RazPayContactRequestDto;
import com.example.bhoomikhata.model.RazPayContactResponseModel;
import com.example.bhoomikhata.model.RazPayFundAccountResponseModel;
import com.example.bhoomikhata.model.RazPayFundAccountValidationResponse;
import com.example.bhoomikhata.repository.ProfileRepository;
import com.google.gson.Gson;

@Service
public class RazPayContactFundAccountService {

	@Autowired
	private RazPayAccountService accountService;

	@Autowired
	private ContactService contactService;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private ProfileRepository profileRepository;

	public RazPayContactResponseModel getOrCreateRazorPayContact(RazPayContactRequestDto contactDetails) {
		RazPayContactResponseModel contactResponse = null;

		contactResponse = accountService.fetchRazorPayContactByMobileNumber(contactDetails.getMobileNumber());
		if (contactResponse != null)
			return contactResponse;

		contactResponse = accountService.createRazPayContact(contactDetails);

		return contactResponse;
	}

	// razor pay contact and fund account for business profile //
	public BussinesModel getOrCreateRazPayContactAndAccountDetails(BussinesModel bussinesModel) {
		// create Contact on RazorPay
		if (bussinesModel != null) {
			if (bussinesModel.getName() != null && bussinesModel.getMobileNumber() != null) {
				RazPayContactRequestDto contatDetails = new RazPayContactRequestDto();
				contatDetails.setMobileNumber(bussinesModel.getMobileNumber());
				contatDetails.setName(bussinesModel.getName());
				contatDetails.setEmail(bussinesModel.getEmailId());
				RazPayContactResponseModel contact = null;
				contact = getOrCreateRazorPayContact(contatDetails);

				if (contact != null) {
					bussinesModel.setRazorpayContactId(contact.getId());
					bussinesModel.setRazorPayContactDetails(contact);
					if (bussinesModel.getRazorPayContactDetails() != null) {
						List<ContactPaymentAccount> accountDetails = new ArrayList<ContactPaymentAccount>();
						if (bussinesModel.getRazorpayPaymentAccountDetails() == null
								|| bussinesModel.getRazorpayPaymentAccountDetails().size() != bussinesModel
										.getRazorPayContactDetails().getNotes().size()) {
							accountDetails = getAccountDetailsForBusinessProfile(contact.getId());
							bussinesModel.setRazorpayPaymentAccountDetails(accountDetails);
						}
					}
				}
			}
		}

		return bussinesModel;
	}

	// razor pay contact and fund account for contact profile //
	public ContactModel getOrCreateRazPayContactAndAccountDetails(ContactModel contactModel) {
		// create Contact on RazorPay
		if (contactModel != null) {
			String mobileNumber = "";
			if (contactModel.getMobileNumber() == null && contactModel.getBussinessId() == null) {
				return contactModel;
			} else {
				if (contactModel.getMobileNumber() != null && contactModel.getMobileNumber().length() > 5) {
					mobileNumber = contactModel.getMobileNumber();
				} else {
					if (contactModel.getBussinessId() != null && contactModel.getBussinessId().length() > 5) {
						mobileNumber = contactModel.getBussinessId();
					}
				}

				if (mobileNumber.length() > 0) {
					RazPayContactRequestDto contatDetails = new RazPayContactRequestDto();
					contatDetails.setMobileNumber(mobileNumber);
					contatDetails.setName(contactModel.getName());
					contatDetails.setEmail(contactModel.getEmailId());
					RazPayContactResponseModel contact = null;
					contact = getOrCreateRazorPayContact(contatDetails);

					if (contact != null) {
						contactModel.setRazorpayContactId(contact.getId());
						contactModel.setRazorPayContactDetails(contact);
					}
				}
			}
		}

		return contactModel;
	}

	public RazPayContactResponseModel getRazorPayContactByCustomerId(String customerId) {
		RazPayContactResponseModel contact = null;

		Optional<ContactModel> contactProfile = contactService.getContactProfile(customerId);
		if (contactProfile.isEmpty())
			return null;

		if (contactProfile.get().getRazorPayContactDetails() != null) {
			contact = contactProfile.get().getRazorPayContactDetails();

			// contact =
			// accountService.getRazorPayContactById(contactProfile.get().getRazorpayContactId());
			if (contact != null)
				return contact;
		}

		// contact =
		// accountService.fetchRazorPayContactByMobileNumber(contactProfile.get().getMobileNumber());

		return contact;
	}

	public RazPayContactResponseModel getRazorPayContactByBusinessId(String businessId) {
		return accountService.fetchRazorPayContactByMobileNumber(businessId);
	}

	/* get list of fund accounts of a contact */
	public List<String> getFundAccountIdsByMobileNumber(String mobileNumber) {
		List<String> fundAccounts = new ArrayList<String>();
		RazPayContactResponseModel contact = null;
		String contactId = null;
		try {
			Optional<BussinesModel> profile = profileService.getBusinessProfile(mobileNumber);
			if (profile.isPresent()) {
				contactId = profile.get().getRazorpayContactId();
			} else {

				contact = accountService.fetchRazorPayContactByMobileNumber(mobileNumber);
				if (contact != null) {
					contactId = contact.getId();
				}
			}
			if (contactId == null)
				return fundAccounts;

			List<RazPayFundAccountResponseModel> accounts = accountService
					.fetchAllActiveFundAccountsOfContact(contactId);
			for (RazPayFundAccountResponseModel account : accounts) {
				fundAccounts.add(account.getId());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return fundAccounts;
	}

	/* fetch fund account details for profile */
	public List<ContactPaymentAccount> getAccountDetailsForBusinessProfile(String razorPayContactId) {
		List<ContactPaymentAccount> profilePaymentAccountDetails = new ArrayList<ContactPaymentAccount>();
		RazPayContactResponseModel razPayContact = null;
		List<RazPayFundAccountResponseModel> razPayFundAccounts = null;

		try {
			razPayContact = accountService.getRazorPayContactById(razorPayContactId);
			if (razPayContact == null) {
				return null;
			}

			razPayFundAccounts = accountService.fetchAllActiveFundAccountsOfContact(razorPayContactId);

			if (razPayFundAccounts == null) {
				return null;
			}

			Map<String, RazPayFundAccountValidationResponse> validationDetails = accountService
					.fetchFundAccountIdAndValidaionDetailsByContactId(razorPayContactId, razPayFundAccounts);

			razPayFundAccounts.sort(new Comparator<RazPayFundAccountResponseModel>() {

				@Override
				public int compare(RazPayFundAccountResponseModel o1, RazPayFundAccountResponseModel o2) {
					// TODO Auto-generated method stub
					return o1.getCreated_at().compareTo(o2.getCreated_at());
				}
			});

			Map<String, String> contactNotes = new HashMap<String, String>();
			contactNotes = razPayContact.getNotes();
			if (contactNotes.size() == 0 || contactNotes.size() != razPayFundAccounts.size()) {
				Map<String, String> contactNotesUpdate = new HashMap<String, String>();
				for (RazPayFundAccountResponseModel fa : razPayFundAccounts) {
					String vId = "NA";
					if (validationDetails.get(fa.getId()) != null)
						vId = validationDetails.get(fa.getId()).getId();

					contactNotesUpdate.put(fa.getId(), vId);
				}

				razPayContact.setNotes(contactNotesUpdate);
				razPayContact = accountService.updateRazPayContact(razPayContact);
			}
			if (contactNotes != null) {
				for (RazPayFundAccountResponseModel fundAcc : razPayFundAccounts) {
					ContactPaymentAccount accountDetail = new ContactPaymentAccount();
					if (fundAcc.getActive()) {
						accountDetail.setIsDefault(false);
						accountDetail.setFundAccountId(fundAcc.getId());
						String validationId = contactNotes.get(fundAcc.getId());

						accountDetail.setIsFundAccountActive(fundAcc.getActive());
						accountDetail.setAccountType(fundAcc.getAccount_type());

						if (fundAcc.getAccount_type().equalsIgnoreCase("vpa")) {
							accountDetail.setAccountDescription(
									razPayContact.getName() + "'s UPI address " + fundAcc.getVpa().getAddress() + ".");
							accountDetail.setUpiDetails(fundAcc.getVpa());
						} else {
							RazPayBankAccountResponseModel bankAccount = fundAcc.getBank_account();
							accountDetail.setAccountDescription(
									bankAccount.getName() + "'s " + bankAccount.getBank_name() + " Ac. No." + "xxxx-"
											+ bankAccount.getAccount_number().substring(
													bankAccount.getAccount_number().length() - 4,
													bankAccount.getAccount_number().length())
											+ ".");
							accountDetail.setBankDetails(fundAcc.getBank_account());
						}

						if (validationId != null && !validationId.equalsIgnoreCase("NA")) {
							accountDetail.setValidationId(razPayContact.getNotes().get(fundAcc.getId()).toString());
							RazPayFundAccountValidationResponse validationResponse = validationDetails
									.get(fundAcc.getId());
							if (validationResponse != null) {
								accountDetail.setIsValidationRequested(true);
								if (validationResponse.getStatus().equalsIgnoreCase("completed")) {
									if (validationResponse.getResults().getAccount_status()
											.equalsIgnoreCase("active")) {
										accountDetail.setIsBankOrUpiActive(true);
										// accountDetail.setValidationStatus("SUCCESS : Valid "
										// + (fundAcc.getAccount_type().equalsIgnoreCase("vpa") ? "UPI address."
										// : "Bank Account."));

										accountDetail.setValidationStatus(AccStatus.VERIFIED);
									} else if (validationResponse.getResults().getAccount_status()
											.equalsIgnoreCase("invalid")) {
										accountDetail.setIsBankOrUpiActive(false);
										/*
										 * accountDetail.setValidationStatus("FAILED : Invalid " +
										 * (fundAcc.getAccount_type().equalsIgnoreCase("vpa") ? "UPI address." :
										 * "Bnak Account.") + " Kindly delete this entry and add new one.");
										 */

										accountDetail.setValidationStatus(AccStatus.FAILED);
										accountDetail.setIsBankOrUpiActive(false);
									} else {
										accountDetail.setIsBankOrUpiActive(false);
										/*
										 * accountDetail.setValidationStatus(
										 * validationResponse.getResults().getAccount_status());
										 */
										accountDetail.setValidationStatus(AccStatus.PENDING);
									}
								} else if (validationResponse.getStatus().equalsIgnoreCase("created")) {
									accountDetail.setIsBankOrUpiActive(false);
									accountDetail.setIsValidationRequested(true);
									/*
									 * accountDetail.setValidationStatus(
									 * "PENDING : Waiting for verification process to complete.");
									 */
									accountDetail.setValidationStatus(AccStatus.PENDING);
								} else {
									if (validationResponse.getStatus().equalsIgnoreCase("failed")) {
										accountDetail.setIsBankOrUpiActive(false);
										accountDetail.setIsBankOrUpiActive(false);
										/*
										 * accountDetail.setValidationStatus(
										 * "FAILED : Internal Server Error. Try to verify again.");
										 */
										accountDetail.setValidationStatus(AccStatus.FAILED);
									}
								}
							}
						} else {
							accountDetail.setIsValidationRequested(false);
							accountDetail.setIsBankOrUpiActive(false);
							/*
							 * accountDetail.setValidationStatus(
							 * "PENDING : Kindly verify to receive payments on this account.");
							 */
							accountDetail.setValidationStatus(AccStatus.PENDING);
						}
						profilePaymentAccountDetails.add(accountDetail);
					}
				}
			}
		} catch (

		Exception e) {
			e.printStackTrace();
			return null;
		}

		return profilePaymentAccountDetails;
	}

	/*********************************
	 * Add UPI Account for business Profile
	 * 
	 * @throws Exception
	 *******************************************/
	// public ContactPaymentAccount addUpiAccountByBusinessId(ContactUpiAccount
	// upiDetails) throws Exception {
	public Object addUpiAccountByBusinessId(ContactUpiAccount upiDetails) throws Exception {
		JSONObject statusResponse = new JSONObject();
		ContactPaymentAccount accountDetails = new ContactPaymentAccount();
		RazPayFundAccountResponseModel fundAccount = null;
		Optional<BussinesModel> profile = null;
		try {

			if (upiDetails == null || upiDetails.getBusinessId() == null || upiDetails.getUpiAddress() == null)
				throw new Exception("Error : Please enter valid upi details.");

			if (upiDetails.getBusinessId().length() < 10)
				throw new Exception("Error : Business ID must be valid.");

			profile = profileService.getBusinessProfile(upiDetails.getBusinessId());
			if (profile.isEmpty())
				throw new Exception("Forbidden : Signup and create business profile on Bhoomi Khata first.");

			fundAccount = accountService.createUPIFundAccountForBusinessId(upiDetails);

			if (fundAccount == null)
				throw new Exception("Error : Internal Server Error. Please try again.");

			accountDetails = prepareAccountDetailsResponse(fundAccount);

			if (accountDetails.getValidationStatus().equals(AccStatus.FAILED)) {
				throw new Exception("Failed : Invalid upi Address.");
			} else {
				profile = profileRepository.findById(upiDetails.getBusinessId());
				if (profile.isPresent()) {
					BussinesModel exProfile = profile.get();
					RazPayContactResponseModel contactNewData = accountService
							.getRazorPayContactById(exProfile.getRazorpayContactId());

					List<ContactPaymentAccount> allAccountDetails = new ArrayList<ContactPaymentAccount>();
					if (exProfile.getRazorpayPaymentAccountDetails() == null || exProfile
							.getRazorpayPaymentAccountDetails().size() != contactNewData.getNotes().size()) {
						allAccountDetails = getAccountDetailsForBusinessProfile(exProfile.getRazorpayContactId());
						exProfile.setRazorpayPaymentAccountDetails(allAccountDetails);
						exProfile.setRazorPayContactDetails(contactNewData);
						profileRepository.save(exProfile);
					}

					if (accountDetails.getIsBankOrUpiActive()) {
						statusResponse.put("status", AccStatus.VERIFIED);
						statusResponse.put("info", accountDetails.getUpiDetails().getAddress()
								+ " verified and saved successfully. You can accept payments on this.");
					} else {
						statusResponse.put("status", AccStatus.PENDING);
						statusResponse.put("info", accountDetails.getUpiDetails().getAddress()
								+ " verification pending. You can only accept payments on this once verified.");
					}
				}
			}

		} catch (Exception e) {
			statusResponse.put("status", AccStatus.FAILED);
			if (e.getLocalizedMessage().contains("Invalid"))
				statusResponse.put("info", "Invalid upi address. Try again with a valid one.");
			else
				statusResponse.put("info", e.getLocalizedMessage().replace("BAD_REQUEST_ERROR:", ""));
		}

		Gson gson = new Gson();

		return gson.toJson(statusResponse.toMap());
	}

	/*********************************
	 * Add Bank Account for business Profile
	 * 
	 * @throws Exception
	 *******************************************/
	public Object addBankAccountByBusinessId(ContactBankAccount bankDetails) throws Exception {
		JSONObject statusResponse = new JSONObject();
		ContactPaymentAccount accountDetails = new ContactPaymentAccount();
		RazPayFundAccountResponseModel fundAccount = null;
		Optional<BussinesModel> profile = null;

		try {
			if (bankDetails == null || bankDetails.getBusinessId() == null || bankDetails.getBank_account() == null
					|| bankDetails.getBank_account().getAccount_number() == null
					|| bankDetails.getBank_account().getIfsc() == null
					|| bankDetails.getBank_account().getName() == null)
				throw new Exception("Error : Please enter valid bank account details.");

			if (bankDetails.getBusinessId().length() < 10)
				throw new Exception("Error : Business ID must be valid.");

			profile = profileService.getBusinessProfile(bankDetails.getBusinessId());
			if (profile.isEmpty())
				throw new Exception("Forbidden : Signup and create business profile on Bhoomi Khata first.");

			fundAccount = accountService.createBankFundAccountForBusinessId(bankDetails);

			if (fundAccount == null)
				throw new Exception("Error : Internal Server Error. Please try again.");

			accountDetails = prepareAccountDetailsResponse(fundAccount);

			if (accountDetails.getValidationStatus().equals(AccStatus.FAILED)) {
				throw new Exception("Failed : Invalid Bank Details.");
			} else {
				profile = profileRepository.findById(bankDetails.getBusinessId());
				if (profile.isPresent()) {
					BussinesModel exProfile = profile.get();
					RazPayContactResponseModel contactNewData = accountService
							.getRazorPayContactById(exProfile.getRazorpayContactId());

					List<ContactPaymentAccount> allAccountDetails = new ArrayList<ContactPaymentAccount>();
					if (exProfile.getRazorpayPaymentAccountDetails() == null || exProfile
							.getRazorpayPaymentAccountDetails().size() != contactNewData.getNotes().size()) {
						allAccountDetails = getAccountDetailsForBusinessProfile(exProfile.getRazorpayContactId());
						exProfile.setRazorpayPaymentAccountDetails(allAccountDetails);
						exProfile.setRazorPayContactDetails(contactNewData);
						profileRepository.save(exProfile);
					}

					if (accountDetails.getIsBankOrUpiActive()) {
						statusResponse.put("status", AccStatus.VERIFIED);
						statusResponse.put("info","Bank account No. "+ accountDetails.getBankDetails().getAccount_number()
								+ " verified and saved successfully. You can accept payments on this.");
					} else {
						statusResponse.put("status", AccStatus.PENDING);
						statusResponse.put("info", "Bank account No. "+ accountDetails.getBankDetails().getAccount_number()
								+ " verification pending. You can only accept payments on this once verified.");
					}
				}
			}

		} catch (Exception e) {
			statusResponse.put("status",AccStatus.FAILED);
			if (e.getLocalizedMessage().contains("Invalid"))
				statusResponse.put("info", "Invalid Bank Details. Try again with a valid one.");
			else
				statusResponse.put("info", e.getLocalizedMessage().replace("BAD_REQUEST_ERROR:", ""));
		}

		Gson gson = new Gson();

		return gson.toJson(statusResponse.toMap());
	}

	/*********************************
	 * prepare account details response
	 *******************************************/
	public ContactPaymentAccount prepareAccountDetailsResponse(RazPayFundAccountResponseModel fundAccountResponse) {
		ContactPaymentAccount accountDetail = new ContactPaymentAccount();
		RazPayContactResponseModel contact = null;

		accountDetail.setIsFundAccountActive(fundAccountResponse.getActive());
		accountDetail.setValidationId(fundAccountResponse.getAccountValidation().getId());
		accountDetail.setIsDefault(false);
		accountDetail.setFundAccountId(fundAccountResponse.getId());
		contact = accountService.getRazorPayContactById(fundAccountResponse.getContact_id());
		accountDetail.setIsFundAccountActive(fundAccountResponse.getActive());
		accountDetail.setAccountType(fundAccountResponse.getAccount_type());

		if (fundAccountResponse.getActive()) {

			String validationId = fundAccountResponse.getAccountValidation().getId();

			accountDetail.setIsFundAccountActive(fundAccountResponse.getActive());
			accountDetail.setAccountType(fundAccountResponse.getAccount_type());

			if (fundAccountResponse.getAccount_type().equalsIgnoreCase("vpa")) {
				accountDetail.setAccountDescription(
						contact.getName() + "'s UPI address " + fundAccountResponse.getVpa().getAddress() + ".");
				accountDetail.setUpiDetails(fundAccountResponse.getVpa());
			} else {
				RazPayBankAccountResponseModel bankAccount = fundAccountResponse.getBank_account();
				accountDetail.setAccountDescription(
						bankAccount.getName() + "'s " + bankAccount.getBank_name() + " Ac. No." + "xxxx-"
								+ bankAccount.getAccount_number().substring(
										bankAccount.getAccount_number().length() - 4,
										bankAccount.getAccount_number().length())
								+ ".");
				accountDetail.setBankDetails(fundAccountResponse.getBank_account());
			}

			if (validationId != null && !validationId.equalsIgnoreCase("NA")) {
				accountDetail.setValidationId(validationId);
				RazPayFundAccountValidationResponse validationResponse = fundAccountResponse.getAccountValidation();
				if (validationResponse != null) {
					accountDetail.setIsValidationRequested(true);
					if (validationResponse.getStatus().equalsIgnoreCase("completed")) {
						if (validationResponse.getResults().getAccount_status().equalsIgnoreCase("active")) {
							accountDetail.setIsBankOrUpiActive(true);
							accountDetail.setValidationStatus(AccStatus.VERIFIED);
						} else if (validationResponse.getResults().getAccount_status().equalsIgnoreCase("invalid")) {
							accountDetail.setIsBankOrUpiActive(false);
							accountDetail.setValidationStatus(AccStatus.FAILED);
							accountDetail.setIsBankOrUpiActive(false);
						} else {
							accountDetail.setIsBankOrUpiActive(false);
							accountDetail.setValidationStatus(AccStatus.PENDING);
						}
					} else if (validationResponse.getStatus().equalsIgnoreCase("created")) {
						accountDetail.setIsBankOrUpiActive(false);
						accountDetail.setIsValidationRequested(true);
						accountDetail.setValidationStatus(AccStatus.PENDING);
					} else {
						if (validationResponse.getStatus().equalsIgnoreCase("failed")) {
							accountDetail.setIsBankOrUpiActive(false);
							accountDetail.setIsBankOrUpiActive(false);
							accountDetail.setValidationStatus(AccStatus.FAILED);
						}
					}
				}
			} else {
				accountDetail.setIsValidationRequested(false);
				accountDetail.setIsBankOrUpiActive(false);
				accountDetail.setValidationStatus(AccStatus.PENDING);

			}
		} else {
			accountDetail.setBankDetails(fundAccountResponse.getBank_account());
			accountDetail.setUpiDetails(fundAccountResponse.getVpa());
			// accountDetail.setValidationStatus("Error : This Bank Or UPI is not active on
			// Bhoomi Khata.");
			accountDetail.setValidationStatus(AccStatus.FAILED);
			accountDetail.setIsBankOrUpiActive(false);
		}

		return accountDetail;
	}

}
