package com.example.bhoomikhata.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.bhoomikhata.model.BussinesModel;
import com.example.bhoomikhata.model.ContactBankAccount;
import com.example.bhoomikhata.model.ContactUpiAccount;
import com.example.bhoomikhata.model.RazPayBankAccountValidationRequest;
import com.example.bhoomikhata.model.RazPayContactAllResponse;
import com.example.bhoomikhata.model.RazPayContactRequestDto;
import com.example.bhoomikhata.model.RazPayContactRequestModel;
import com.example.bhoomikhata.model.RazPayContactResponseModel;
import com.example.bhoomikhata.model.RazPayFundAccount;
import com.example.bhoomikhata.model.RazPayFundAccountResponseModel;
import com.example.bhoomikhata.model.RazPayFundAccountValidationResponse;
import com.example.bhoomikhata.model.RazPayFundAccountValidationResponseAll;
import com.example.bhoomikhata.model.RazPayFundAccountsByContactResponse;
import com.example.bhoomikhata.model.RazPayUPIModel;
import com.example.bhoomikhata.model.RazPayUPIValidationRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.razorpay.FundAccount;
import com.razorpay.RazorpayClient;

@Service
public class RazPayAccountService {

	@Value("${razorpay.payout.account}")
	private String razPayAccNo;

	@Value("${razorpay.contact.url}")
	private String contactUrl;

	@Value("${razorpay.fundaccount.url}")
	private String fundAccountUrl;

	@Autowired
	private RazorpayClient razorpayClient;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private RazPayContactFundAccountService contactFundAccountService;

	@Autowired
	private ProfileService profileService;

	/************************** // RazorPay Contact // *************************/

	public RazPayContactResponseModel createRazPayContact(RazPayContactRequestDto newContact) {
		RazPayContactResponseModel response = null;
		RazPayContactRequestModel contactModel = new RazPayContactRequestModel();
		try {
			contactModel.setName(newContact.getName());
			contactModel.setContact(newContact.getMobileNumber());
			contactModel.setEmail(newContact.getEmail());
			contactModel.setReference_id("bhoomikhata_" + newContact.getMobileNumber());
			contactModel.setType("customer");
			contactModel.setNotes(new HashMap<String, String>());

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<RazPayContactRequestModel> requestEntity = new HttpEntity<RazPayContactRequestModel>(
					contactModel, headers);
			ResponseEntity<String> resp = restTemplate.postForEntity(contactUrl, requestEntity, String.class);

			Gson gson = new Gson();
			if (resp != null)
				response = gson.fromJson(resp.getBody(), RazPayContactResponseModel.class);
			// response = rep.getBody();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return response;
	}

	// *****************************update contact on razorPay
	// ****************************
	public RazPayContactResponseModel updateRazPayContact(RazPayContactResponseModel existingContact) {
		RazPayContactResponseModel response = existingContact;
		RazPayContactRequestModel contactModel = new RazPayContactRequestModel();
		try {
			contactModel.setName(existingContact.getName());
			contactModel.setContact(existingContact.getContact());

			if (existingContact.getEmail() == null || existingContact.getEmail().equalsIgnoreCase("string"))
				contactModel.setEmail("");
			else
				contactModel.setEmail(existingContact.getEmail());

			String mobileNumber = existingContact.getContact();
			contactModel.setReference_id("bhoomikhata_"
					+ (mobileNumber.contains("+91") ? mobileNumber.replace("+91", "").trim() : mobileNumber));
			contactModel.setType("customer");
			contactModel.setNotes(existingContact.getNotes());

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			RazPayContactResponseModel razorPayResponse = null;

			Gson gson = new Gson();
			String jb = gson.toJson(contactModel);

			HttpEntity<String> requestEntity = new HttpEntity<String>(jb, headers);
			String apiResponse = restTemplate.patchForObject(contactUrl + existingContact.getId(), requestEntity,
					String.class);
			razorPayResponse = gson.fromJson(apiResponse, RazPayContactResponseModel.class);

			if (razorPayResponse != null)
				response = razorPayResponse;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	/*********************************
	 * fetch razor pay contact by id
	 *******************************************/
	public RazPayContactResponseModel getRazorPayContactById(String contactId) {
		RazPayContactResponseModel response = null;
		try {
			ResponseEntity<String> objectResponse = restTemplate.getForEntity(contactUrl + contactId, String.class);
			if (objectResponse.hasBody()) {
				Gson gson = new Gson();
				response = gson.fromJson(objectResponse.getBody(), RazPayContactResponseModel.class);
			}
			return response;
		} catch (Exception ex) {
			throw ex;
		}
	}

	/*********************************
	 * fetch razor pay active contact by Mobile number
	 *******************************************/
	public RazPayContactResponseModel fetchRazorPayContactByMobileNumber(String mobileNumber) {
		RazPayContactResponseModel response = null;
		RazPayContactAllResponse contactResponse = null;
		try {
			String queryString = contactUrl + "?active=1&reference_id=bhoomikhata_" + mobileNumber;

			ResponseEntity<String> objectResponse = restTemplate.getForEntity(queryString.trim(), String.class);

			if (objectResponse.hasBody()) {
				Gson gson = new Gson();
				contactResponse = gson.fromJson(objectResponse.getBody(), RazPayContactAllResponse.class);
				if (contactResponse != null) {
					if (contactResponse.getItems().size() > 0)
						response = contactResponse.getItems().get(0);
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return response;
	}

	/*********************************
	 * fetch razor pay all contacts
	 *******************************************/
	public List<RazPayContactResponseModel> fetchAllRazorPayContacts() {
		List<RazPayContactResponseModel> allContacts = new ArrayList<RazPayContactResponseModel>();
		List<RazPayContactResponseModel> contacts = null;
		try {
			int skip = 0;
			int count = 50;

			do {
				String queryString = contactUrl + "?count=" + count + "&skip=" + skip;
				ResponseEntity<RazPayContactAllResponse> contactListResponse = restTemplate.getForEntity(queryString,
						RazPayContactAllResponse.class);

				if (contactListResponse.hasBody()) {
					contacts = contactListResponse.getBody().getItems();
					allContacts.addAll(contacts);
				}
				skip = skip + count;

			} while (contacts.size() > 0);

			return allContacts;
		} catch (Exception ex) {
			throw ex;
		}
	}

	/*********************************
	 * fetch razor pay all Active contacts
	 *******************************************/
	public List<RazPayContactResponseModel> fetchAllActiveRazorPayContacts() {
		List<RazPayContactResponseModel> allContacts = new ArrayList<RazPayContactResponseModel>();
		List<RazPayContactResponseModel> contacts = null;
		try {
			int skip = 0;
			int count = 50;

			do {
				String queryString = contactUrl + "?active=1&count=" + count + "&skip=" + skip;
				ResponseEntity<RazPayContactAllResponse> contactListResponse = restTemplate.getForEntity(queryString,
						RazPayContactAllResponse.class);

				if (contactListResponse.hasBody()) {
					contacts = contactListResponse.getBody().getItems();
					allContacts.addAll(contacts);
				}
				skip = skip + count;

			} while (contacts.size() > 0);

			return allContacts;
		} catch (Exception ex) {
			throw ex;
		}
	}

	/**************************
	 * // RazorPay FundAccount //
	 * 
	 * @throws Exception
	 *************************/
	/*********************************
	 * BankAccount
	 *******************************************/

	/**************************************************************************
	 * Create Fund Account for Bank Account for business Profile and update in
	 * contact's notes
	 ***************************************************************************/
	public RazPayFundAccountResponseModel createBankFundAccountForBusinessId(ContactBankAccount bankDetails)
			throws Exception {
		RazPayFundAccountResponseModel fundAccResponse = null;
		BussinesModel contactProfile = null;

		try {
			if (profileService.getBusinessProfile(bankDetails.getBusinessId()).isPresent()) {
				contactProfile = profileService.getBusinessProfile(bankDetails.getBusinessId()).get();
				if (contactProfile.getRazorpayContactId() == null)
					return fundAccResponse;

			} else {
				return fundAccResponse;
			}

			RazPayContactResponseModel razorPayContact = fetchRazorPayContactByMobileNumber(
					bankDetails.getBusinessId());

			if (razorPayContact == null)
				return fundAccResponse;

			Map<String, String> notes = new HashMap<String, String>();
			notes = razorPayContact.getNotes();

			JSONObject request = new JSONObject();
			request.put("contact_id", contactProfile.getRazorpayContactId());
			request.put("account_type", "bank_account");
			request.put("bank_account", new JSONObject(bankDetails.getBank_account()));
			FundAccount fundAccount = razorpayClient.fundAccount.create(request);

			ObjectMapper mapper = new ObjectMapper();
			fundAccResponse = mapper.readValue(fundAccount.toJson().toString(), RazPayFundAccountResponseModel.class);

			if (fundAccResponse != null) {
				// validate Fund Account
				fundAccResponse = validatAndVerifyFundAccount(fundAccResponse);

				if (fundAccResponse.getAccountValidation() != null) {
					RazPayFundAccountValidationResponse validation = fundAccResponse.getAccountValidation();

					if (validation.getStatus().equalsIgnoreCase("created")) {
						Thread.sleep(5000);
						validation = verifyAccountValidation(fundAccResponse.getAccountValidation().getId());

					}

					if (validation.getStatus().equalsIgnoreCase("completed")) {
						if (validation.getResults().getAccount_status().equalsIgnoreCase("invalid"))
							if (activateOrDeactivateFundAccount(fundAccResponse.getId(), false) != null)
								fundAccResponse = activateOrDeactivateFundAccount(fundAccResponse.getId(), false);
					}

					fundAccResponse.setAccountValidation(validation);
				}

				if (fundAccResponse.getActive()) {
					notes.put(fundAccResponse.getId(), fundAccResponse.getAccountValidation().getId());
					razorPayContact.setNotes(notes);
					updateRazPayContact(razorPayContact);
				}
			}

		} catch (Exception ex) {
			throw ex;
		}
		return fundAccResponse;
	}

	/*********************************
	 * fetch razor pay fund account by fund account id
	 *******************************************/
	public RazPayFundAccountResponseModel fetchRpayFundAccountById(String funAccountId) {
		RazPayFundAccountResponseModel fundAccResponse = null;

		if (funAccountId == null)
			return null;

		try {
			FundAccount fundAccount = razorpayClient.fundAccount.fetch(funAccountId);
			ObjectMapper mapper = new ObjectMapper();
			fundAccResponse = mapper.readValue(fundAccount.toJson().toString(), RazPayFundAccountResponseModel.class);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return fundAccResponse;
	}

	/*********************************
	 * fetch razor pay all fund accounts
	 *******************************************/
	public Object fetchAllRpayFundAccounts() throws Exception {
		try {
			return restTemplate.getForObject(fundAccountUrl, Object.class);
		} catch (Exception ex) {
			throw ex;
		}
	}

	/*********************************
	 * fetch All razor pay fund accounts by contact id
	 *******************************************/
	public List<RazPayFundAccountResponseModel> fetchAllActiveFundAccountsOfContact(String razorPayContactId)
			throws Exception {
		List<RazPayFundAccountResponseModel> listOfAccounts = new ArrayList<RazPayFundAccountResponseModel>();
		RazPayFundAccountsByContactResponse fundAccountsResponse = null;

		try {

			String urlToFetch = fundAccountUrl + "?contact_id=" + razorPayContactId.trim();
			fundAccountsResponse = restTemplate.getForObject(urlToFetch, RazPayFundAccountsByContactResponse.class);

			if (fundAccountsResponse == null)
				return null;

			for (RazPayFundAccountResponseModel fa : fundAccountsResponse.getItems()) {
				if (fa.getActive())
					listOfAccounts.add(fa);
			}

			// listOfAccounts = fundAccountsResponse.getItems();

		} catch (Exception ex) {
			listOfAccounts = null;
			throw ex;
		}

		return listOfAccounts;
	}

	/*********************************
	 * VPA (UPI ID)
	 * 
	 * @throws RazorpayException
	 *******************************************/

	/*********************************
	 * Create Fund Account for UPI with customerId
	 *******************************************/
	public RazPayFundAccountResponseModel createFundAccountUPIforCustomerId(RazPayUPIModel upi) throws Exception {
		RazPayFundAccountResponseModel fundAccResponse = null;

		if (upi == null)
			return fundAccResponse;

		// get razor pay contact details from customerId
		RazPayContactResponseModel contact = contactFundAccountService
				.getRazorPayContactByCustomerId(upi.getCustomerId());
		if (contact == null)
			return fundAccResponse;

		JSONObject request = new JSONObject();
		request.put("account_type", "vpa");
		request.put("contact_id", contact.getId());
		request.put("vpa", new JSONObject().put("address", upi.getUpiAddress()));

		FundAccount fundAccount = razorpayClient.fundAccount.create(request);

		ObjectMapper mapper = new ObjectMapper();
		fundAccResponse = mapper.readValue(fundAccount.toJson().toString(), RazPayFundAccountResponseModel.class);

		// validate UPI details
		if (fundAccResponse != null) {
			fundAccResponse = validatAndVerifyFundAccount(fundAccResponse);
		}

		return fundAccResponse;
	}

	/**************************************************************************
	 * Create Fund Account for UPI for business Profile and update in contact's
	 * notes
	 ***************************************************************************/
	public RazPayFundAccountResponseModel createUPIFundAccountForBusinessId(ContactUpiAccount upiDetails)
			throws Exception {
		RazPayFundAccountResponseModel fundAccResponse = null;
		BussinesModel contactProfile = null;

		if (profileService.getBusinessProfile(upiDetails.getBusinessId()).isPresent()) {
			contactProfile = profileService.getBusinessProfile(upiDetails.getBusinessId()).get();
			if (contactProfile.getRazorpayContactId() == null)
				return fundAccResponse;

		} else {
			return fundAccResponse;
		}

		RazPayContactResponseModel razorPayContact = fetchRazorPayContactByMobileNumber(upiDetails.getBusinessId());

		if (razorPayContact == null)
			return fundAccResponse;

		Map<String, String> notes = new HashMap<String, String>();
		notes = razorPayContact.getNotes();

		// creating fund account
		JSONObject request = new JSONObject();
		request.put("account_type", "vpa");
		request.put("contact_id", razorPayContact.getId());
		request.put("vpa", new JSONObject().put("address", upiDetails.getUpiAddress()));
		FundAccount fundAccount = razorpayClient.fundAccount.create(request);

		ObjectMapper mapper = new ObjectMapper();
		fundAccResponse = mapper.readValue(fundAccount.toJson().toString(), RazPayFundAccountResponseModel.class);

		if (fundAccResponse != null) {
			// validate Fund Account
			fundAccResponse = validatAndVerifyFundAccount(fundAccResponse);

			if (fundAccResponse.getAccountValidation() != null) {
				RazPayFundAccountValidationResponse validation = fundAccResponse.getAccountValidation();

				if (validation.getStatus().equalsIgnoreCase("created")) {
					Thread.sleep(5000);
					validation = verifyAccountValidation(fundAccResponse.getAccountValidation().getId());

				}

				if (validation.getStatus().equalsIgnoreCase("completed")) {
					if (validation.getResults().getAccount_status().equalsIgnoreCase("invalid"))
						if (activateOrDeactivateFundAccount(fundAccResponse.getId(), false) != null)
							fundAccResponse = activateOrDeactivateFundAccount(fundAccResponse.getId(), false);
				}

				fundAccResponse.setAccountValidation(validation);
			}

			if (fundAccResponse.getActive()) {
				notes.put(fundAccResponse.getId(), fundAccResponse.getAccountValidation().getId());
				razorPayContact.setNotes(notes);
				updateRazPayContact(razorPayContact);
			}
		}
		return fundAccResponse;
	}

	/*********************************
	 * Account Validation (Bank Account / UPI)
	 * 
	 * @throws Exception *
	 * 
	 *******************************************/
	// ***********validate and verify fundAccount *************************
	public RazPayFundAccountResponseModel validatAndVerifyFundAccount(
			RazPayFundAccountResponseModel fundAccountDetails) {
		try {
			if (fundAccountDetails != null) {
				RazPayFundAccountValidationResponse validationResult = validateBankAccountUpiAccount(
						fundAccountDetails.getId());

				if (validationResult != null) {
					RazPayFundAccountValidationResponse validationCheck = null;
					validationCheck = verifyAccountValidation(validationResult.getId());
					if (validationCheck != null)
						fundAccountDetails.setAccountValidation(validationCheck);
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return fundAccountDetails;
	}

	// ***********validate fundAccount *************************
	public RazPayFundAccountValidationResponse validateBankAccountUpiAccount(String razorPayFundAccount)
			throws Exception {

		RazPayFundAccountResponseModel fundAccount = fetchRpayFundAccountById(razorPayFundAccount);

		String fundAccountType = fundAccount.getAccount_type();

		RazPayFundAccountValidationResponse validationResponse = null;

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<?> requestEntity = null;

		if (fundAccountType.equalsIgnoreCase("bank_account")) {
			RazPayBankAccountValidationRequest request = new RazPayBankAccountValidationRequest();
			request.setAccount_number(razPayAccNo);
			request.setFund_account(new RazPayFundAccount(razorPayFundAccount));
			request.setCurrency("INR");
			request.setAmount(100);

			requestEntity = new HttpEntity<RazPayBankAccountValidationRequest>(request, headers);
		} else if (fundAccountType.equalsIgnoreCase("vpa")) {
			RazPayUPIValidationRequest payUPIValidationRequest = new RazPayUPIValidationRequest();
			payUPIValidationRequest.setAccount_number(razPayAccNo);
			RazPayFundAccount upiAccId = new RazPayFundAccount(razorPayFundAccount);
			payUPIValidationRequest.setFund_account(upiAccId);

			requestEntity = new HttpEntity<RazPayUPIValidationRequest>(payUPIValidationRequest, headers);
		}

		if (requestEntity != null) {
			ResponseEntity<RazPayFundAccountValidationResponse> responseEntity = restTemplate.postForEntity(
					fundAccountUrl + "validations", requestEntity, RazPayFundAccountValidationResponse.class);

			validationResponse = responseEntity.getBody();
			// update in contact notes//
			if (validationResponse != null) {
				RazPayContactResponseModel contact = getRazorPayContactById(
						validationResponse.getFund_account().getContact_id());
				Map<String, String> notes = new HashMap<String, String>();
				if (contact != null) {
					notes = contact.getNotes();
					if (notes.size() > 0) {
						notes.replace(razorPayFundAccount, validationResponse.getId());
						contact.setNotes(notes);
						updateRazPayContact(contact);
					}
				}
			}
		}
		return validationResponse;
	}

	// ***********verify fundAccount validation *************************
	public RazPayFundAccountValidationResponse verifyAccountValidation(String fundAccountValidationId) {
		RazPayFundAccountValidationResponse validationResponse = null;

		try {
			String url = fundAccountUrl + "validations/" + fundAccountValidationId;
			ResponseEntity<RazPayFundAccountValidationResponse> responseEntity = restTemplate.getForEntity(url,
					RazPayFundAccountValidationResponse.class);

			if (responseEntity.hasBody())
				validationResponse = responseEntity.getBody();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return validationResponse;
	}

	// *************get all fund account validation by contact id************//
	public Map<String, RazPayFundAccountValidationResponse> fetchFundAccountIdAndValidaionDetailsByContactId(
			String razorPaycontactId, List<RazPayFundAccountResponseModel> availableFundAccounts) {
		Map<String, RazPayFundAccountValidationResponse> result = new HashMap<String, RazPayFundAccountValidationResponse>();
		List<RazPayFundAccountValidationResponse> validationResponseAll = new ArrayList<RazPayFundAccountValidationResponse>();
		try {

			if (availableFundAccounts == null) {
				return result;
			}

			if (availableFundAccounts.size() == 0) {
				return result;
			}

			for (RazPayFundAccountResponseModel fa : availableFundAccounts) {
				if (fa.getActive()) {
					result.put(fa.getId(), null);
				}
			}

			int count = 20;
			int skip = 0;
			List<RazPayFundAccountValidationResponse> validationResponse = null;
			do {
				String url = fundAccountUrl + "validations?account_number=" + razPayAccNo + "&count=" + count + "&skip="
						+ skip;
				validationResponse = new ArrayList<RazPayFundAccountValidationResponse>();
				ResponseEntity<RazPayFundAccountValidationResponseAll> responseEntity = restTemplate.getForEntity(url,
						RazPayFundAccountValidationResponseAll.class);

				if (responseEntity.hasBody()) {
					validationResponse = responseEntity.getBody().getItems();
					validationResponseAll.addAll(validationResponse);
				}
				skip = skip + 20;

			} while (validationResponse.size() == count);

			for (RazPayFundAccountValidationResponse vlResp : validationResponseAll) {
				if (vlResp.getFund_account().getActive()) {
					if (vlResp.getFund_account().getContact_id().equalsIgnoreCase(razorPaycontactId)) {
						result.replace(vlResp.getFund_account().getId(), vlResp);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	/**************************************************************************
	 * Activate or Deactivate Fund Account
	 ***************************************************************************/
	public RazPayFundAccountResponseModel activateOrDeactivateFundAccount(String fundAccountId,
			Boolean activeOrDeactive) {
		RazPayFundAccountResponseModel response = null;
		String url = fundAccountUrl + fundAccountId;
		try {
			JSONObject activeDeactive = new JSONObject();
			activeDeactive.put("active", activeOrDeactive);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> requestEntity = new HttpEntity<String>(activeDeactive.toString(), headers);

			response = restTemplate.patchForObject(url, requestEntity, RazPayFundAccountResponseModel.class);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return response;
	}

}
