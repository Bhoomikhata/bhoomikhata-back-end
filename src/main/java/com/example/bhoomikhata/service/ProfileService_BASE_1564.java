package com.example.bhoomikhata.service;

import java.util.List;
import java.util.Optional;

import com.example.bhoomikhata.model.BussinesModel;
import com.example.bhoomikhata.repository.ProfileRepository;
import com.example.bhoomikhata.utils.Utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfileService {

    private ProfileRepository profileRepository;

    @Autowired
    public ProfileService(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    public Optional<BussinesModel> getBusinessProfile(String bussinessId) {

        return profileRepository.findById(bussinessId);
    }

    public BussinesModel createOrUpdateBussinesProfile(BussinesModel bussinesModel) {
        bussinesModel.setId(bussinesModel.getMobileNumber());
        return profileRepository
                .save(Utility.setCreatedAndUpdatedDate(Utility.setCreatedAndUpdatedDate(bussinesModel)));
    }

    public Boolean deleteBussinesProfile(String bussinessId) {
        try {
            profileRepository.deleteById(bussinessId);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }

    public Boolean deleteAllBussinesProfile() {
        try {
            profileRepository.deleteAll();
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }

    }

    public List<BussinesModel> getAllBusinessProfile() {

        return profileRepository.findAll();
    }

}
