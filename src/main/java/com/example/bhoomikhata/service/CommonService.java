package com.example.bhoomikhata.service;

import java.time.OffsetDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.example.bhoomikhata.model.BussinesModel;
import com.example.bhoomikhata.model.ContactModel;
import com.example.bhoomikhata.model.ExtendedAddressModel;
import com.example.bhoomikhata.model.ExtendedTransactionModel;
import com.example.bhoomikhata.model.PaymentStatus;
import com.example.bhoomikhata.model.TransactionModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommonService {

    private ContactService contactService;
    private TransactionService transactionService;
    private Optional<ContactModel> contactProfile;
    private ProfileService profileService;

    @Autowired
    public CommonService(ContactService contactService, TransactionService transactionService,
            ProfileService profileService) {
        this.contactService = contactService;
        this.transactionService = transactionService;
        this.profileService = profileService;
    }

    public List<ExtendedAddressModel> getAllAdressesOfBussiness(String bussinessId) {
        List<ContactModel> contacts = contactService.getAllContactProfile().stream()
                .filter(e -> e.getId().split("_")[1].equals(bussinessId))
                .collect(Collectors.toList());
        return getExtendedAdressModelFromContactModel(contacts, bussinessId);
    }

    public List<ExtendedAddressModel> getExtendedAdressModelFromContactModel(List<ContactModel> contacts,
            String bussinessId) {

        List<ExtendedAddressModel> adressModels = new ArrayList<>();
        if (contacts == null || contacts.isEmpty())
            return new ArrayList<>();
        contacts.stream().forEach(contact -> {
            ExtendedAddressModel adressModel = new ExtendedAddressModel();
            adressModel.setId(contact.getId());
            if (!contact.getBussinessId().equals(bussinessId)) {
                Optional<BussinesModel> bussinessProfile = profileService.getBusinessProfile(contact.getBussinessId());
                if (bussinessProfile.isPresent()) {
                    adressModel.setLabel(bussinessProfile.get().getName());
                } else {
                    adressModel.setLabel(contact.getMobileNumber());
                }
            } else {
                adressModel.setLabel(contact.getName());
            }
            if (contact.getAdressModel() != null) {
                adressModel.setPostalAddress(contact.getAdressModel().getPostalAddress());
                adressModel.setLattitude(contact.getAdressModel().getLattitude());
                adressModel.setLongitude(contact.getAdressModel().getLongitude());
            }
            List<TransactionModel> transactions = transactionService.getTransactionDetails(bussinessId,
                    contact.getId());

            Collections.sort(transactions, (o1, o2) -> o2.getDueDate().compareTo(o1.getDueDate()));
            int overdueAmount = 0;
            if (!transactions.isEmpty()) {
                for (TransactionModel transactionModel : transactions) {
                    if (Boolean.TRUE.equals(transactionModel.getIsCredit()))
                        overdueAmount += transactionModel.getAmount();
                    else
                        overdueAmount -= transactionModel.getAmount();
                }
                adressModel.setDueAmount(overdueAmount);
                adressModel.setDueDate(transactions.get(0).getDueDate().format(DateTimeFormatter.ISO_LOCAL_DATE));

                if (overdueAmount > 0) {
                    if (transactions.get(0).getDueDate().isAfter(OffsetDateTime.now()) && Period
                            .between(OffsetDateTime.now().toLocalDate(), transactions.get(0).getDueDate().toLocalDate())
                            .getDays() < transactions.get(0).getDueDateDuration()) {
                        adressModel.setColour(PaymentStatus.YELLOW);
                    } else if (transactions.get(0).getDueDate().isBefore(OffsetDateTime.now())) {
                        adressModel.setColour(PaymentStatus.RED);
                    } else {
                        adressModel.setColour(PaymentStatus.GREEN);
                    }
                } else {
                    adressModel.setColour(PaymentStatus.PURPLE);
                }
            } else {
                adressModel.setDueAmount(overdueAmount);
                adressModel.setDueDate(
                        OffsetDateTime.parse("2099-12-12T13:30:30+05:00").format(DateTimeFormatter.ISO_LOCAL_DATE));
                adressModel.setColour(PaymentStatus.PURPLE);
            }
            adressModels.add(adressModel);
        });
        return adressModels;
    }

    public ExtendedTransactionModel getAllTransactionsOfBussinessByContact(String bussinessId, String contactId) {

        ExtendedTransactionModel exTransactionModel = new ExtendedTransactionModel();
        if (contactService.getContactProfile(contactId).isPresent()) {
            contactProfile = contactService.getContactProfile(contactId);
            ContactModel contact = contactProfile.get();
            exTransactionModel.setId(contact.getId());
            if (!contact.getBussinessId().equals(bussinessId)) {
                Optional<BussinesModel> bussinessProfile = profileService.getBusinessProfile(contact.getBussinessId());
                if (bussinessProfile.isPresent()) {
                    exTransactionModel.setName(bussinessProfile.get().getName());
                } else {
                    exTransactionModel.setName(contact.getMobileNumber());
                }
            } else {
                exTransactionModel.setName(contact.getName());
            }

            if (contact.getAdressModel() != null) {
                exTransactionModel.setPostalAddress(contact.getAdressModel().getPostalAddress());
            }
            exTransactionModel.setCountryCode(contact.getCountryCode());
            List<TransactionModel> transactions = transactionService.getTransactionDetails(bussinessId,
                    contact.getId());
            Collections.sort(transactions, new Comparator<TransactionModel>() {
                @Override
                public int compare(TransactionModel o1, TransactionModel o2) {
                    // TODO Auto-generated method stub
                    return o2.getCreatedDate().compareTo(o1.getCreatedDate());
                }
            });
            Map<OffsetDateTime, Set<TransactionModel>> map = transactions.stream()
                    .collect(Collectors.groupingBy(TransactionModel::getCreatedDate, Collectors.toSet()));
            exTransactionModel.setTransactions(map);
            // Create a new TreeMap
            Map<OffsetDateTime, Set<TransactionModel>> treeMap = new TreeMap<>();

            // Pass the hashMap to putAll() method
            treeMap.putAll(map);
            exTransactionModel.setTransactions(treeMap);
            int overdueAmount = 0;
            if (!transactions.isEmpty()) {
                for (TransactionModel transactionModel : transactions) {
                    if (Boolean.TRUE.equals(transactionModel.getIsCredit()))
                        overdueAmount += transactionModel.getAmount();
                    else
                        overdueAmount -= transactionModel.getAmount();
                }
                exTransactionModel.setOverdueAmount(overdueAmount);
                exTransactionModel
                        .setDueDate(transactions.get(0).getDueDate().format(DateTimeFormatter.ISO_LOCAL_DATE));
            } else {
                exTransactionModel.setOverdueAmount(overdueAmount);
                exTransactionModel.setDueDate(
                        OffsetDateTime.parse("2099-12-12T13:30:30+05:00").format(DateTimeFormatter.ISO_LOCAL_DATE));
            }
        }

        return exTransactionModel;
    }

    public List<ExtendedAddressModel> getAllAdressesOfBussinessHavingValidMapping(String bussinessId) {
        List<ExtendedAddressModel> extendedAdressModels = getAllAdressesOfBussiness(bussinessId);
        return extendedAdressModels.stream().filter(e -> e.getLattitude() != null && e.getLongitude() != null)
                .collect(Collectors.toList());
    }

    public ContactModel createOrUpdateContactProfile(ContactModel contactModel) {
        return contactService.createOrUpdateContactProfile(contactModel);
    }

    public TransactionModel createOrUpdateTransactionDetails(TransactionModel transactionModel) {
        return transactionService.createOrUpdateTransactionDetails(transactionModel);

    }

    public List<ExtendedAddressModel> getAllAdressesOfSuppliers(String bussinessId) {
        List<ContactModel> contacts = contactService.getAllContactProfile().stream()
                .filter(e -> e.getId().split("_")[0].equals(bussinessId))
                .collect(Collectors.toList());
        return getExtendedAdressModelFromContactModel(contacts, bussinessId);
    }
}
