package com.example.bhoomikhata.service;

import java.util.List;
import java.util.Optional;

import com.example.bhoomikhata.model.AccessKeyModel;
import com.example.bhoomikhata.model.AuthModel;
import com.example.bhoomikhata.repository.AuthRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class AuthService {

    @Autowired
    private AuthRepository authRepository;

    public String saveAuthModel(AuthModel authModel) {
        authRepository.save(authModel);
        return "succees";
    }

    public Optional<AuthModel> getAuthModel(String id) {
        return authRepository.findById(id);
    }

    public String updateAuthModel(AuthModel authModel) {
        authRepository.save(authModel);
        return "success";
    }

    public String deleteAuthModel(String id) {
        authRepository.findById(id).ifPresent(entity -> authRepository.delete(entity));
        return "sucess";
    }

    public List<AuthModel> getAllAuthModel() {
        return authRepository.findAll();
    }

    public String getAccessKey(AccessKeyModel accessKeyModel) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("grant_type", accessKeyModel.getGrantType());
        map.add("username", accessKeyModel.getUsername());
        map.add("password", accessKeyModel.getPassword());
        map.add("scope", accessKeyModel.getScope());
        map.add("client_id", accessKeyModel.getClientId());
        map.add("client_secret", accessKeyModel.getClientSecret());

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                "http://localhost:8180/auth/realms/test/protocol/openid-connect/token", HttpMethod.POST, request,
                String.class);
        return response.getBody();
    }
}
