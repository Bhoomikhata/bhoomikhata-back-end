package com.example.bhoomikhata.service;

import java.security.SignatureException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.bhoomikhata.model.RazPayCardPaymentModel;
import com.example.bhoomikhata.model.RazPayFundAccountResponseModel;
import com.example.bhoomikhata.model.RazPayOrderModel;
import com.example.bhoomikhata.model.RazPayPaymentInDataModel;
import com.example.bhoomikhata.model.RazPayPaymentModel;
import com.example.bhoomikhata.model.RazPayTransactionModel;
import com.example.bhoomikhata.model.RazPayTransactionModelResponse;
import com.example.bhoomikhata.model.RazPayTrxResult;

@Service
public class RazPayPaymentInService {

	@Value("${razorpay.secret}")
	private String razorPaySecret;

	@Value("${razorpay.payout.mode}")
	private String razorPayMode;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private RazPayTransactionService transactionService;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private RazPayOrderService orderService;

	@Autowired
	private RazPayPayOutService payOutService;

	@Autowired
	private RazPayRefundService refundService;

	@Autowired
	private RazPayAccountService accountService;

	@Autowired
	private RazPayPaymentTransactionService paymentDetailsService;

	@Autowired
	private RazPayDateTimeService dateTimeService;

	// Fetch Payments by ID
	public RazPayPaymentModel fetchRazorPayPaymentById(String id) {
		RazPayPaymentModel paymentModel = null;
		try {
			String paymentUrl = "https://api.razorpay.com/v1/payments/" + id + "?expand[]=card";
			ResponseEntity<RazPayPaymentModel> responseEntity = restTemplate.getForEntity(paymentUrl,
					RazPayPaymentModel.class);

			if (responseEntity.hasBody())
				paymentModel = responseEntity.getBody();

		} catch (Exception e) {
			return paymentModel;
		}

		return paymentModel;
	}

	// payment signature check
	public boolean checkPaymentSignature(RazPayPaymentInDataModel paymentData) throws SignatureException {
		boolean result = false;

		String payData = paymentData.getRazorpay_order_id() + "|" + paymentData.getRazorpay_payment_id();
		String generatedSignatuere = RazPayPaymentSignatureService.calculateRFC2104HMAC(payData, razorPaySecret);

		// System.out.println(generatedSignatuere);
		// System.out.println(paymentData.getRazorpay_signature());

		if (paymentData.getRazorpay_signature().equals(generatedSignatuere))
			result = true;

		return result;
	}

	// *****************************************************************************************
	// ************************* Create Transaction Response
	// ************************************

	public RazPayTransactionModelResponse buildTransactionResponse(RazPayTransactionModel trxnModel,
			List<String> trxnStatusDetails) {
		RazPayTransactionModelResponse trxResp = new RazPayTransactionModelResponse();

		String trxStatus = "SUCCESS";
		String trxTimeAndDate = "";
		String moneyFromName = "you";
		String moneyToName = "recipient";
		String moneyFromAcount = "your account";
		String moneyToAccount = "recipient's account";

		List<RazPayTrxResult> statusDetailsList = new ArrayList<RazPayTrxResult>();

		try {

			trxTimeAndDate = trxnModel.getTransactionStart();

			moneyFromName = trxnModel.getPaymentOrder().getNotes().get("from_name");
			moneyToName = trxnModel.getPaymentOrder().getNotes().get("to_name");

			RazPayPaymentModel paymentInResp = trxnModel.getPaymentInResponse();

			if (paymentInResp == null)
				paymentInResp = fetchRazorPayPaymentById(trxnModel.getRazorPayPaymentId());

			if (paymentInResp != null) {
				switch (paymentInResp.getMethod()) {
				case "card":
					RazPayCardPaymentModel card = paymentInResp.getCard();
					if (card != null) {
						moneyFromAcount = card.getNetwork() + " " + card.getType() + " " + card.getEntity()
								+ " XXXX-XXXX-XXXX-" + card.getLast4() + " of " + card.getIssuer() + " bank.";
					} else {
						moneyFromAcount = "card";
					}

					break;
				case "netbanking":
					moneyFromAcount = paymentInResp.getBank();
					break;
				case "wallet":
					moneyFromAcount = paymentInResp.getWallet();
					break;
				case "upi":
					moneyFromAcount = paymentInResp.getVpa();
					break;
				}

				if (paymentInResp.getCreated_at() != null) {
					trxTimeAndDate = dateTimeService.getIndianDateTime(paymentInResp.getCreated_at());
				}

			}

			RazPayFundAccountResponseModel fundAcc = null;

			if (trxnModel.getRazorPayToFundAccountId() != null) {
				fundAcc = trxnModel.getPaymentToFundAccount();
				if (fundAcc == null)
					fundAcc = accountService.fetchRpayFundAccountById(trxnModel.getRazorPayToFundAccountId());
			}

			if (fundAcc != null) {
				if (fundAcc.getAccount_type().equalsIgnoreCase("bank_account")) {
					String accNo = fundAcc.getBank_account().getAccount_number();
					moneyToAccount = "Bnak " + fundAcc.getBank_account().getBank_name() + " Account No. XXXX-"
							+ accNo.substring(accNo.length() - 4);
				} else if (fundAcc.getAccount_type().equalsIgnoreCase("vpa")) {
					moneyToAccount = "UPI Id " + fundAcc.getVpa().getAddress();
				}
			}

			if (trxnModel.getStatusErrorLog().toString().contains("Failed")) {
				trxStatus = "FAILED";
			} else if (trxnModel.getStatusErrorLog().toString().contains("FORBIDDEN")) {
				trxStatus = "FORBIDDEN";
			}

			trxResp.setTrxRefId(trxnModel.getTransactionRefId());
			trxResp.setBusinessId(trxnModel.getFromBid());
			trxResp.setAmount(paymentInResp.getAmount() / 100);
			trxResp.setTimeDate(trxTimeAndDate);
			trxResp.setFromName(moneyFromName);
			trxResp.setFromAccount(moneyFromAcount);
			trxResp.setToName(moneyToName);
			trxResp.setToAccount(moneyToAccount);
			trxResp.setTrxStatus(trxStatus);

			for (String sd : trxnStatusDetails) {
				String[] stsArr = sd.split(":");
				if (stsArr.length == 2) {
					String txStat = stsArr[0];
					String txMsg = stsArr[1];
					String fianlMessage = txMsg.trim();
					if (txMsg.contains("PAYER_NAME")) {
						txMsg = txMsg.replaceAll("PAYER_NAME", moneyFromName);
						fianlMessage = txMsg;
					}
					if (txMsg.contains("RECIPIENT_NAME")) {
						txMsg = txMsg.replaceAll("RECIPIENT_NAME", moneyToName);
						fianlMessage = txMsg;
					}
					if (txMsg.contains("PAYER_ACCOUNT")) {
						txMsg = txMsg.replaceAll("PAYER_ACCOUNT", moneyFromAcount);
						fianlMessage = txMsg;
					}
					if (txMsg.contains("RECIPIENT_ACCOUNT")) {
						txMsg = txMsg.replaceAll("RECIPIENT_ACCOUNT", moneyToAccount);
						fianlMessage = txMsg;
					}

					RazPayTrxResult resultBuilder = RazPayTrxResult.tXResult(txStat.contains("SUCCESS") ? true : false,
							fianlMessage);
					statusDetailsList.add(resultBuilder);
				}
			}

			trxResp.setTrxStatusDetails(statusDetailsList);
			paymentDetailsService.savePaymentDetails(trxResp);

		} catch (Exception e) {
			trxResp.setTrxRefId(trxnModel.getTransactionRefId());
			trxResp.setTrxStatus("FAILED");

			for (String sd : trxnStatusDetails) {
				String[] stsArr = sd.split(":");
				if (stsArr.length == 2) {
					RazPayTrxResult resultBuilder = RazPayTrxResult
							.tXResult(stsArr[0].contains("SUCCESS") ? true : false, stsArr[1]);
					statusDetailsList.add(resultBuilder);
				}
			}

			trxResp.setTrxStatusDetails(statusDetailsList);
			// e.printStackTrace();
			return trxResp;
		}

		return trxResp;
	}

	// **********************************************************************************************************
	// ******************************Payment In verify, saveData and Initiate
	// PayOut***********************
	// ********* with contactId included in Order and fundAccountId included in
	// paymentCheckoutResponse
	// *************************************version 2.0 ************************
	// ****************************************************************************************************
	public RazPayTransactionModelResponse savePaymentInSuccessDataWithFundAccountId(
			RazPayPaymentInDataModel paymentCheckoutResponse) {
		boolean signatureVarified = false;
		// RazPayRefundResponseModel refund = null;
		String razorPayPayoutStaus = "processing";

		final String moneyFromName = "PAYER_NAME";
		final String moneyToName = "RECIPIENT_NAME";
		final String moneyFromAcount = "PAYER_ACCOUNT";
		final String moneyToAccount = "RECIPIENT_ACCOUNT";

		RazPayTransactionModel tx = transactionService
				.getTransactionByOrderId(paymentCheckoutResponse.getRazorpay_order_id());
		List<String> transactionResult = new ArrayList<String>();

		RazPayOrderModel order = orderService.fetchOrdersById(paymentCheckoutResponse.getRazorpay_order_id());

		if (tx == null) {
			if (order == null) {
				RazPayTransactionModelResponse txErrResponse = new RazPayTransactionModelResponse();
				txErrResponse.setTrxStatus("FAILED");
				List<RazPayTrxResult> statusDetailsList = new ArrayList<RazPayTrxResult>();
				statusDetailsList.add(RazPayTrxResult.tXResult(false,
						"No Payment details available. Make the payment and try again.."));
				txErrResponse.setTrxStatusDetails(statusDetailsList);
				return txErrResponse;
			} else {
				tx = new RazPayTransactionModel();
				tx.setTransactionRefId(order.getReceipt());
				tx.setPaymentOrder(order);
				tx.setFromBid(order.getNotes().get("from_mobile"));
				tx.setRazorPayOrderId(order.getId());
				tx.setToBid(order.getNotes().get("to_mobile"));
				List<String> trxnStatusUpdate = new ArrayList<String>();
				trxnStatusUpdate.add("Warning : No transaction details available. Fetching data from Razor pay");
				tx.setStatusErrorLog(trxnStatusUpdate);
				tx = transactionService.saveTransactionDetails(tx);
			}
		}

		if (razorPayMode.equalsIgnoreCase("live"))
			razorPayPayoutStaus = "processed";

		List<String> statusLog = tx.getStatusErrorLog();

		try {

			// fetch order and update transaction-details ............

			if (order != null) {
				// ORDER AVAILABLE
				tx.setRazorPayOrderStatus(order.getStatus());
				tx.setRazorPayPayInAmount(order.getAmount_paid());
				tx.setFromBid(order.getNotes().get("from_mobile"));
				tx.setToBid(order.getNotes().get("to_mobile"));
				// .........................$$....................
				if (paymentCheckoutResponse.getPayToFundAccountId() != null)
					tx.setRazorPayToFundAccountId(paymentCheckoutResponse.getPayToFundAccountId());
				tx.setRazorPayToContactId(order.getNotes().get("toRazorPayConatctId"));

				tx = transactionService.saveTransactionDetails(tx);
			} else {
				// NO ORDER AVAILABLE
				// code..................
				// ........................
			}

			// fetch payment details and update transaction-details .......
			RazPayPaymentModel razPayPayment = fetchRazorPayPaymentById(
					paymentCheckoutResponse.getRazorpay_payment_id());
			if (razPayPayment != null) {
				tx.setPaymentInResponse(razPayPayment);
				tx.setRazorPayPaymentId(razPayPayment.getId());
				tx.setRazorPayPaymentStatus(razPayPayment.getStatus());
				tx.setRazorPayPaymentMethod(razPayPayment.getMethod());
				if (razPayPayment.getRefund_status() != null) {
					tx.setRazorPayPaymentRefundStatus(razPayPayment.getRefund_status());
					tx.setRazorPayPaymentRefundId(
							refundService.fetchRefundByTransactionId(tx.getTransactionRefId()).getId());
				}
				tx = transactionService.saveTransactionDetails(tx);
			}

			transactionResult.add("SUCCESS :Payment Started by " + moneyFromName + ".");
			tx.setRazorPaySignature(paymentCheckoutResponse.getRazorpay_signature());

			if (razPayPayment != null) {
				if (tx.getRazorPayPayOutStatus() != null) {
					if (tx.getRazorPayPayOutStatus().equals(razorPayPayoutStaus)) {
						transactionResult.add("FORBIDDEN :Frobidden, This payment is already completed. Stay Away ...");
						RazPayTransactionModel txErr = new RazPayTransactionModel();
						txErr = tx;
						txErr.setStatusErrorLog(transactionResult);
						return buildTransactionResponse(txErr, transactionResult);
					}
				}

				tx.setRazorPayPaymentStatus(razPayPayment.getStatus());
				tx.setRazorPayPaymentMethod(razPayPayment.getMethod());
			}

			// fetch order status
			if (order != null) {
				tx.setRazorPayOrderStatus(order.getStatus());
				if (order.getStatus().equals("paid"))
					transactionResult.add("SUCCESS :Money deducted from " + moneyFromAcount + ".");
				else {
					if (!tx.getStatusErrorLog().toString().contains("PENDING"))
						transactionResult.add("PENDING :Money not received yet. Waiting for your bank's confimation.");
					return buildTransactionResponse(tx, transactionResult);
				}
			}
			// Verify payment checkout response.....
			signatureVarified = checkPaymentSignature(paymentCheckoutResponse);

			if (signatureVarified) {

				tx.setRazorPaySinatureVarified(true);
				statusLog.add("SUCCESS :Payment Received Successfully And Signature is Varified.");

				tx.setRazorPayPayInAmount(razPayPayment.getAmount());

				transactionService.saveTransactionDetails(tx);

				// tx = payOutService.payOut(tx.getTransactionRefId(), transactionResult);
				// tx = payOutService.payOutNew(tx.getTransactionRefId(), transactionResult);

				tx = payOutService.payOutNewWithFundAccountId(tx.getTransactionRefId(), transactionResult);
				statusLog = tx.getStatusErrorLog();

				if (tx.getRazorPayPayOutId() != null && tx.getRazorPayPayOutStatus() != null) {
					// for test only... processed
					if (tx.getRazorPayPayOutStatus().equals(razorPayPayoutStaus)) {
						transactionResult.add(
								"SUCCESS :Money sent to " + moneyToName + "'s " + moneyToAccount + " successfully.");
						transactionResult.add("SUCCESS :Payment Completed.");
					} else if (tx.getRazorPayPayOutStatus().equals("failed")) {
						// *********** Issue Refund If PayOut Failed......
						if (razPayPayment.getStatus().equals("refunded")) {
							transactionResult.add("SUCCESS :Payment refund prcessed Already there is Nothing to do.");
						} else {
							transactionResult.add("SUCCESS :Refund process started.");
							tx = refundService.createRefundByTransactionId(tx.getTransactionRefId());
							statusLog = tx.getStatusErrorLog();
							if (tx.getRazorPayPaymentRefundStatus().equals("processed")) {
								transactionResult
										.add("SUCCESS :Refund prcessed successfully to " + moneyFromAcount + ".");
							} else {
								transactionResult.add(
										"Failed :Internal SEVER Error. We have issued a refund. Money will be creditd to "
												+ moneyFromAcount + " shortly. Sorry for the inconvenience...");
							}
						}
					} else {
						transactionResult.add("Failed :Money has been transfered to " + moneyToAccount
								+ " but bank has not confirmed yet. If transaction fails we'll issue refund.");
					}
				} else {
					// *********** Issue Refund If PayOut Exception Occurred......
					if (razPayPayment.getStatus().equals("refunded")) {
						transactionResult.add("SUCCESS :Payment refund prcessed Already there is Nothing to do.");
					} else {
						transactionResult.add("SUCCESS :Refund process started.");
						tx = refundService.createRefundByTransactionId(tx.getTransactionRefId());
						statusLog = tx.getStatusErrorLog();
						if (tx.getRazorPayPaymentRefundStatus().equals("processed")) {
							transactionResult.add("SUCCESS :Refund prcessed successfully to " + moneyFromAcount + ".");
						} else {
							transactionResult.add("Failed :Internal SEVER Error. We have issued a refund to your "
									+ moneyFromAcount
									+ ". Money will be creditd to your account shortly. Sorry for the inconvenience...");
						}
					}
				}

			} else {
				statusLog.add(
						"ERROR-Signature : Payment Received Successfully But Signature Do NOT Match. FRAUD Posibility immediate action required.");
				transactionResult.add("FRAUD Detected : Are you sure you are doing the right thing.");
				tx.setStatusErrorLog(statusLog);
			}

		} catch (Exception e) {
			// e.printStackTrace();
			statusLog.add("ERROR PayIN : " + e.getMessage());
			transactionResult.add("Failed :In case Money deducted from your account " + moneyFromAcount
					+ ", will be refund shortly.");
		} finally {
			tx.setStatusErrorLog(statusLog);
			transactionService.saveTransactionDetails(tx);
		}

		return buildTransactionResponse(tx, transactionResult);
	}

}
