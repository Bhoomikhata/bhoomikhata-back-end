package com.example.bhoomikhata.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.example.bhoomikhata.model.BussinesModel;
import com.example.bhoomikhata.model.ContactPaymentAccount;
import com.example.bhoomikhata.model.RazPayFundAccountResponseModel;
import com.example.bhoomikhata.model.RazPayOrderModel;
import com.example.bhoomikhata.model.RazPayPayOutModel;
import com.example.bhoomikhata.model.RazPayPayOutResponseModel;
import com.example.bhoomikhata.model.RazPayTransactionModel;
import com.example.bhoomikhata.model.RazPayTransactionAdminResponseModel;

@Service
public class RazPayPayOutService {
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private RazPayTransactionService transactionService;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private RazPayAccountService accountService;

	@Autowired
	private RazPayDateTimeService dateTimeService;

	@Autowired
	private RazPayOrderService orderService;

	@Value("${razorpay.payout.url}")
	private String payOutUrl;

	@Value("${razorpay.payout.account}")
	private String payOutFromAccount;

	public RazPayTransactionModel payOut(String txRefNo, List<String> transactionResult) {
		RazPayTransactionModel tx = null;
		RazPayFundAccountResponseModel fundAccount = null;
		List<String> statusLog = new ArrayList<String>();

		// String result = "Payment Failed...";
		try {
			transactionResult.add("SUCCESS : Money transfer started.");
			statusLog.add("SUCCESS PayOut : Payout started.");

			tx = transactionService.getTransactionById(txRefNo);

			if (tx == null) {
				throw new Exception(txRefNo + " is not a valid transaction reference ID.");
			}
			statusLog = tx.getStatusErrorLog();

			RazPayPayOutModel sendMoneyTo = new RazPayPayOutModel();
			sendMoneyTo.setAccount_number(payOutFromAccount);

			String fundAccountId = tx.getRazorPayToFundAccountId();

			fundAccount = accountService.fetchRpayFundAccountById(fundAccountId);

			if (fundAccount == null) {
				statusLog.add("SUCCESS PayOut : Fund account " + fundAccountId + " not available in razor pay.");
				RazPayOrderModel order = orderService.fetchOrdersById(tx.getRazorPayOrderId());
				String fundAccountsInOrder = order.getNotes().get("FUND_ACCOUNTS");
				if (fundAccountsInOrder != null) {
					statusLog.add("SUCCESS PayOut : Checking Fund account in Order Notes.");
					if (fundAccountsInOrder.length() > 3) {
						statusLog.add("SUCCESS PayOut : Fund account available in Order Notes.");

						if (fundAccountsInOrder.contains(":")) {
							statusLog.add("SUCCESS PayOut : Multiple Fund accounts available in Order Notes.");
							String[] fundAccs = fundAccountsInOrder.split(":");
							fundAccountId = fundAccs[0];
							statusLog.add("SUCCESS PayOut : Fund account collected from Order Notes.");
						} else {
							fundAccountId = fundAccountsInOrder;
							statusLog.add("SUCCESS PayOut : Fund account collected from Order Notes.");
						}
					}
				}
			}

			// ***** Fetch FundAccount ******
			fundAccount = accountService.fetchRpayFundAccountById(fundAccountId);
			if (fundAccount == null) {
				throw new Exception("Fund account " + fundAccountId + " not available in razor pay.");
			}

			statusLog.add("SUCCESS PayOut : Fund account " + fundAccountId + " verified on razor pay.");

			if (!fundAccount.getActive()) {
				throw new Exception("Fund account " + fundAccountId + " is not Active. Stopping Payout process.");
			}

			statusLog.add("SUCCESS PayOut : Fund account " + fundAccountId + " is Active. Initiating Payout process.");

			sendMoneyTo.setFund_account_id(fundAccountId);
			sendMoneyTo.setAmount(tx.getRazorPayPayInAmount());
			sendMoneyTo.setCurrency("INR");

			// ***** Fetch FundAccount Type ******
			String payoutAccountType = fundAccount.getAccount_type();

			String payOutMode = "IMPS";

			// ******** Check fundAccount type bank_account or VPA **** And Set PayOut
			// Mode...
			if (payoutAccountType.equalsIgnoreCase("vpa"))
				payOutMode = "UPI";

			sendMoneyTo.setMode(payOutMode);
			sendMoneyTo.setPurpose("payout");
			sendMoneyTo.setReference_id(tx.getTransactionRefId());

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<RazPayPayOutModel> requestEntity = new HttpEntity<RazPayPayOutModel>(sendMoneyTo, headers);
			ResponseEntity<RazPayPayOutResponseModel> responseEntity = restTemplate.postForEntity(payOutUrl,
					requestEntity, RazPayPayOutResponseModel.class);

			RazPayPayOutResponseModel payOutResponse = new RazPayPayOutResponseModel();
			if (responseEntity.hasBody())
				payOutResponse = responseEntity.getBody();

			transactionResult.add("SUCCESS : Money transfer processed.");

			statusLog.add("SUCCESS PayOut : Payout Successfully Completed.");
			tx.setRazorPayToFundAccountId(payOutResponse.getFund_account_id());
			tx.setRazorPayPayOutId(payOutResponse.getId());
			tx.setRazorPayPayOutAmount(payOutResponse.getAmount());
			tx.setRazorPayPayOutStatus(payOutResponse.getStatus());
			tx.setTransactionEnd(dateTimeService.getIndianDateTime());
			tx.setStatusErrorLog(statusLog);

		} catch (Exception e) {
			// ***** fetch payOut details...
			statusLog.add("ERROR PayOUT : " + e.getMessage());
			tx.setStatusErrorLog(statusLog);
			transactionResult.add("Failed : Money transfer failed.");
			// System.out.println(e.getMessage());
		} finally {
			tx.setStatusErrorLog(statusLog);
			transactionService.saveTransactionDetails(tx);
		}

		return tx;
	}

	// **********************************************************************************************************
	// ******************************PayOUT with fundAccount included in
	// RazPayPaymentInDataModel
	// ******************************************************************************************************
	// *************************************version 2.0
	// **********************************************************************************************************
	public RazPayTransactionModel payOutNewWithFundAccountId(String txRefNo, List<String> transactionResult) {
		RazPayTransactionModel tx = transactionService.getTransactionById(txRefNo);

		List<String> statusLog = tx.getStatusErrorLog();
		RazPayFundAccountResponseModel paymentToFundAccount = null;

		try {

			// ***** Fetch FundAccount Details ******
			paymentToFundAccount = accountService.fetchRpayFundAccountById(tx.getRazorPayToFundAccountId().trim());

			tx.setPaymentToFundAccount(paymentToFundAccount);

			if (paymentToFundAccount != null) {
				transactionResult.add(
						"SUCCESS : " + (paymentToFundAccount.getAccount_type().equalsIgnoreCase("vpa") ? "UPI account"
								: "Bank Account") + " verified.");
				transactionResult.add("SUCCESS : Money Transfer started.");

			} else {
				transactionResult
						.add("Failed : NO verified bank or upi account linked on Bhoomi Khata by RECIPIENT_NAME.");
				throw new Exception();
			}

			transactionResult.add("SUCCESS : Sending Money to RECIPIENT_NAME "
					+ (paymentToFundAccount.getAccount_type().equalsIgnoreCase("vpa") ? "UPI account"
							: "Bank Account"));

			RazPayPayOutModel sendMoneyTo = new RazPayPayOutModel();
			sendMoneyTo.setAccount_number(payOutFromAccount);
			sendMoneyTo.setFund_account_id(paymentToFundAccount.getId());
			sendMoneyTo.setAmount(tx.getRazorPayPayInAmount());
			sendMoneyTo.setCurrency("INR");

			// ***** get FundAccount Type ******
			String payoutAccountType = paymentToFundAccount.getAccount_type();

			// ******** Check fundAccount type bank_account or VPA **** And Set PayOut mode
			String payOutMode = "IMPS";
			if (payoutAccountType.equalsIgnoreCase("vpa"))
				payOutMode = "UPI";

			sendMoneyTo.setMode(payOutMode);
			sendMoneyTo.setPurpose("payout");
			sendMoneyTo.setReference_id(tx.getTransactionRefId());

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<RazPayPayOutModel> requestEntity = new HttpEntity<RazPayPayOutModel>(sendMoneyTo, headers);
			ResponseEntity<RazPayPayOutResponseModel> responseEntity = restTemplate.postForEntity(payOutUrl,
					requestEntity, RazPayPayOutResponseModel.class);

			RazPayPayOutResponseModel payOutResponse = new RazPayPayOutResponseModel();
			if (responseEntity.hasBody())
				payOutResponse = responseEntity.getBody();

			// transactionResult.add("SUCCESS : Payout process completed.");

			statusLog.add(
					"SUCCESS PayOut : Payout Successfully Completed. To FundAccount : " + paymentToFundAccount.getId());
			tx.setRazorPayToFundAccountId(payOutResponse.getFund_account_id());
			tx.setRazorPayPayOutId(payOutResponse.getId());
			tx.setRazorPayPayOutAmount(payOutResponse.getAmount());
			tx.setRazorPayPayOutStatus(payOutResponse.getStatus());
			tx.setTransactionEnd(dateTimeService.getIndianDateTime());
			tx.setStatusErrorLog(statusLog);

		} catch (Exception e) {
			// ***** fetch payOut details...
			statusLog.add("ERROR PayOUT : " + e.getMessage());
			tx.setStatusErrorLog(statusLog);
			transactionResult.add("Failed : Money transfer failed.");
		} finally {
			tx.setStatusErrorLog(statusLog);
			transactionService.saveTransactionDetails(tx);
		}

		return tx;
	}

	// **********************************************************************
	// Fetch PayOut details from RazorPay //
	// ***********************************************************************
	public RazPayPayOutResponseModel fetchPayout(String payOutId) {
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			ResponseEntity<RazPayPayOutResponseModel> responseEntity = restTemplate.getForEntity(payOutUrl + payOutId,
					RazPayPayOutResponseModel.class);
			return responseEntity.getBody();
		} catch (HttpStatusCodeException ex) {
			throw ex;
		}
	}

	// **********************************************************************
	// get razorPay payOut to FundAccount from profile by BusinessId//
	// ***********************************************************************
	public RazPayFundAccountResponseModel checkForVarifiedDefaultFundAccountId(String businessId) {
		RazPayFundAccountResponseModel fundAccount = null;
		Optional<BussinesModel> businessModel = profileService.getBusinessProfile(businessId);

		if (businessModel.isPresent()) {
			List<ContactPaymentAccount> paymentAccounts = businessModel.get().getRazorpayPaymentAccountDetails();
			if (!paymentAccounts.isEmpty()) {
				for (ContactPaymentAccount contactPaymentAccount : paymentAccounts) {
					// for Test Mode Only
					if (contactPaymentAccount.getIsDefault())
						fundAccount = accountService.fetchRpayFundAccountById(contactPaymentAccount.getFundAccountId());

					// for Production Mode
					/*
					 * if (contactPaymentAccount.getIsDefault() &&
					 * contactPaymentAccount.getIsVarified()) fundAccountId =
					 * contactPaymentAccount.getAccountDetails().getId();
					 */
				}
			}
		}
		return fundAccount;
	}

//**************************************************************
	// for transactions only
	public RazPayTransactionAdminResponseModel fetchPayOutTransactionByRefrenceId(String refId) {
		String url = "https://api.razorpay.com/v1/transactions/";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			ResponseEntity<RazPayTransactionAdminResponseModel> responseEntity = restTemplate.getForEntity(url + refId,
					RazPayTransactionAdminResponseModel.class);
			return responseEntity.getBody();
		} catch (HttpStatusCodeException ex) {
			throw ex;
		}
	}

}
