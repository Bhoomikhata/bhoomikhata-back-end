package com.example.bhoomikhata.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bhoomikhata.model.RazPayTransactionModel;
import com.example.bhoomikhata.repository.RazPayTransactionRepository;

@Service
public class RazPayTransactionService {

	@Autowired
	private RazPayTransactionRepository transactionRepository;

	public RazPayTransactionModel getTransactionById(String txId) {
		RazPayTransactionModel transaction = null;
		if (transactionRepository.existsById(txId))
			transaction = transactionRepository.findById(txId).get();
		return transaction;
	}

	public List<RazPayTransactionModel> getAllTransactions() {
		return transactionRepository.findAll();
	}

	public RazPayTransactionModel getTransactionByOrderId(String orderId) {
		RazPayTransactionModel transaction = transactionRepository.findByRazorPayOrderId(orderId);
		return transaction;
	}

	public RazPayTransactionModel saveTransactionDetails(RazPayTransactionModel tx) {
		return transactionRepository.save(tx);
	}

	public RazPayTransactionModel updateTransactionDetails(RazPayTransactionModel tx) {
		RazPayTransactionModel newTx = new RazPayTransactionModel();
		
		if (transactionRepository.existsById(tx.getTransactionRefId()))
			newTx = transactionRepository.save(tx);

		return newTx;
	}

	public String getNewId() {
		UUID id = UUID.randomUUID();
		while (transactionRepository.existsById(id.toString())) {
			id = UUID.randomUUID();
		}
		return id.toString();
	}

	// for test only......
	public boolean deleteAllTransactionLogs() {
		boolean deleteResponse = false;
		transactionRepository.deleteAll();
		if (transactionRepository.count() == 0)
			deleteResponse = true;
		return deleteResponse;
	}

}
