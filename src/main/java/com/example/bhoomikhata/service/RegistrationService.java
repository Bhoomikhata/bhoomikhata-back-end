package com.example.bhoomikhata.service;

import java.util.SplittableRandom;

import com.amazonaws.services.sns.model.PublishResult;
import com.example.bhoomikhata.clients.AwsSNSClient;
import com.example.bhoomikhata.model.OTPModel;
import com.example.bhoomikhata.repository.RegistrationRepository;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService {

    private AwsSNSClient awsSNSClient;

    private RegistrationRepository registrationRepository;

    @Autowired
    public RegistrationService(AwsSNSClient awsSNSClient, RegistrationRepository registrationRepository) {
        this.awsSNSClient = awsSNSClient;
        this.registrationRepository = registrationRepository;
    }

    public boolean generateOTP(String mobileNumber, String countryCode) {

        StringBuilder otpValue = new StringBuilder();
        SplittableRandom splittableRandom = new SplittableRandom();
        for (int i = 0; i < 6; i++) {
            int random = splittableRandom.nextInt(0, 9);
            otpValue.append(random);
        }
        StringBuilder otpMessage = new StringBuilder();
        otpMessage.append("One Time Password (OTP) for Bhoomikhata Login:").append(otpValue);
        // PublishResult publishResult = null;

        // try {
        // publishResult = awsSNSClient.sendSingleSMS(otpMessage.toString(), countryCode
        // + mobileNumber);
        // } catch (Exception e) {
        // // TODO: handle exception
        // e.printStackTrace();
        // }
        Boolean otpStatus = sendTwillioMessage(mobileNumber, countryCode, otpMessage.toString());
        // if (publishResult != null && publishResult.getSdkHttpMetadata() != null
        // && publishResult.getSdkHttpMetadata().getHttpStatusCode() ==
        // HttpStatus.OK.value()) {
        if (Boolean.TRUE.equals(otpStatus)) {
            OTPModel otpModel = new OTPModel();
            otpModel.setCreatedDate(System.currentTimeMillis());
            otpModel.setMobileNumber(mobileNumber);
            otpModel.setUpdatedDate(System.currentTimeMillis());
            otpModel.setCountryCode(countryCode);
            otpModel.setOtp(otpValue.toString());
            saveOTP(otpModel);
            return true;
        }
        return false;
    }

    public Boolean sendMessage(String mobileNumber, String countryCode, String message) {

        // try {
        // awsSNSClient.sendSingleSMS(message, countryCode + mobileNumber);
        // } catch (Exception e) {
        // // TODO: handle exception
        // e.printStackTrace();
        // return false;
        // }
        // return true;
        return sendTwillioMessage(mobileNumber, countryCode, message);
    }

    public Boolean sendTwillioMessage(String mobileNumber, String countryCode, String message) {

        try {
            Twilio.init("AC63ebfc780f4606840f43656cffc426bb", "aa8808112543f7203c152fe5fb994c54");
            String toNumber = countryCode + mobileNumber;
            Message twiilioMessage = Message.creator(new PhoneNumber(toNumber),
                    new PhoneNumber("+13147945871"),
                    message).create();

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public OTPModel getOTP(String mobileNumber) {
        return registrationRepository.findByMobileNumber(mobileNumber);
    }

    public OTPModel saveOTP(OTPModel otpModel) {
        return registrationRepository.save(otpModel);
    }

    public OTPModel updateOTP(OTPModel otpModel) {
        return registrationRepository.save(otpModel);
    }

    public boolean isOTPExpired(String mobileNumber) {
        OTPModel otpModel2 = registrationRepository.findByMobileNumber(mobileNumber);

        Long timeElapsed = System.currentTimeMillis() - otpModel2.getUpdatedDate();

        return timeElapsed > 240000;
    }

}
