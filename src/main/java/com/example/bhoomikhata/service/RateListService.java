package com.example.bhoomikhata.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bhoomikhata.model.RateListModel;
import com.example.bhoomikhata.repository.RateListRepository;

@Service
public class RateListService {

	@Autowired
	private RateListRepository rateListRepository;

	public RateListModel saveRateList(RateListModel rateListModel) {
		String rateId = rateListModel.getBusinessId() + "_" + getCurrentDateTime();
		rateListModel.setItemId(rateId);
		return rateListRepository.save(rateListModel);
	}

	public List<RateListModel> getRateListByBusinessId(String businessId) {
		return rateListRepository.findByBusinessId(businessId);
	}

	public RateListModel getRateListByItemId(String itemId) {
		if (rateListRepository.existsById(itemId))
			return rateListRepository.findById(itemId).get();
		return new RateListModel();
	}

	public RateListModel updateRateList(RateListModel rateList) {
		if (!rateListRepository.existsById(rateList.getItemId()))
			return null;
		return rateListRepository.save(rateList);
	}
	
	public void deleteAllRateList() {
		rateListRepository.deleteAll();
	}

	public String getCurrentDateTime() {
		Date dNow = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyMMddhhmmssMs");
		String datetime = format.format(dNow);
		return datetime;
	}

}
