package com.example.bhoomikhata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bhoomikhata.model.ChatNotificationModel;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;

@Service
public class ChatNotificationService {

	@Autowired
	private FirebaseMessaging firebaseMessaging;

	@Autowired
	private BusinessIdTokenService idTokenService;

	public String sendChatMessage(ChatNotificationModel note) throws FirebaseMessagingException {
		String toBId = note.getFromAndTo().getToBId();
		String fromBId = note.getFromAndTo().getFromeBId();
		String token = idTokenService.getTokenById(toBId);

		if (token.length() == 0) {
			return "Failed : No Token asociated with business ID : " + toBId;
		}

		Notification notification = Notification.builder().setTitle(note.getTitle()).setBody(note.getMessageBody())
				.setImage(note.getImageUrl()).build();

		Message message = Message.builder().setToken(token).setNotification(notification).putData("fromBId", fromBId)
				.putData("toBId", toBId).build();
		return firebaseMessaging.send(message);
	}

	// testing.....
	public String sendChatMessageToToken(String token) {
		Notification notification = Notification.builder().setTitle("TEST").setBody("HI There!").build();
		Message message = Message.builder().setToken(token).setNotification(notification).build();

		try {
			return firebaseMessaging.send(message);
		} catch (FirebaseMessagingException e) {
			return e.getMessage();
		}
	}

}
