package com.example.bhoomikhata.service;

import java.util.List;
import java.util.Optional;

import com.example.bhoomikhata.model.CouponModel;
import com.example.bhoomikhata.repository.CouponRepository;

import org.springframework.stereotype.Service;

@Service
public class CouponService {

    private CouponRepository couponRepository;

    public CouponService(CouponRepository couponRepository) {
        this.couponRepository = couponRepository;
    }

    public Optional<CouponModel> getCouponById(String id) {
        return couponRepository.findById(id);
    }

    public List<CouponModel> getCouponbyContactId(String id) {
        return couponRepository.findByContactId(id);
    }

    public List<CouponModel> getCouponbyBussinessId(String id) {
        return couponRepository.findByBussinessId(id);
    }

    public CouponModel saveOrUpdateCouponDetails(CouponModel couponModel) {
        return couponRepository.save(couponModel);
    }

    public Boolean deleteCouponById(String id) {
        try {
            couponRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }

    public Boolean deleteCouponByContactId(String id) {
        try {
            couponRepository.deleteByContactId(id);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }

    public Boolean deleteCouponByBussinessId(String id) {
        try {
            couponRepository.deleteByBussinessId(id);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }

    public Boolean deleteAllRecords() {
        try {
            couponRepository.deleteAll();
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }

    public List<CouponModel> getAllRecords() {
        return couponRepository.findAll();
    }

}
