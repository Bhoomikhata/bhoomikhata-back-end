package com.example.bhoomikhata.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bhoomikhata.model.RazPayTransactionModelResponse;
import com.example.bhoomikhata.repository.RazPayPaymentTransactionRepository;

@Service
public class RazPayPaymentTransactionService {

	@Autowired
	private RazPayPaymentTransactionRepository transactionRepository;

	public List<RazPayTransactionModelResponse> getAllPaymentsByBusinessId(String businessId) {
		return transactionRepository.findByBusinessId(businessId);
	}

	public List<RazPayTransactionModelResponse> getAllPaymentDetails() {
		return transactionRepository.findAll();
	}

	public RazPayTransactionModelResponse savePaymentDetails(RazPayTransactionModelResponse paymentDetail) {
		return transactionRepository.save(paymentDetail);
	}

}
