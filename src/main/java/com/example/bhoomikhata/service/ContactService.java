package com.example.bhoomikhata.service;

import java.util.List;
import java.util.Optional;

import com.example.bhoomikhata.model.ContactModel;
import com.example.bhoomikhata.model.RazPayContactRequestDto;
import com.example.bhoomikhata.model.RazPayContactResponseModel;
import com.example.bhoomikhata.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContactService {

	@Autowired
	private RazPayContactFundAccountService razPayContactService;

	@Autowired
	private ContactRepository contactRepository;

	public Optional<ContactModel> getContactProfile(String id) {
		return contactRepository.findById(id);
	}

	public List<ContactModel> getContactProfileByMobile(String mobileNumber) {

		return contactRepository.findByMobileNumber(mobileNumber);
	}

	public List<ContactModel> getContactProfileByBussinessId(String bussinessId) {

		return contactRepository.findByBussinessId(bussinessId);
	}

	public ContactModel createOrUpdateContactProfile(ContactModel contactModel) {

		if (contactModel != null) {
			contactModel = razPayContactService.getOrCreateRazPayContactAndAccountDetails(contactModel);
		}

		return contactRepository.save(contactModel);
	}

	public Boolean deleteContactProfile(String id) {
		try {
			contactRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	public Boolean deleteAllContactProfile() {
		try {
			contactRepository.deleteAll();
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	public List<ContactModel> getAllContactProfile() {
		return contactRepository.findAll();
	}

}
