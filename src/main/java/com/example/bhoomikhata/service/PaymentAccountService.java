package com.example.bhoomikhata.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.example.bhoomikhata.model.PaymentModeModel;
import com.example.bhoomikhata.repository.PaymentAccountRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentAccountService {

    private PaymentAccountRepository paymentAccountRepository;

    @Autowired
    public PaymentAccountService(PaymentAccountRepository paymentAccountRepository) {
        this.paymentAccountRepository = paymentAccountRepository;
    }

    public Optional<PaymentModeModel> getPaymentModeDetails(String id) {
        return paymentAccountRepository.findById(id);
    }

    public List<PaymentModeModel> getAllPaymentModeDetails() {
        return paymentAccountRepository.findAll();
    }

    public List<PaymentModeModel> getPaymentModeDetails(String bussinessId, String contactId) {
        List<PaymentModeModel> paymentModeModels = paymentAccountRepository.findByBussinessId(bussinessId);
        if (paymentModeModels == null || paymentModeModels.isEmpty())
            return new ArrayList<>();

        return paymentModeModels.stream().filter(e -> e.getContactId().equals(contactId)).collect(Collectors.toList());
    }

    public PaymentModeModel createOrUpdatePaymentModeDetails(PaymentModeModel paymentModeModel) {
        paymentModeModel.setId(paymentModeModel.getBussinessId() + "_" + paymentModeModel.getContactId() + "_"
                + paymentModeModel.getCreatedDate());
        return paymentAccountRepository.save(paymentModeModel);
    }

    public Boolean deletePaymentModeDetails(String transactionId) {
        try {
            paymentAccountRepository.deleteById(transactionId);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }

    }

    public Boolean deletePaymentModeDetails(String bussinessId, String contactId) {
        try {
            paymentAccountRepository.deleteAll(getPaymentModeDetails(bussinessId, contactId));
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }

    }

    public Boolean deleteAllPaymentModeDetails() {
        try {
            paymentAccountRepository.deleteAll();
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }

    }
}
