package com.example.bhoomikhata.service;

import java.util.List;
import java.util.Optional;

import com.example.bhoomikhata.model.CoinModel;
import com.example.bhoomikhata.repository.CoinRepository;

import org.springframework.stereotype.Service;

@Service
public class CoinService {

    CoinRepository coinRepository;

    public CoinService(CoinRepository coinRepository) {
        this.coinRepository = coinRepository;
    }

    public CoinModel saveOrUpdateCoinDetails(CoinModel coinModel) {
        return coinRepository.save(coinModel);
    }

    public Optional<CoinModel> getCoinById(String id) {
        return coinRepository.findById(id);
    }

    public List<CoinModel> getCoinbyContactId(String id) {
        return coinRepository.findByContactId(id);
    }

    public Integer getTotalRewardsByContactId(String id) {
        List<CoinModel> coinModels = coinRepository.findByContactId(id);
        Integer total = 0;
        for (CoinModel coinModel : coinModels) {
            total += coinModel.getAmount();
        }
        return total;
    }

    public List<CoinModel> getCoinbyBussinessId(String id) {
        return coinRepository.findByBussinessId(id);
    }

    public Boolean deleteCoinById(String id) {
        try {
            coinRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }

    public Boolean deleteCoinByContactId(String id) {
        try {
            coinRepository.deleteByContactId(id);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }

    public Boolean deleteCoinByBussinessId(String id) {
        try {
            coinRepository.deleteByBussinessId(id);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }

    public Boolean deleteAllRecords() {
        try {
            coinRepository.deleteAll();
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }

    public List<CoinModel> getAllRecords() {
        return coinRepository.findAll();
    }
}
