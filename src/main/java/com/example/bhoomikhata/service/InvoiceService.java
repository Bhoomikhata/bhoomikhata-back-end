package com.example.bhoomikhata.service;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bhoomikhata.model.BussinesModel;
import com.example.bhoomikhata.model.InvoiceAndPOModel;
import com.example.bhoomikhata.model.InvoiceItemModel;
import com.example.bhoomikhata.model.InvoiceModel;
import com.example.bhoomikhata.model.OrderDetails;
import com.example.bhoomikhata.model.OrderModel;
import com.example.bhoomikhata.model.RateListModel;
import com.example.bhoomikhata.repository.InvoiceAndPORepository;
import com.example.bhoomikhata.repository.InvoiceRepository;
import com.example.bhoomikhata.repository.RateListRepository;

@Service
public class InvoiceService {

	@Autowired
	private InvoiceRepository invoiceRepository;

	@Autowired
	private InvoiceAndPORepository invoiceAndPORepository;

	@Autowired
	private OrderService orderService;

	@Autowired
	private RateListRepository rateRepository;

	@Autowired
	private ProfileService profileService;
	

	public InvoiceModel generateInvoice(String orderId) {

		// check order exists
		if (orderService.getOrderById(orderId).isEmpty())
			return null;

		String purchaseOrderId = "PO_" + orderId;
		String invoiceId = "INVO_" + orderId;

		// check invoice exists
		if (invoiceAndPORepository.existsById(purchaseOrderId))
			if (invoiceRepository.existsById(invoiceId))
				return invoiceRepository.findById(invoiceId).get();

		OffsetDateTime createdDate = OffsetDateTime.now();

		InvoiceModel invoice = new InvoiceModel();
		invoice.setInvoiceId(invoiceId);
		invoice.setCreatedDate(createdDate);

		
		List<InvoiceItemModel> invoiceItemList = new ArrayList<InvoiceItemModel>();
		List<OrderDetails> orderDetailsList = orderService.getOrderById(orderId).get().getOrderDetails();
		int srNo = 1;
		int totalQuantity = 0;
		double subTotal = 0;
		for (OrderDetails orderItem : orderDetailsList) {
			InvoiceItemModel invoiceItem = new InvoiceItemModel();			
			invoiceItem.setSerialNo(srNo);
			if (rateRepository.existsById(orderItem.getItemId())) {
				RateListModel rateList = rateRepository.findById(orderItem.getItemId()).get();
				invoiceItem.setItemName(rateList.getItemName());
				invoiceItem.setQuantity(orderItem.getQuantity());
				totalQuantity = totalQuantity + orderItem.getQuantity();
				invoiceItem.setHsnCode(rateList.getHsnCode());
				invoiceItem.setUnitPrice(orderItem.getUnitPrice());
				invoiceItem.setAmount(orderItem.getQuantity() * orderItem.getUnitPrice());
				subTotal = subTotal + (orderItem.getQuantity() * orderItem.getUnitPrice());
			}
			invoiceItemList.add(invoiceItem);
			++srNo;
		}

		invoice.setItemList(invoiceItemList);
		invoice.setTotalQuantity(totalQuantity);
		invoice.setSubTotal(subTotal);

		// extract bill from and bill to from order's custumerId
		OrderModel orderModel = orderService.getOrderById(orderId).get();
		String fromAndTo[] = orderModel.getCustomerId().split("_");
		String toId = fromAndTo[0];
		String fromId = fromAndTo[1];

		// get name and phone from businessModel
		BussinesModel bussinesModel = profileService.getBusinessProfile(toId).get();
		invoice.setBillTo(bussinesModel.getName());
		invoice.setBillToContact(bussinesModel.getMobileNumber());

		//invoice.setBillToAddress(contactService.getContactProfile(orderModel.getCustomerId()).get().getAdressModel().getPostalAddress());

		bussinesModel = profileService.getBusinessProfile(fromId).get();
		invoice.setBillFrom(bussinesModel.getName());
		invoice.setBillFromContact(bussinesModel.getMobileNumber());
		//invoice.setBillFromAddress(bussinesModel.getAdressModel().getPostalAddress());

		// save InvoiceAndPO
		invoiceAndPORepository.save(new InvoiceAndPOModel(purchaseOrderId, invoiceId, createdDate));

		// save Invoice
		invoiceRepository.save(invoice);

		return invoice;
	}

	public List<InvoiceModel> getAllInvoice() {
		return invoiceRepository.findAll();
	}

	public InvoiceModel getInvoiceByInvoiceId(String invoiceId) {
		if (invoiceRepository.existsById(invoiceId))
			return invoiceRepository.findById(invoiceId).get();
		else
			return null;
	}

	public void deleteAllInvoice() {
		invoiceAndPORepository.deleteAll();
		invoiceRepository.deleteAll();
	}

}
