package com.example.bhoomikhata.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bhoomikhata.model.OrderDetails;
import com.example.bhoomikhata.model.OrderDto;
import com.example.bhoomikhata.model.OrderModel;
import com.example.bhoomikhata.model.OrderStatus;
import com.example.bhoomikhata.model.OrderType;
import com.example.bhoomikhata.repository.OrderRepository;

@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRepository;

	public OrderModel saveOrder(OrderDto order) {

		String bId = order.getBussinessId();
		String cId = order.getCustomerId();
		List<OrderDetails> orderList = order.getOrderDetails();

		if (bId.isBlank() || cId.isBlank() || bId.equalsIgnoreCase("string") || cId.equalsIgnoreCase("string"))
			return null;
		for (OrderDetails od : orderList) {
			if (od.getQuantity() == 0)
				return null;
		}

		OrderModel orderModel = new OrderModel();
		
		orderModel.setOrderId("OID_B" + bId.substring(0, 3) + "C" + cId.substring(0, 3) + "_" + new Date().getTime());
				
		orderModel.setOrderDate(dtNow());
		orderModel.setOrderUpdateDate(dtNow());
		orderModel.setBussinessId(bId);
		orderModel.setCustomerId(cId);
		orderModel.setOrderDetails(orderList);
		orderModel.setOrderStatus(OrderStatus.CREATED);
		orderModel.setOrderType(OrderType.ORDER);
		return orderRepository.save(orderModel);
	}

	public Optional<OrderModel> getOrderById(String id) {
		return orderRepository.findById(id);
	}

	public List<OrderModel> getAllOrder() {
		return orderRepository.findAll();
	}

	public Boolean deleteOrder(String id) {

		try {
			if (orderRepository.existsById(id)) {
				orderRepository.deleteById(id);
				return true;
			} else
				return false;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	public Boolean deleteAllOrder() {
		try {
			orderRepository.deleteAll();
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	public List<OrderModel> getByBusinessId(String businessId) {
		return orderRepository.findByBussinessId(businessId);
	}

	public List<OrderModel> getByCustomerId(String customerId) {
		return orderRepository.findByCustomerId(customerId);
	}

	public List<OrderModel> getByBusinessIdAndCustomerId(String customerId, String businessId) {
		return getByCustomerId(customerId).stream().filter(e -> e.getBussinessId().equals(businessId))
				.collect(Collectors.toList());

	}

	public OrderModel updateOrderById(String orderId, List<OrderDetails> order) {
		OrderModel orderNew = new OrderModel();
		if (orderRepository.existsById(orderId)) {
			OrderModel orderOld = orderRepository.findById(orderId).get();
			orderNew.setOrderId(orderOld.getOrderId());
			orderNew.setBussinessId(orderOld.getBussinessId());
			orderNew.setCustomerId(orderOld.getCustomerId());
			orderNew.setOrderDate(orderOld.getOrderDate());
			orderNew.setOrderUpdateDate(dtNow());
			orderNew.setOrderStatus(OrderStatus.UPDATED);
			orderNew.setOrderType(orderOld.getOrderType());
			orderNew.setOrderDetails(order);
			orderNew = orderRepository.save(orderNew);
		}
		return orderNew;
	}

	public OrderModel returnOrder(String orderId) {
		OrderModel orderUpdate = new OrderModel();
		if (orderRepository.existsById(orderId)) {
			OrderModel orderOld = orderRepository.findById(orderId).get();
			orderUpdate.setOrderId(orderOld.getOrderId());
			orderUpdate.setBussinessId(orderOld.getBussinessId());
			orderUpdate.setCustomerId(orderOld.getCustomerId());
			orderUpdate.setOrderDate(orderOld.getOrderDate());
			orderUpdate.setOrderDetails(orderOld.getOrderDetails());
			orderUpdate.setOrderStatus(OrderStatus.INPROGRESS);
			orderUpdate.setOrderType(OrderType.RETURN);
			
			orderUpdate = orderRepository.save(orderUpdate);
		}
		return orderUpdate;
	}
	
	private String dtNow() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
		return sdf.format(new Date());
		
	}

}
