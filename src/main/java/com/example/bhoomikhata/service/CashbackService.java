package com.example.bhoomikhata.service;

import java.util.List;

import com.example.bhoomikhata.model.CashbackModel;
import com.example.bhoomikhata.repository.CashbackRepository;

import org.springframework.stereotype.Service;

@Service
public class CashbackService {
    CashbackRepository cashbackRepository;

    public CashbackService(CashbackRepository cashbackRepository) {
        this.cashbackRepository = cashbackRepository;
    }

    public CashbackModel getCashBackById(String id) {
        return cashbackRepository.findById(id).isPresent() ? cashbackRepository.findById(id).get() : null;
    }

    public List<CashbackModel> getCashBackbyContactId(String id) {
        return cashbackRepository.findByContactId(id);
    }

    public List<CashbackModel> getCashBackbyBussinessId(String id) {
        return cashbackRepository.findByBussinessId(id);
    }

    public Boolean deleteCashBackById(String id) {
        try {
            cashbackRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }

    public Boolean deleteCashBackByContactId(String id) {
        try {
            cashbackRepository.deleteByContactId(id);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }

    public Boolean deleteCashBackByBussinessId(String id) {
        try {
            cashbackRepository.deleteByBussinessId(id);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }

    public CashbackModel saveOrUpdateCoinDetails(CashbackModel cashbackModel) {
        return cashbackRepository.save(cashbackModel);
    }

    public Boolean deleteAllRecords() {
        try {
            cashbackRepository.deleteAll();
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }

    public List<CashbackModel> getAllRecords() {
        return cashbackRepository.findAll();
    }

    public Integer getTotalRewardsByContactId(String id) {
        List<CashbackModel> cashbackModels = cashbackRepository.findByContactId(id);
        Integer total = 0;
        for (CashbackModel cashbackModel : cashbackModels) {
            total += cashbackModel.getAmount();
        }
        return total;
    }
}
