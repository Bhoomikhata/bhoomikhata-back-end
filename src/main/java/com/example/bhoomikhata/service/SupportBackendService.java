package com.example.bhoomikhata.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bhoomikhata.model.BussinesModel;
import com.example.bhoomikhata.model.DocModel;
import com.example.bhoomikhata.model.FilesStorageModel;
import com.example.bhoomikhata.model.KycStatus;
import com.example.bhoomikhata.repository.FilesStorageRepository;

@Service
public class SupportBackendService {

	@Autowired
	private FilesStorageRepository filesRepository;

	@Autowired
	private ProfileService profileService;

	// ************ get all KYC Waiting ***********************//
	public Map<String, List<DocModel>> getAllKycWaitingVerification() {
		Map<String, List<DocModel>> kycList = new HashMap<String, List<DocModel>>();
		List<FilesStorageModel> fileStore = filesRepository.findByIsForSupport(true);
		for (FilesStorageModel kycData : fileStore) {
			Optional<BussinesModel> bmIs = profileService.getBusinessProfile(kycData.getBusinessId());
			if (bmIs.isPresent()) {
				kycList.put(bmIs.get().getId(), bmIs.get().getKyc());
			}
		}
		return kycList;
	}

	// ************ Verify KYC ***********************//
	public String verifyAndSetKycStatus(String businessId, String docId, KycStatus kycStatus) {
		String result = "KYC not verified";
		Optional<BussinesModel> bmIs = profileService.getBusinessProfile(businessId);
		if (bmIs.isEmpty())
			return "Business Profile [" + businessId + "] do not exists in record.";

		Optional<FilesStorageModel> fsIs = filesRepository.findById(docId);

		if (fsIs.isEmpty())
			return "Document[" + docId + "] not available in records.";

		BussinesModel bm = bmIs.get();

		List<DocModel> dmList = bm.getKyc();
		DocModel dm = null;

		for (DocModel docModel : dmList) {
			if (docModel.getDocId().equals(docId))
				dm = docModel;
		}

		if (dm != null) {
			dmList.remove(dm);
			dm.setKycStatus(kycStatus);
			dmList.add(dm);

			bm.setKyc(dmList);

			if (kycStatus.equals(KycStatus.VERIFIED)) {
				bm.setIsKycComplete(true);
				FilesStorageModel fsm = fsIs.get();
				fsm.setIsForSupport(false);
				filesRepository.save(fsm);
			}

			profileService.createOrUpdateBussinesProfile(bm);

			result = "KYC verification Document [" + dm.getDocType() + "] docId [" + docId + "] for businessID : "
					+ businessId + " has successfully set to [" + kycStatus + "].";

		}

		return result;
	}

}
