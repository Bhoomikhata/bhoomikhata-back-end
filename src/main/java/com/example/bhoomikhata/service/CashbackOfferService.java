package com.example.bhoomikhata.service;

import java.util.List;
import java.util.Optional;

import com.example.bhoomikhata.model.CashbackOffers;
import com.example.bhoomikhata.repository.CashbackOfferRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CashbackOfferService {
    CashbackOfferRepository cashbackOfferRepository;

    @Autowired
    public CashbackOfferService(CashbackOfferRepository cashbackOfferRepository) {
        this.cashbackOfferRepository = cashbackOfferRepository;
    }

    public Optional<CashbackOffers> getCashbackOfferById(String id) {
        return cashbackOfferRepository.findById(id);
    }

    public CashbackOffers saveOrUpdateCoinOfferDetails(CashbackOffers cashbackOffers) {
        return cashbackOfferRepository.save(cashbackOffers);
    }

    public Boolean deleteCashbackOfferById(String id) {
        try {
            cashbackOfferRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }

    public List<CashbackOffers> getAllRecords() {
        return cashbackOfferRepository.findAll();
    }

    public boolean deleteAllRecords() {
        try {
            cashbackOfferRepository.deleteAll();
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }
}
