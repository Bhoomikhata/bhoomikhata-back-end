package com.example.bhoomikhata.service;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bhoomikhata.model.ContactPaymentAccount;
import com.example.bhoomikhata.model.RazPayContactResponseModel;
import com.example.bhoomikhata.model.RazPayOrderDto;
import com.example.bhoomikhata.model.RazPayOrderDtoResponse;
import com.example.bhoomikhata.model.RazPayOrderModel;
import com.example.bhoomikhata.model.RazPayOrderPaymentFundAccount;
import com.example.bhoomikhata.model.RazPayTransactionModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.razorpay.Order;
import com.razorpay.RazorpayClient;

@Service
public class RazPayOrderService {
	@Autowired
	private RazPayTransactionService transactionService;

	@Autowired
	private RazorpayClient razorpayClient;

	@Autowired
	private RazPayDateTimeService dateTimeService;

	@Autowired
	private RazPayContactFundAccountService contactFundAccountService;

	public RazPayOrderDtoResponse createRazPayOrder(RazPayOrderDto order) {
		RazPayTransactionModel transaction = new RazPayTransactionModel();
		RazPayOrderDtoResponse orderResponse = new RazPayOrderDtoResponse();

		String toRazorPayConatctId = null;
		String toMobileNumber = null;
		String toName = null;
		List<RazPayOrderPaymentFundAccount> fundAccounts = null;
		String tofundAccountsOrder = "--";
		String userMessages = "You Can not make this payment.";

		List<String> statusLog = new ArrayList<String>();

		try {

			String transactionID = transactionService.getNewId();
			orderResponse.setTransactionRefId(transactionID);

			Integer amount = order.getAmount();
			orderResponse.setAmount(amount);

			String fromBId = order.getFromBusinessId();
			orderResponse.setFromBId(fromBId);

			String toCId = order.getToCustomerId();
			orderResponse.setToCustomerId(toCId);

			// get contact and fund accounts details//
			// by customerId........................//
			RazPayContactResponseModel toContact = null;
			RazPayContactResponseModel fromContact = null;
			fromContact = contactFundAccountService.getRazorPayContactByBusinessId(fromBId);
			toContact = contactFundAccountService.getRazorPayContactByCustomerId(toCId);
			if (fromContact == null) {
				userMessages = "You need to register on BhoomiKhata first.";
				throw new Exception(userMessages);
			}

			String fromName = fromContact.getName();
			String fromContactId = fromContact.getId();

			if (toContact != null) {
				toRazorPayConatctId = toContact.getId();
				// toRazorPayConatctId="cont_JpL4fgU203vDsy";
				toMobileNumber = toContact.getContact().contains("+91")
						? toContact.getContact().replace("+91", "").trim()
						: toContact.getContact();
				toName = toContact.getName();

				fundAccounts = paymentDetailsForOrder(toRazorPayConatctId);

				/// fundAccounts =
				/// accountService.fetchAllRpayFundAccountsByContactId(toRazorPayConatctId);
				if (fundAccounts != null) {
					if (fundAccounts.size() == 0) {
						userMessages = "ERROR : " + toContact.getName()
								+ " has not added any Bank Account or UPI address on Bhoomi Khata. You can not make this payment.";

						orderResponse.setMessagesToUser(userMessages);
						orderResponse.setToName(toName);
						orderResponse.setToRazorPayContactId(toRazorPayConatctId);
						return orderResponse;
					}

					if (fundAccounts.size() == 1)
						userMessages = "SUCCESS : You Can proceed to the payment.";
					else
						userMessages = "SUCCESS : " + toContact.getName()
								+ " has multiple payment options linked to BhoomiKhata. Select one to proceed payment.";

					for (RazPayOrderPaymentFundAccount acc : fundAccounts) {
						if (tofundAccountsOrder.length() == 2)
							tofundAccountsOrder = acc.getFundAccountId();
						else
							tofundAccountsOrder = tofundAccountsOrder + ":" + acc.getFundAccountId();
					}
				}
			} else {
				userMessages = "ERROR : You Can not make this payment. Recipient has not registered on BhoomiKhata.";
			}

			// for transaction logs................................
			transaction.setTransactionRefId(transactionID);
			transaction.setTransactionStart(dateTimeService.getIndianDateTime());
			transaction.setFromBid(fromBId);
			transaction.setToBid(toMobileNumber);

			// create payment order on razor pay................................
			JSONObject orderRequest = new JSONObject();
			orderRequest.put("amount", amount * 100);
			orderRequest.put("currency", "INR");
			orderRequest.put("receipt", transactionID);
			JSONObject notes = new JSONObject();
			notes.put("API_DATE", dateTimeService.getIndianDateTime());
			notes.put("from_mobile", fromBId);
			notes.put("from_name", fromName);
			notes.put("from_contact_id", fromContactId);
			notes.put("to_mobile", toMobileNumber);
			notes.put("to_name", toName);
			notes.put("amount", amount);
			notes.put("to_conatct_id", toRazorPayConatctId);
			notes.put("FUND_ACCOUNTS", tofundAccountsOrder);
			orderRequest.put("notes", notes);

			// Calling razorPay API for order................................
			Order razOrder = null;
			RazPayOrderModel razPayOrder = null;
			if (toContact != null) {
				razOrder = razorpayClient.orders.create(orderRequest);
				ObjectMapper mapper = new ObjectMapper();
				razPayOrder = mapper.readValue(razOrder.toJson().toString(), RazPayOrderModel.class);
				if (razPayOrder != null) {
					// for transaction logs................................
					transaction.setPaymentOrder(razPayOrder);
					transaction.setRazorPayOrderId(razPayOrder.getId());
					transaction.setRazorPayOrderStatus(razPayOrder.getStatus());
					statusLog.add("SUCCESS : Payment Order Created.");
					transaction.setStatusErrorLog(statusLog);
					// prepare response....................................
					orderResponse.setRazorPayOrderId(razPayOrder.getId());
					orderResponse.setAmount(razPayOrder.getAmount() / 100);
					orderResponse.setToName(toName);
					orderResponse.setToRazorPayContactId(toRazorPayConatctId);
					orderResponse.setPaymentAccountOptions(fundAccounts);
				}
			}

		} catch (Exception e) {
			statusLog.add("ERROR : " + e.getMessage());
			userMessages = "ERROR : " + e.getMessage();
		} finally {
			// save details on transaction table
			transaction.setStatusErrorLog(statusLog);
			orderResponse.setMessagesToUser(userMessages);
			transactionService.saveTransactionDetails(transaction);
		}

		return orderResponse;
	}

	/*********************************
	 * fetch razor pay fund accounts by contact id for new order of payment
	 *******************************************/
	public List<RazPayOrderPaymentFundAccount> paymentDetailsForOrder(String razorPayContactId) {
		List<RazPayOrderPaymentFundAccount> accountsDetails = null;
		try {

			if (razorPayContactId == null)
				return null;

			List<ContactPaymentAccount> availableAccounts = new ArrayList<ContactPaymentAccount>();
			availableAccounts = contactFundAccountService.getAccountDetailsForBusinessProfile(razorPayContactId);

			if (availableAccounts.size() > 0) {
				accountsDetails = new ArrayList<RazPayOrderPaymentFundAccount>();
				for (ContactPaymentAccount conPayAcc : availableAccounts) {
					if (conPayAcc.getIsBankOrUpiActive()) {
						RazPayOrderPaymentFundAccount ordAcc = new RazPayOrderPaymentFundAccount();
						ordAcc.setFundAccountId(conPayAcc.getFundAccountId());
						ordAcc.setAccountType(conPayAcc.getAccountType());
						ordAcc.setAccountDesc(conPayAcc.getAccountDescription());
						accountsDetails.add(ordAcc);
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return accountsDetails;
	}

	/*********************************
	 * fetch payment order *
	 *******************************************/
	public RazPayOrderModel fetchOrdersById(String id) {
		RazPayOrderModel orderModel = null;
		try {
			Order razOrder = razorpayClient.orders.fetch(id);
			ObjectMapper mapper = new ObjectMapper();
			orderModel = mapper.readValue(razOrder.toJson().toString(), RazPayOrderModel.class);
			return orderModel;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return orderModel;
	}

}
