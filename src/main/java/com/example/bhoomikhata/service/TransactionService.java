package com.example.bhoomikhata.service;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.TreeMap;

import com.example.bhoomikhata.model.TransactionModel;
import com.example.bhoomikhata.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionService {

	private TransactionRepository transactionRepository;

	@Autowired
	public TransactionService(TransactionRepository transactionRepository) {
		this.transactionRepository = transactionRepository;
	}

	public Optional<TransactionModel> getTransactionDetails(String id) {
		Optional<TransactionModel> trx = transactionRepository.findById(id);
		if (trx.isPresent() && trx.get().getTransactionEditHistory() != null) {
			TreeMap<String, Integer> editHistory = new TreeMap<String, Integer>(Collections.reverseOrder());
			editHistory.putAll(trx.get().getTransactionEditHistory());
			trx.get().setTransactionEditHistory(editHistory);
		}
		return trx;
	}

	public List<TransactionModel> getTransactionDetails(String bussinessId, String contactId) {
		return transactionRepository.findByContactId(contactId);

	}

	public TransactionModel createOrUpdateTransactionDetails(TransactionModel transactionModel) {
		OffsetDateTime timeNow = OffsetDateTime.now();
		transactionModel.setCreatedDate(timeNow);
		transactionModel.setUpdatedDate(timeNow);
		transactionModel.setIsSMSDeliverd(false);
		transactionModel.setIsInSync(false);
		return transactionRepository.save(transactionModel);
	}

	public TransactionModel patchTransactionDetails(TransactionModel transactionModel) {
		TransactionModel oldData = null;
		TransactionModel updateData = new TransactionModel();

		if (transactionRepository.findById(transactionModel.getId()).isEmpty())
			return null;

		oldData = transactionRepository.findById(transactionModel.getId()).get();
		updateData.setId(oldData.getId());
		updateData.setBussinessId(oldData.getBussinessId());
		updateData.setContactId(oldData.getContactId());
		updateData.setCreatedDate(oldData.getCreatedDate());
		updateData.setUpdatedDate(OffsetDateTime.now());
		updateData.setPayeeId(oldData.getPayeeId());
		updateData.setPayerId(oldData.getPayerId());

		updateData.setDueDate(
				transactionModel.getDueDate() != null ? transactionModel.getDueDate() : oldData.getDueDate());
		updateData.setAmount(transactionModel.getAmount() != null ? transactionModel.getAmount() : oldData.getAmount());
		updateData.setIsCredit(
				transactionModel.getIsCredit() != null ? transactionModel.getIsCredit() : oldData.getIsCredit());
		updateData.setType(transactionModel.getType() != null ? transactionModel.getType() : oldData.getType());
		updateData.setDueDateDuration(transactionModel.getDueDate() != null ? transactionModel.getDueDateDuration()
				: oldData.getDueDateDuration());
		updateData.setTransactionTypeId(
				transactionModel.getTransactionTypeId() != null ? transactionModel.getTransactionTypeId()
						: oldData.getTransactionTypeId());
		updateData.setTransactionType(
				transactionModel.getTransactionType() != null ? transactionModel.getTransactionType()
						: oldData.getTransactionType());
		updateData.setDocumentFile(transactionModel.getDocumentFile() != null ? transactionModel.getDocumentFile()
				: oldData.getDocumentFile());
		updateData.setIsInSync(
				transactionModel.getIsInSync() != null ? transactionModel.getIsInSync() : oldData.getIsInSync());
		updateData.setIsSMSDeliverd(transactionModel.getIsSMSDeliverd() != null ? transactionModel.getIsSMSDeliverd()
				: oldData.getIsSMSDeliverd());
		updateData.setDescriptons(transactionModel.getDescriptons() != null ? transactionModel.getDescriptons()
				: oldData.getDescriptons());
		updateData.setMessagedByName(transactionModel.getMessagedByName() != null ? transactionModel.getMessagedByName()
				: oldData.getMessagedByName());

		TreeMap<String, Integer> txHistoryChange = new TreeMap<String, Integer>(Collections.reverseOrder());

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MMM-yyyy hh:mm:ss a");
		ZoneId zoneId = ZoneId.of("Asia/Kolkata");

		if (oldData.getTransactionEditHistory() != null) {
			txHistoryChange.putAll(oldData.getTransactionEditHistory());
		} else {
			OffsetDateTime odt = oldData.getCreatedDate();
			txHistoryChange.put(odt.atZoneSameInstant(zoneId).format(dtf), oldData.getAmount());
		}

		if (transactionModel.getAmount() != null && !oldData.getAmount().equals(transactionModel.getAmount())) {
			LocalDateTime ld = LocalDateTime.now(zoneId);
			txHistoryChange.put(ld.format(dtf), transactionModel.getAmount());
		}

		updateData.setTransactionEditHistory(txHistoryChange);
		return transactionRepository.save(updateData);
	}

	public Boolean deleteTransactionDetails(String bussinessId, String contactId) {
		try {
			List<TransactionModel> transactionModels = new ArrayList<>();
			List<TransactionModel> transactionModelsByBussiness = transactionRepository.findByBussinessId(bussinessId);
			if (transactionModelsByBussiness == null || transactionModelsByBussiness.isEmpty())
				return false;
			transactionModelsByBussiness.stream().forEach(e -> {
				if (e.getBussinessId().equals(bussinessId) && e.getContactId().equals(contactId))
					transactionModels.add(e);
			});
			transactionRepository.deleteAll(transactionModels);
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		return true;
	}

	public Boolean deleteAllTransactions() {
		try {
			transactionRepository.deleteAll();
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return true;
		}
	}

	public List<TransactionModel> getTransactionDetailsByPayee(String payeeId) {
		return transactionRepository.findByPayeeId(payeeId);
	}

	public List<TransactionModel> getTransactionDetailsByPayer(String payerId) {

		return transactionRepository.findByPayerId(payerId);
	}

	public List<TransactionModel> getTransactionDetailsByBussinessId(String bussinessId) {
		return transactionRepository.findByBussinessId(bussinessId);
	}

	public List<TransactionModel> getTransactionDetailsByContactId(String contactId) {

		return transactionRepository.findByPayerId(contactId);
	}

	public List<TransactionModel> getAllTransactionDetails() {

		return transactionRepository.findAll();
	}

	public Boolean deleteTransactionDetailsById(String id) {
		try {
			transactionRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}

	}
}
