package com.example.bhoomikhata.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.bhoomikhata.model.BussinesModel;
import com.example.bhoomikhata.model.DocModel;
import com.example.bhoomikhata.model.DocType;
import com.example.bhoomikhata.model.FilesCategory;
import com.example.bhoomikhata.model.FilesStorageModel;
import com.example.bhoomikhata.model.KycStatus;
import com.example.bhoomikhata.model.TransactionModel;
import com.example.bhoomikhata.repository.FilesStorageRepository;

@Service
public class FilesUploadService {

	@Autowired
	private FilesStorageRepository filesRepository;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private TransactionService transactionService;

	/*
	 * ******* for profile
	 ************************************************/
	// *****profile image upload ***********//
	public String uploadProfileImage(String businessId, MultipartFile file) throws Exception {
		String result = "Image upload failed.";
		try {
			Optional<BussinesModel> bmIs = profileService.getBusinessProfile(businessId);

			if (bmIs.isEmpty())
				throw new Exception("Business profile does not exists.");

			if (file.isEmpty())
				throw new Exception("No image file selected.");

			if (file.getSize() >= 10485760)
				throw new Exception("You can only upload imgae file upto 10Mb in size.");

			if (!file.getContentType().equalsIgnoreCase(MediaType.IMAGE_JPEG_VALUE)
					&& !file.getContentType().equalsIgnoreCase(MediaType.IMAGE_PNG_VALUE))
				throw new Exception("You can only upload image file with extention *.jpeg, *.jpg or *.png ["
						+ file.getContentType() + " | " + file.getOriginalFilename() + "]");

			FilesStorageModel fileStore = new FilesStorageModel();

			if (bmIs.get().getImage() != null) {
				if (filesRepository.findById(bmIs.get().getImage()).isPresent())
					fileStore.setId(filesRepository.findById(bmIs.get().getImage()).get().getId());
			}

			fileStore.setBusinessId(businessId);
			fileStore.setFilesCategory(FilesCategory.PROFILE_IMAGE);
			fileStore.setFileSize(file.getSize());
			fileStore.setFileType(file.getContentType());
			fileStore.setFileName(file.getOriginalFilename());
			fileStore.setFileData(new Binary(BsonBinarySubType.BINARY, file.getBytes()));

			fileStore = filesRepository.save(fileStore);

			BussinesModel profile = profileService.getBusinessProfile(businessId).get();
			profile.setImage(fileStore.getId());
			profileService.createOrUpdateBussinesProfile(profile);
			result = "Profile image(" + file.getOriginalFilename() + ") uploaded successfully.";

		} catch (Exception e) {
			throw e;
		}
		return result;
	}

	// *****profile image gallery upload ***********//
	public String uploadProfileGallaryImage(String businessId, MultipartFile file) throws Exception {
		String result = "Image upload failed.";
		try {
			if (profileService.getBusinessProfile(businessId).isEmpty())
				throw new Exception("Business profile does not exists.");

			if (file.isEmpty())
				throw new Exception("No image file selected.");

			if (file.getSize() >= 10485760)
				throw new Exception("You can only upload imgae file upto 10Mb in size.");

			if (!file.getContentType().equalsIgnoreCase(MediaType.IMAGE_JPEG_VALUE)
					&& !file.getContentType().equalsIgnoreCase(MediaType.IMAGE_PNG_VALUE))
				throw new Exception("You can only upload image file with extention *.jpeg or *.png");

			BussinesModel profile = profileService.getBusinessProfile(businessId).get();
			List<String> galleryImages = new ArrayList<String>();
			if (profile.getImageGallery() != null && profile.getImageGallery().size() != 0) {
				galleryImages = profile.getImageGallery();
			}

			FilesStorageModel fileStore = new FilesStorageModel();

			fileStore.setBusinessId(businessId);
			fileStore.setFilesCategory(FilesCategory.PROFILE_GALLERY_IMAGE);
			fileStore.setFileSize(file.getSize());
			fileStore.setFileType(file.getContentType());
			fileStore.setFileName(file.getOriginalFilename());
			fileStore.setFileData(new Binary(BsonBinarySubType.BINARY, file.getBytes()));

			fileStore = filesRepository.save(fileStore);

			galleryImages.add(fileStore.getId());
			profile.setImageGallery(galleryImages);
			profileService.createOrUpdateBussinesProfile(profile);
			result = "Image (" + file.getOriginalFilename() + ") added to your gallery.";

		} catch (Exception e) {
			throw e;
		}
		return result;
	}

	// *****profile KYC image upload ***********//
	public String uploadProfileKycImage(String businessId, DocType docType, MultipartFile file) throws Exception {
		String result = "Kyc Document upload failed.";
		try {
			if (profileService.getBusinessProfile(businessId).isEmpty())
				throw new Exception("Business profile does not exists.");

			if (file.isEmpty())
				throw new Exception("No image file selected.");

			if (file.getSize() >= 10485760)
				throw new Exception("You can only upload imgae file upto 10Mb in size.");

			if (!file.getContentType().equalsIgnoreCase(MediaType.IMAGE_JPEG_VALUE)
					&& !file.getContentType().equalsIgnoreCase(MediaType.IMAGE_PNG_VALUE))
				throw new Exception("You can only upload image file with extention *.jpeg or *.png");

			BussinesModel profile = profileService.getBusinessProfile(businessId).get();

			FilesStorageModel fileStore = new FilesStorageModel();

			fileStore.setBusinessId(businessId);
			fileStore.setFilesCategory(FilesCategory.PROFILE_KYC);
			fileStore.setFilesSubCategory(docType.name());
			fileStore.setFileSize(file.getSize());
			fileStore.setFileType(file.getContentType());
			fileStore.setFileName(file.getOriginalFilename());
			fileStore.setFileData(new Binary(BsonBinarySubType.BINARY, file.getBytes()));
			fileStore.setIsForSupport(true);

			fileStore = filesRepository.save(fileStore);

			List<DocModel> docList = new ArrayList<DocModel>();
			if (profile.getKyc() != null)
				docList = profile.getKyc();

			DocModel kycDoc = new DocModel();
			kycDoc.setDocId(fileStore.getId());
			kycDoc.setDocType(docType);
			kycDoc.setKycStatus(KycStatus.CREATED);

			docList.add(kycDoc);
			profile.setKyc(docList);
			profileService.createOrUpdateBussinesProfile(profile);
			result = docType + " document file(" + file.getOriginalFilename() + ") uploaded successfully.";

		} catch (Exception e) {
			throw e;
		}
		return result;
	}

	/*
	 * ***** for Invoice Data upload
	 ************************************************/
	public String uploadFileForChatTransaction(String transactionId, MultipartFile file) throws Exception {
		String result = "File upload failed.";
		try {

			if (file.isEmpty())
				throw new Exception("No file selected. Try again.");

			if (file.getSize() >= 31457280)
				throw new Exception("You can only upload files upto 30Mb in size.");
			String fileName = file.getOriginalFilename().toLowerCase();

			if (fileName.endsWith(".exe") || fileName.endsWith(".mp4"))
				throw new Exception("Incorrect file type.");

			Optional<TransactionModel> tModelIs = transactionService.getTransactionDetails(transactionId);
			if (tModelIs.isEmpty())
				throw new Exception("Chat transaction Id not available.");

			TransactionModel tModel = tModelIs.get();

			FilesStorageModel fileStore = new FilesStorageModel();

			if (tModel.getDocumentFile() != null) {
				if (filesRepository.findById(tModel.getDocumentFile()).isPresent())
					fileStore.setId(filesRepository.findById(tModel.getDocumentFile()).get().getId());
			}

			fileStore.setBusinessId(tModel.getBussinessId());
			fileStore.setTransactionId(transactionId);
			fileStore.setFilesCategory(FilesCategory.CHAT_INVOICE);
			fileStore.setFileSize(file.getSize());
			fileStore.setFileType(file.getContentType());
			fileStore.setFileName(file.getOriginalFilename());
			fileStore.setFileData(new Binary(BsonBinarySubType.BINARY, file.getBytes()));

			fileStore = filesRepository.save(fileStore);

			tModel.setDocumentFile(fileStore.getId());
			transactionService.createOrUpdateTransactionDetails(tModel);

			result = "Document uploaded successfully.";

		} catch (Exception e) {
			throw e;
		}
		return result;
	}

}
