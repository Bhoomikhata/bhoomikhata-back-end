package com.example.bhoomikhata.service;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bhoomikhata.model.BussinesModel;
import com.example.bhoomikhata.model.UserRating;
import com.example.bhoomikhata.model.UserRatingDto;
import com.example.bhoomikhata.model.UserRatingModel;
import com.example.bhoomikhata.repository.ProfileRepository;
import com.example.bhoomikhata.repository.UserRatingRepository;

@Service
public class RatingsService {

	@Autowired
	private UserRatingRepository ratingRepository;

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private ContactService contactService;

	private final DecimalFormat df = new DecimalFormat("0.00");

	public UserRating getUserRating(String businessId) {
		List<UserRatingModel> ratingList = null;
		UserRating userRating = new UserRating(0.0, 0, 0);

		ratingList = ratingRepository.findByRatedTobusinessId(businessId);
		if (ratingList != null && ratingList.size() != 0) {
			int ratingUsers = ratingList.size();
			double totalRating = 0;
			for (UserRatingModel ur : ratingList) {
				totalRating = totalRating + ur.getRating();
			}

			double avgRating = totalRating / ratingUsers;
			userRating.setRatedByUsers(ratingUsers);
			userRating.setRating(Double.parseDouble(df.format(avgRating)));
		}

		return userRating;
	}

	public UserRating getUserRatingForContactProfile(String businessId, String customerId) {
		List<UserRatingModel> ratingList = null;
		UserRating userRating = new UserRating(0.0, 0, 0);

		
		if (contactService.getContactProfile(customerId).isEmpty())
			return userRating;

		String bIdArr[] = customerId.trim().split("_");
		String ratedTo = "-";

		if (bIdArr.length == 2) {
			if (bIdArr[0].equalsIgnoreCase(businessId))
				ratedTo = bIdArr[1];
			else
				ratedTo = bIdArr[0];
		}

		ratingList = ratingRepository.findByRatedTobusinessId(ratedTo);
		if (ratingList != null && ratingList.size() != 0) {
			int ratingUsers = ratingList.size();
			double totalRating = 0;
			for (UserRatingModel ur : ratingList) {
				totalRating = totalRating + ur.getRating();
			}

			double avgRating = totalRating / ratingUsers;
			userRating.setRatedByUsers(ratingUsers);
			userRating.setRating(Double.parseDouble(df.format(avgRating)));
		}

		Optional<UserRatingModel> userRatingOwnIs = ratingRepository
				.findById(businessId.trim() + "_" + customerId.trim());
		if (userRatingOwnIs.isPresent())
			userRating.setRatedByYou(userRatingOwnIs.get().getRating());
		else
			userRating.setRatedByYou(0);

		return userRating;
	}

	public String giveUserRating(UserRatingDto rating) {
		String result = "Rating submit Failed.";
		
		if (contactService.getContactProfile(rating.getCustomerId()).isEmpty())
			return result;

		UserRatingModel ratingModel = new UserRatingModel();
		ratingModel.setRatingId(rating.getBusinessId().trim() + "_" + rating.getCustomerId().trim());
		ratingModel.setRating(rating.getRating());

		String bIdArr[] = rating.getCustomerId().trim().split("_");
		String ratedBy = rating.getBusinessId();
				
		String ratedTo = "-";

		if (bIdArr.length == 2) {
			if (bIdArr[0].equalsIgnoreCase(rating.getBusinessId()))
				ratedTo = bIdArr[1];
			else
				ratedTo = bIdArr[0];
		}

		ratingModel.setRatedByBusinessId(ratedBy);
		ratingModel.setRatedTobusinessId(ratedTo);

		ratingRepository.save(ratingModel);
		UserRating userRating = null;
		userRating = getUserRating(ratingModel.getRatedTobusinessId());

		if (userRating != null) {
			Optional<BussinesModel> bmIs = profileRepository.findById(ratingModel.getRatedTobusinessId());
			if (bmIs.isPresent()) {
				BussinesModel bm = bmIs.get();
				bm.setRatings(userRating);
				profileRepository.save(bm);
			}
			result = "Rating submit success.";
		}
		return result;
	}

}
