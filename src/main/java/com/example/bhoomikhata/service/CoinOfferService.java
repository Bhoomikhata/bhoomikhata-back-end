package com.example.bhoomikhata.service;

import java.util.List;
import java.util.Optional;

import com.example.bhoomikhata.model.CoinOffers;
import com.example.bhoomikhata.repository.CoinOfferRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CoinOfferService {
    CoinOfferRepository coinOfferRepository;

    @Autowired
    public CoinOfferService(CoinOfferRepository coinOfferRepository) {
        this.coinOfferRepository = coinOfferRepository;
    }

    public Optional<CoinOffers> getCoinOfferById(String id) {
        return coinOfferRepository.findById(id);
    }

    public CoinOffers saveOrUpdateCoinOfferDetails(CoinOffers coinOfferModel) {
        return coinOfferRepository.save(coinOfferModel);
    }

    public Boolean deleteCoinOfferById(String id) {
        try {
            coinOfferRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }

    public List<CoinOffers> getAllRecords() {
        return coinOfferRepository.findAll();
    }

    public boolean deleteAllRecords() {
        try {
            coinOfferRepository.deleteAll();
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }
}
