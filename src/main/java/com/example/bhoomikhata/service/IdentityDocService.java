package com.example.bhoomikhata.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.example.bhoomikhata.model.IdentityDocModel;
import com.example.bhoomikhata.repository.IdentityDocRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * IdentityDocService
 */
@Service
public class IdentityDocService {

    private IdentityDocRepository identityDocRepository;

    @Autowired
    public IdentityDocService(IdentityDocRepository identityDocRepository) {
        this.identityDocRepository = identityDocRepository;
    }

    public Optional<IdentityDocModel> getDocDetails(String id) {
        return identityDocRepository.findById(id);
    }

    public List<IdentityDocModel> getAllDocDetails() {
        return identityDocRepository.findAll();
    }

    public List<IdentityDocModel> getDocDetails(String bussinessId, String contactId) {
        return identityDocRepository.findByBussinessId(bussinessId).stream()
                .filter(e -> e.getContactId().equals(contactId)).collect(Collectors.toList());
    }

    public IdentityDocModel createAndUpdateDocDetails(IdentityDocModel identityDocModel) {
        identityDocModel.setId(identityDocModel.getBussinessId() + "_" + identityDocModel.getContactId() + "_"
                + identityDocModel.getCreatedDate());
        return identityDocRepository.save(identityDocModel);

    }

    public Boolean deleteDocDetails(String docId) {
        try {
            identityDocRepository.deleteById(docId);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }

    public Boolean deleteAllDocDetails() {
        try {
            identityDocRepository.deleteAll();
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }

}