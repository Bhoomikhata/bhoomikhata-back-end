package com.example.bhoomikhata.service;

import java.util.List;
import java.util.Optional;

import com.example.bhoomikhata.model.CouponOffers;
import com.example.bhoomikhata.repository.CouponOfferRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CouponOfferService {
    CouponOfferRepository couponOfferRepository;

    @Autowired
    public CouponOfferService(CouponOfferRepository couponOfferRepository) {
        this.couponOfferRepository = couponOfferRepository;
    }

    public Optional<CouponOffers> getCouponOfferById(String id) {
        return couponOfferRepository.findById(id);
    }

    public CouponOffers saveOrUpdateCouponOfferDetails(CouponOffers couponOffers) {
        return couponOfferRepository.save(couponOffers);
    }

    public Boolean deleteCouponOfferById(String id) {
        try {
            couponOfferRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }

    public List<CouponOffers> getAllRecords() {
        return couponOfferRepository.findAll();
    }

    public boolean deleteAllRecords() {
        try {
            couponOfferRepository.deleteAll();
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
    }
}
