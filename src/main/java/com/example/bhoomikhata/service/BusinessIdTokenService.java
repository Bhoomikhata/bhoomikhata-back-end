package com.example.bhoomikhata.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bhoomikhata.model.BusinessIdTokenModel;
import com.example.bhoomikhata.repository.BusinessIdTokenRepository;

@Service
public class BusinessIdTokenService {

	@Autowired
	private BusinessIdTokenRepository tokenRepository;

	public BusinessIdTokenModel saveToken(BusinessIdTokenModel idTokenModel) {
		return tokenRepository.save(idTokenModel);
	}

	public String getTokenById(String bId) {
		String token = "";
		if (tokenRepository.existsById(bId))
			token = tokenRepository.findById(bId).get().getTokenId();
		return token;
	}

	public List<BusinessIdTokenModel> getAllTokenList() {
		List<BusinessIdTokenModel> list = new ArrayList<BusinessIdTokenModel>();
		if (tokenRepository.count() > 0)
			list = tokenRepository.findAll();
		System.out.println(list);
		return list;
	}
}
