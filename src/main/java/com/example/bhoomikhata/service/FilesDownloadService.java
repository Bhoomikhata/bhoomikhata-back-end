package com.example.bhoomikhata.service;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.bhoomikhata.model.BussinesModel;
import com.example.bhoomikhata.model.DocModel;
import com.example.bhoomikhata.model.FilesStorageModel;
import com.example.bhoomikhata.model.TransactionModel;
import com.example.bhoomikhata.repository.FilesStorageRepository;

@Service
public class FilesDownloadService {

	@Autowired
	private FilesStorageRepository filesRepository;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private TransactionService transactionService;

	// *****profile image download ***********//
	public ResponseEntity<InputStreamResource> getProfileImage(String businessId) throws Exception {
		try {
			if (profileService.getBusinessProfile(businessId).isEmpty())
				throw new Exception("Business profile does not exists.");

			if (profileService.getBusinessProfile(businessId).get().getImage() == null)
				return null;

			BussinesModel profile = profileService.getBusinessProfile(businessId).get();
			Optional<FilesStorageModel> fileStoreIs = filesRepository.findById(profile.getImage());

			if (fileStoreIs.isEmpty())
				return null;

			FilesStorageModel fileStore = fileStoreIs.get();
			if (fileStore.getFileData() != null) {
				return ResponseEntity.ok().contentType(MediaType.parseMediaType(fileStore.getFileType()))
						.contentLength(fileStore.getFileSize())
						.body(new InputStreamResource(new ByteArrayInputStream(fileStore.getFileData().getData())));
			}
			return null;

		} catch (Exception e) {
			throw e;
		}
	}

	// *****profile Gallery image download ***********//
	public ResponseEntity<InputStreamResource> getProfileGallaryImage(String businessId, String imageId)
			throws Exception {
		try {
			if (profileService.getBusinessProfile(businessId).isEmpty())
				throw new Exception("Business profile does not exists.");

			if (profileService.getBusinessProfile(businessId).get().getImageGallery() == null)
				return null;

			return getFileById(imageId);

		} catch (Exception e) {
			throw e;
		}
	}

	// *****profile get Gallery images IDS ***********//
	public List<String> getAllGalleryImageDetails(String businessId) {
		Optional<BussinesModel> bmIs = profileService.getBusinessProfile(businessId);
		if (bmIs.isEmpty())
			return null;

		return bmIs.get().getImageGallery();
	}

	// *****profile KYC image download ***********//
	public ResponseEntity<InputStreamResource> getKycImageById(String kycId) throws Exception {
		return getFileById(kycId);
	}

	// *****profile KYC image download ***********//
	public List<DocModel> getAllKycByBusinessId(String businessId) throws Exception {
		if (profileService.getBusinessProfile(businessId).isPresent()) {
			return profileService.getBusinessProfile(businessId).get().getKyc();
		}
		return null;
	}

	/*
	 * ***** for Invoice Data download
	 ************************************************/
	public ResponseEntity<ByteArrayResource> downloadFileForChatTransaction(String transactionId) throws Exception {
		try {
			if (transactionService.getTransactionDetails(transactionId).isEmpty())
				throw new Exception("Chat transaction Id not available.");

			TransactionModel tModel = transactionService.getTransactionDetails(transactionId).get();
			Optional<FilesStorageModel> fsmIs = filesRepository.findById(tModel.getDocumentFile());

			if (fsmIs.isPresent() && fsmIs.get().getFileData() == null)
				throw new Exception("No Document available.");

			FilesStorageModel fsm = fsmIs.get();
			HttpHeaders header = new HttpHeaders();
			header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fsm.getFileName());
			return ResponseEntity.ok().headers(header).contentType(MediaType.parseMediaType(fsm.getFileType()))
					.contentLength(fsm.getFileSize()).body(new ByteArrayResource(fsm.getFileData().getData()));

		} catch (Exception e) {
			//e.printStackTrace();
			throw e;
		}
	}

	// *****Get File ByID***********//
	public ResponseEntity<InputStreamResource> getFileById(String kycId) throws Exception {
		try {
			Optional<FilesStorageModel> fileStoreIs = filesRepository.findById(kycId);

			if (fileStoreIs.isEmpty())
				return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);

			FilesStorageModel fileStore = fileStoreIs.get();

			if (fileStore.getFileData() != null) {
				return ResponseEntity.ok().contentType(MediaType.parseMediaType(fileStore.getFileType()))
						.contentLength(fileStore.getFileSize())
						.body(new InputStreamResource(new ByteArrayInputStream(fileStore.getFileData().getData())));
			} else {
				return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
			}
		} catch (Exception e) {
			throw e;
		}
	}

}
