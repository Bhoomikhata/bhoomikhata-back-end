package com.example.bhoomikhata.service;

<<<<<<< HEAD
import java.util.ArrayList;
=======
import java.time.OffsetDateTime;
>>>>>>> Manish
import java.util.List;
import java.util.Optional;

import com.example.bhoomikhata.model.BussinesModel;
import com.example.bhoomikhata.model.ContactPaymentAccount;
import com.example.bhoomikhata.model.RazPayContactFundAccount;
import com.example.bhoomikhata.model.RazPayContactRequestDto;
import com.example.bhoomikhata.model.RazPayContactResponseModel;
import com.example.bhoomikhata.repository.ProfileRepository;
import com.example.bhoomikhata.utils.Utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfileService {

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private RazPayContactFundAccountService razPayContactService;

<<<<<<< HEAD
	@Autowired
	public ProfileService(ProfileRepository profileRepository) {
		this.profileRepository = profileRepository;
	}
=======
	/*
	 * @Autowired public ProfileService(ProfileRepository profileRepository) {
	 * this.profileRepository = profileRepository; }
	 */
>>>>>>> Manish

	public Optional<BussinesModel> getBusinessProfile(String bussinessId) {

		return profileRepository.findById(bussinessId);
	}

	public BussinesModel createOrUpdateBussinesProfile(BussinesModel bussinesModel) {
		bussinesModel.setId(bussinesModel.getMobileNumber());

<<<<<<< HEAD
		// get or create Contact and  Get fund Account details on RazorPay
=======
		// get or create Contact and Get fund Account details on RazorPay
>>>>>>> Manish
		if (bussinesModel != null) {
			bussinesModel = razPayContactService.getOrCreateRazPayContactAndAccountDetails(bussinesModel);
		}

<<<<<<< HEAD
		return profileRepository
				.save(Utility.setCreatedAndUpdatedDate(Utility.setCreatedAndUpdatedDate(bussinesModel)));
	}

	public Boolean deleteBussinesProfile(String bussinessId) {
		try {
			profileRepository.deleteById(bussinessId);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

=======
		if (profileRepository.findById(bussinesModel.getMobileNumber()).isEmpty())
			bussinesModel.setIsKycComplete(false);

		return profileRepository
				.save(Utility.setCreatedAndUpdatedDate(Utility.setCreatedAndUpdatedDate(bussinesModel)));
	}

	public BussinesModel patchBusinessProfile(BussinesModel bModel) {
		BussinesModel oldBModel = null;
		BussinesModel updatedBModel = new BussinesModel();

		if (profileRepository.findById(bModel.getId()).isEmpty())
			return null;

		oldBModel = profileRepository.findById(bModel.getId()).get();
		updatedBModel.setId(oldBModel.getId());
		updatedBModel.setImage(oldBModel.getImage());
		updatedBModel.setImageGallery(oldBModel.getImageGallery());
		updatedBModel.setKyc(oldBModel.getKyc());
		updatedBModel.setIsKycComplete(oldBModel.getIsKycComplete());
		updatedBModel.setStaffs(oldBModel.getStaffs());
		updatedBModel.setRazorPayContactDetails(oldBModel.getRazorPayContactDetails());
		updatedBModel.setRazorpayContactId(oldBModel.getRazorpayContactId());
		updatedBModel.setRazorpayPaymentAccountDetails(oldBModel.getRazorpayPaymentAccountDetails());
		updatedBModel.setStrength(oldBModel.getStrength());
		updatedBModel.setCreatedDate(oldBModel.getCreatedDate());
		updatedBModel.setCountryCode(oldBModel.getCountryCode());
		updatedBModel.setRatings(oldBModel.getRatings());
		updatedBModel.setMobileNumber(oldBModel.getMobileNumber());

		updatedBModel.setUpdatedDate(OffsetDateTime.now());

		updatedBModel
				.setAdressModel(bModel.getAdressModel() != null ? bModel.getAdressModel() : oldBModel.getAdressModel());
		updatedBModel.setEmailId(bModel.getEmailId() != null ? bModel.getEmailId() : oldBModel.getEmailId());
		// to be updated in razorPay......

		updatedBModel.setBussinessName(
				bModel.getBussinessName() != null ? bModel.getBussinessName() : oldBModel.getBussinessName());
		updatedBModel.setName(bModel.getName() != null ? bModel.getName() : oldBModel.getName());
		updatedBModel.setCategory(bModel.getCategory() != null ? bModel.getCategory() : oldBModel.getCategory());
		updatedBModel.setBussinessType(
				bModel.getBussinessType() != null ? bModel.getBussinessType() : oldBModel.getBussinessType());
		updatedBModel.setGstin(bModel.getGstin() != null ? bModel.getGstin() : oldBModel.getGstin());

		BussinesModel bm = profileRepository.save(updatedBModel);
		bm.setImage(null);
		bm.setImageGallery(null);
		bm.setKyc(null);

		return bm;
	}

	public Boolean deleteBussinesProfile(String bussinessId) {
		try {
			profileRepository.deleteById(bussinessId);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

>>>>>>> Manish
	public Boolean deleteAllBussinesProfile() {
		try {
			profileRepository.deleteAll();
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}

	}

	public List<BussinesModel> getAllBusinessProfile() {

		return profileRepository.findAll();
	}

}
