package com.example.bhoomikhata.config;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;

@Configuration
public class FireBaseConfig {

	@Value("${firebase.service.accountkey}")
	private String serviceAccountKey;

	@Bean
	public FirebaseMessaging firebaseMessaging() throws IOException {
		InputStream refreshToken;
		refreshToken = new ClassPathResource(serviceAccountKey).getInputStream();
		FirebaseOptions options = FirebaseOptions.builder().setCredentials(GoogleCredentials.fromStream(refreshToken))
				.build();
		FirebaseApp app = FirebaseApp.initializeApp(options);
		return FirebaseMessaging.getInstance(app);

	}

}
