package com.example.bhoomikhata.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;


@Configuration
public class SwaggerConfiguration {

	@Bean
	OpenAPI bhoomiKhataAPI() {
		return new OpenAPI()
				.info(new Info().title("BHOOMIKHATA REST API").description("BHOOMIKHATA REST API UNDER DEVELOPMENT")
						.version("v2.1")
						.license(new License().name("Terms of service")
								.url("https://bhoomikhata.com/privacy_policy.html"))
						.contact(new Contact().name("BhoomiKhata Support | Prashant | Manish | Rasum | Razum")
								.email("bhoomikhata@gmail.com")))
				.externalDocs(new ExternalDocumentation().description("Copyright © Bhoomikhata")
						.url("https://bhoomikhata.com/privacy_policy.html"));
	}
}