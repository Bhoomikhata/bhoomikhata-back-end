package com.example.bhoomikhata.model;

public enum PaymentStatus {

    PURPLE("purple"), RED("red"), GREEN("green"), YELLOW("yellow");

    private String color;

    PaymentStatus(String color) {
        this.color = color;
    }

    public String value() {
        return color;
    }
}
