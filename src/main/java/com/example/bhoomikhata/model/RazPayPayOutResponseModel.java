package com.example.bhoomikhata.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazPayPayOutResponseModel {
	private String id;
	private String reference_id;
	private String entity;
	private String fund_account_id;
	private Integer amount;
	private String currency;
	private String fees;
	private String tax;
	private String status;
	private String mode;
	private String utr;
	private String purpose;
	private Integer created_at;
	private PayOutStatusDetails status_details;
	private String narration;
}
