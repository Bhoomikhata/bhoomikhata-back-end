package com.example.bhoomikhata.model;

import java.time.OffsetDateTime;

import lombok.Data;

@Data
public class SubUserModel {
    String id;
    String name;
    String mobile;
    UserType userType;
    String bussinessId;
    OffsetDateTime createdDate;
    OffsetDateTime updatedDate;
}
