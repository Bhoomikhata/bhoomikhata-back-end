package com.example.bhoomikhata.model;

import lombok.Data;

@Data
public class ErrorResponse {
    String errorCode;
    String errorResponse;
}
