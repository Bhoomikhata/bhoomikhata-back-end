package com.example.bhoomikhata.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "contacts")
public class ContactModel extends ContactDetails {
    @Id
    private String id;
    private String name;
    private String bussinessId;
}
