package com.example.bhoomikhata.model;

import java.time.OffsetDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document (collection = "po_invoice")
public class InvoiceAndPOModel {
	@Id
	private String purchaseOrderId;	
	private String invoiceId;	
	private OffsetDateTime createdDate;
}
