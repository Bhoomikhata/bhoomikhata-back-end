package com.example.bhoomikhata.model;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazPayContactResponseModel {
	private String id;
	private String entity;
	private String name;
	private String contact;
	private String email;
	private String type;
	private Boolean active;
	private Integer created_at;
	private Map<String, String> notes;
}
