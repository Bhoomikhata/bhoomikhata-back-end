package com.example.bhoomikhata.model;

import java.time.OffsetDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "cashbackOffers")
public class CashbackOffers extends BaseOfferModel {
    @Id
    private String id;
    private OffsetDateTime startDate;
    private OffsetDateTime endDate;
}
