package com.example.bhoomikhata.model;

import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "files_store_collection")
public class FilesStorageModel {
	@Id
	private String id;
	private String businessId;
	private String customerId;
	private Binary fileData;
	private String fileName;
	private String fileType;
	private Long fileSize;
	private FilesCategory filesCategory;
	private String filesSubCategory;
	private String transactionId;
	private Boolean isForSupport;
}
