package com.example.bhoomikhata.model;

import java.time.OffsetDateTime;
import java.util.TreeMap;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "transactions")
public class TransactionModel extends BaseModel {
	@Id
	private String id;
	private OffsetDateTime dueDate;
	private Integer amount;
	private Boolean isCredit;
	private String type;
	private String payeeId;
	private String payerId;
	private Integer dueDateDuration;
	private Integer transactionTypeId;
	private TransactionType transactionType;
	private String documentFile;
	private Boolean isInSync;
	private Boolean isSMSDeliverd;
	private String descriptons;
	private String messagedByName;
	private TreeMap<String, Integer> transactionEditHistory;
}
