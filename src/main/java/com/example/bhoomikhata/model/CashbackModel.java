package com.example.bhoomikhata.model;

import java.time.OffsetDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "cashback")
public class CashbackModel extends BaseModel {
    private Integer amount;
    private TransactionStatus transactionStatus;
    private OffsetDateTime expiry;
    private String remark;
}
