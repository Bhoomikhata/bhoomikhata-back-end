package com.example.bhoomikhata.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazPayFundAccountsByContactResponse {
	private String entity;
	private Integer count;
	private List<RazPayFundAccountResponseModel> items;
}
