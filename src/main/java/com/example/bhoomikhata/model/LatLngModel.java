package com.example.bhoomikhata.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LatLngModel {

    private double latitude;
    private double longitude;
}
