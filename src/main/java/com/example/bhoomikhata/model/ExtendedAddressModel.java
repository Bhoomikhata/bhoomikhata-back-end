package com.example.bhoomikhata.model;

import lombok.Data;

@Data
public class ExtendedAddressModel extends AdressModel {
    String dueDate;
    Integer dueAmount;
    PaymentStatus colour;
}
