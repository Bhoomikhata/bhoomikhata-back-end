package com.example.bhoomikhata.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetails implements Serializable {
	private String itemId;
	private Integer quantity;
	private Double unitPrice;
}
