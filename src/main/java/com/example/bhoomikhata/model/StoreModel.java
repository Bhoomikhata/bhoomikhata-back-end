package com.example.bhoomikhata.model;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "store")
public class StoreModel extends BaseModel {
    private String itemId;
    private String categoryId;
    private String seller;
    private String couponCode;
    private String coinsNeeded;
    private String maxRetailPrice;
    private String offerPrice;
    private String productImage;
}
