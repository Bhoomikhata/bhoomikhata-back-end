package com.example.bhoomikhata.model;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazPayContactRequestModel {
	private String name;
	private String email;
	private String contact;
	private String type;
	private String reference_id;
	private Map<String, String> notes;
}
