package com.example.bhoomikhata.model;

import lombok.Data;

@Data
public class OfferGenerationResponse extends ErrorResponse {
    private Integer totalCoin;
    private Integer offerCoins;
    private Integer totalCashBack;
    private Integer offerCashBack;
    private Boolean isJackpot;
    private String couponCode;
    private String couponRemarks;
    private String cashBackRemarks;
}
