package com.example.bhoomikhata.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "firbase_bid_token")
public class BusinessIdTokenModel {
	@Id
	private String businessId;
	private String tokenId;
}
