package com.example.bhoomikhata.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RazPayPaymentResponseModel {
	public static enum status {
		SUCCESS, FAILED
	};

	private String txStatus;
	private String statusDetails;
}
