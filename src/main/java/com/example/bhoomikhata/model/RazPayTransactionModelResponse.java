package com.example.bhoomikhata.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "customer_payment_transactions")
public class RazPayTransactionModelResponse {
	@Id
	private String trxRefId;
	private String businessId;
	private String trxStatus;
	private List<RazPayTrxResult> trxStatusDetails;
	private Integer amount;
	private String toName;
	private String toAccount;
	private String fromName;
	private String fromAccount;
	private String timeDate;
}
