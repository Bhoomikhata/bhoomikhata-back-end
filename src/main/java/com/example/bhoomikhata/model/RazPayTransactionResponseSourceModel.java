package com.example.bhoomikhata.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazPayTransactionResponseSourceModel {
	private String id;
	private String entity;
	private String fund_account_id;
	private String amount;
	private String status;
	private Integer fees;
	private Integer tax;
	private String mode;

}
