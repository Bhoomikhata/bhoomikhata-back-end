package com.example.bhoomikhata.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazPayCardPaymentModel {
	private String id;
	private String entity;
	private String name;
	private String last4;
	private String network;
	private String type;
	private String issuer;
	private Boolean international;
	private Boolean emi;
	private String sub_type;

}
