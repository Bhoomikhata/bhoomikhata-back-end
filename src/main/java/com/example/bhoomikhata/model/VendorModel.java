package com.example.bhoomikhata.model;

import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "vendorModel")
public class VendorModel {
    private String id;
    private String name;
    private String vendorUrl;
    private String checkoutUrl;
    private Map<String, Boolean> couponStatusMap;
}
