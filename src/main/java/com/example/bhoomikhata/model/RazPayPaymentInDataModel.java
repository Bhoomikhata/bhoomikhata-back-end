package com.example.bhoomikhata.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RazPayPaymentInDataModel {
	private String razorpay_payment_id;
	private String razorpay_order_id;
	private String razorpay_signature;
	private String payToFundAccountId;
}
