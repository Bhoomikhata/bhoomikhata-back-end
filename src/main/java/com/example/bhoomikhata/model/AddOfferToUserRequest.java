package com.example.bhoomikhata.model;

import lombok.Data;

@Data
public class AddOfferToUserRequest {
    String offerId;
    ConversionTypes offerType;
    String bussinessId;
}
