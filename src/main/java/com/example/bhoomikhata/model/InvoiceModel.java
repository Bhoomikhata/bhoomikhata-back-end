package com.example.bhoomikhata.model;

import java.time.OffsetDateTime;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "invoice_data")
public class InvoiceModel {
	@Id
	private String invoiceId;
	private OffsetDateTime createdDate;
	// private String GSTIN;
	private String billTo;
	//private String billToAddress;
	private String billToContact;
	private String billFrom;
	//private String billFromAddress;
	private String billFromContact;
	private List<InvoiceItemModel> itemList;
	private Integer totalQuantity;
	private Double subTotal;

}
