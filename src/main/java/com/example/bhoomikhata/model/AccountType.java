package com.example.bhoomikhata.model;

public enum AccountType {
    UPI("upi"), DEBITCARD("debit-card"), CREDITCARD("credit-card"), BANKACCOUNT("bank-account");

    String account;

    AccountType(String account) {
        this.account = account;
    }

    public String value() {
        return account;
    }
}
