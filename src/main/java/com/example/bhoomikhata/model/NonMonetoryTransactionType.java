package com.example.bhoomikhata.model;

public enum NonMonetoryTransactionType {

    INVOICE("INVOICE"), PO("PO"), RETURN("RETURN");

    String value;

    NonMonetoryTransactionType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
