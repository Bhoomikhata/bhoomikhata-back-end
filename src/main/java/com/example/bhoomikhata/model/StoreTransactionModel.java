package com.example.bhoomikhata.model;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "storeTransaction")
public class StoreTransactionModel extends BaseModel {
    private String transactionId;
    private String customerId;
    private String couponCode;
    private String vendorId;
    private String itemId;
}
