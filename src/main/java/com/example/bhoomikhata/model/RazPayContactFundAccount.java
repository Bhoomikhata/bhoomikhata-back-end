package com.example.bhoomikhata.model;

import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "razorpay_contact_fund_accounts")
public class RazPayContactFundAccount {
	@Id
	private String mobileNumber;
	private RazPayContactResponseModel contactDetails;
	private List<RazPayFundAccountResponseModel> fundAccounts;
	private Boolean isDefaultFundAccountSet;
}
