package com.example.bhoomikhata.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RazPayOrderPaymentFundAccount {
	private String fundAccountId;
	private String accountType;
	private String accountDesc;
}
