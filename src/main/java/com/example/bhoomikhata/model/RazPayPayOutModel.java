package com.example.bhoomikhata.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RazPayPayOutModel {
	private String account_number;
	private String fund_account_id;
	private Integer amount;
	private String currency;
	private String mode;
	private String purpose;
	private String reference_id;
}
