package com.example.bhoomikhata.model;

import lombok.Data;

@Data
public class GenerateRewardRequest {
    String bussinessid;
    String contactId;
    ConversionTypes conversionTypes;
    String offerId;
}
