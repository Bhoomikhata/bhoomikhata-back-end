package com.example.bhoomikhata.model;

import lombok.Data;

@Data
public class GenerateCoinRequest {
    String bussinessid;
    String contactId;
    Integer amount;
    String offerId;
}
