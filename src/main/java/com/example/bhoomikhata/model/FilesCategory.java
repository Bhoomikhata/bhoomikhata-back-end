package com.example.bhoomikhata.model;

public enum FilesCategory {
	PROFILE_IMAGE, PROFILE_GALLERY_IMAGE, PROFILE_KYC, CHAT_INVOICE;
}
