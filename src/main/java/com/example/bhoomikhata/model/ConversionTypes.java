package com.example.bhoomikhata.model;

public enum ConversionTypes {
    COIN("coin"), COUPON("coupon"), CASHBACKS("cashbacks");

    String value;

    ConversionTypes(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
