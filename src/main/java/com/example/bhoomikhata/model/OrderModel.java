package com.example.bhoomikhata.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@Document(collection = "order")
@AllArgsConstructor
@NoArgsConstructor
public class OrderModel implements Serializable {
	@Id
	private String orderId;
	private String orderDate;
	private String orderUpdateDate;
	private String customerId;
	private String bussinessId;
	private OrderStatus orderStatus;
	private OrderType orderType;
	private List<OrderDetails> orderDetails;

}
