package com.example.bhoomikhata.model;

import lombok.Data;

@Data
public class BankAccountModel {

    private String accountNumber;
    private String ifsc;
    private BankAccountType bankAccountType;
    private String accountHoldername;

}
