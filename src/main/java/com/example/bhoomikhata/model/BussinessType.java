package com.example.bhoomikhata.model;

public enum BussinessType {

    RETAILER("retailer"), WHOLESALER("wholesaler"), DISTRIBUTOR("distributor"), SERVICES("services"),
    MANUFACTURER("manufacturer"), OTHERS("others");

    String value;

    BussinessType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
