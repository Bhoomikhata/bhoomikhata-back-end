package com.example.bhoomikhata.model;

import lombok.Data;

@Data
public class AccessKeyModel {
    private String grantType;
    private String username;
    private String password;
    private String scope;
    private String clientId;
    private String clientSecret;
}
