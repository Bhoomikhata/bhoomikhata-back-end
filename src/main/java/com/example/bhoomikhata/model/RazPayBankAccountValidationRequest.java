package com.example.bhoomikhata.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RazPayBankAccountValidationRequest {
	private String account_number;
	private RazPayFundAccount fund_account;
	private Integer amount;
	private String currency;

}
