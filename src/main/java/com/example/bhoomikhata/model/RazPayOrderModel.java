package com.example.bhoomikhata.model;

import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "razorpay_order")
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazPayOrderModel {
	@Id
	private String id;
	private String entity;
	private Integer amount;
	private Integer amount_paid;
	private Integer amount_due;
	private String currency;
	private String receipt;
	private String status;
	private Integer attempts;
	private Map<String, String> notes;
	private Integer created_at;

}
