package com.example.bhoomikhata.model;

import org.bson.types.Binary;

import lombok.Data;

@Data
public class StaffModel extends BaseModel {
    private String name;
    private Binary image;
    private String id;
}
