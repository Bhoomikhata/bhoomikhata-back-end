package com.example.bhoomikhata.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document (collection = "rate_list")
public class RateListModel {
	@Id
	private String itemId;
	private String businessId;
	private String itemName;
	private String hsnCode;
	private Double rate;
	private Double tax;
	private Double discount;

}
