package com.example.bhoomikhata.model;

import lombok.Data;

@Data
public class NonMonetoryTransaction {
    private String id;
    private NonMonetoryTransactionType type;
}
