package com.example.bhoomikhata.model;

import lombok.Data;

@Data
public class AdressModel {
    String id;
    String label;
    String postalAddress;
    String longitude;
    String lattitude;
}
