package com.example.bhoomikhata.model;

public enum MessageStatus {
    RECEIVED, DELIVERED
}
