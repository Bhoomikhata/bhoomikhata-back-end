package com.example.bhoomikhata.model;

import java.time.OffsetDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "offer")
public class OfferModel {

    @Id
    private String offerId;
    private String offerName;
    private ConversionTypes conversionTypes;
    private Integer coinGenerated;
    private Integer coinRequired;
    private Float coinFormula;
    private Float cashbackFormula;
    private String couponRemarks;
    private String couponCode;
    private Integer couponValidity;
    private Integer cashBackValidity;
    private String cashBackRemarks;
    OffsetDateTime createdDate;
    OffsetDateTime updatedDate;
}
