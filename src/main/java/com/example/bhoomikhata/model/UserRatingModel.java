package com.example.bhoomikhata.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "user_ratings")
public class UserRatingModel {
	@Id
	private String ratingId;
	private String ratedTobusinessId;
	private String ratedByBusinessId;
	private Integer rating;
}
