package com.example.bhoomikhata.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "coinOffers")
public class CoinOffers extends BaseOfferModel {

    @Id
    private String id;
    private Boolean isFixed;
}
