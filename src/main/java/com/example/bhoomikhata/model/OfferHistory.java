package com.example.bhoomikhata.model;

import java.time.OffsetDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "offerHistory")
public class OfferHistory {
    @Id
    String id;
    String bussinessId;
    String offerId;
    String issuedAmount;
    OffsetDateTime issueDate;
}
