package com.example.bhoomikhata.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RazPayOrderDtoResponse {
	private String razorPayOrderId;
	private String transactionRefId;
	private Integer amount;
	private String fromBId;
	private String toCustomerId;
	private String toRazorPayContactId;
	private String toName;
	private List<RazPayOrderPaymentFundAccount> paymentAccountOptions;
	private String messagesToUser;
}
