package com.example.bhoomikhata.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRating {	
	private Double rating;
	private Integer ratedByUsers;
	private Integer ratedByYou;
}
