package com.example.bhoomikhata.model;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazPayRefundResponseModel {
	private String id;
	private String entity;
	private Integer amount;
	private String currency;
	private String payment_id;
	private String receipt;
	private String created_at;
	private String status;
	private String speed_processed;
	private String speed_requested;

}
