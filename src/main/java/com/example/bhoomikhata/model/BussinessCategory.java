package com.example.bhoomikhata.model;

public enum BussinessCategory {

    KIRANA("kirana"), MEDICAL("medical"), APPAREL("apparel"), ELECTRONICS("electronics"), MOBILE("mobile"),
    FINANCIALSERVICES("financialservices"), INSURANCE("insurance"), DIGITAL("digital"), AGRICULTURE("agriculture"),
    EDUCATION("education"), COMPUTER("computer"), TOURANDTRAVEL("tourandtravel"), OTHER("other");

    String value;

    BussinessCategory(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
