package com.example.bhoomikhata.model;

import java.time.OffsetDateTime;
import java.util.List;
import lombok.Data;

@Data
public class ContactDetails {
	private String mobileNumber;
	private String phoneNumber;
	private String emailId;
	private String countryCode;
	private AdressModel adressModel;
	OffsetDateTime createdDate;
	OffsetDateTime updatedDate;
	private String image;
	private List<String> imageGallery;
	private int strength;
	private String bussinessName;
	BussinessCategory category;
	BussinessType bussinessType;
	private String gstin;
	private String razorpayContactId;
	private RazPayContactResponseModel razorPayContactDetails;
	private List<ContactPaymentAccount> razorpayPaymentAccountDetails;
	List<DocModel> kyc;
	private Boolean isKycComplete;
	private UserRating ratings;
	List<StaffModel> staffs;
}
