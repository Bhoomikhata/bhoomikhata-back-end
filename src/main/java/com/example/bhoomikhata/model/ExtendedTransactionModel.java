package com.example.bhoomikhata.model;

import java.time.OffsetDateTime;
import java.util.Map;
import java.util.Set;

import lombok.Data;

@Data
public class ExtendedTransactionModel {
    private String id;
    private String name;
    private Integer overdueAmount;
    private String dueDate;
    private String countryCode;
    private String postalAddress;
    private Map<OffsetDateTime, Set<TransactionModel>> transactions;
}
