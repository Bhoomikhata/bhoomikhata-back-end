package com.example.bhoomikhata.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RazPayFundAccountStatusDetails {
	private Boolean isVarified;
	private Boolean isDefault;
	private RazPayFundAccountResponseModel fundAccountDetails;
}
