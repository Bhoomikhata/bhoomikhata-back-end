package com.example.bhoomikhata.model;

public enum KycStatus {
	CREATED, PENDING, VERIFIED, FAILED;
}
