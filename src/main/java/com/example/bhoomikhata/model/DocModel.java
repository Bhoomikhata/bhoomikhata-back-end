package com.example.bhoomikhata.model;

import lombok.Data;

@Data
public class DocModel {	
    private String docId;
    private DocType docType;
    private KycStatus kycStatus;
}
