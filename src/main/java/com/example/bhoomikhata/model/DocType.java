package com.example.bhoomikhata.model;

public enum DocType {
    PAN("paan-card"), ADHAR("adhar-card"), ELECTRICITY("electricity-bill"), GAS("gas-bill"), PASSPORT("passport");

    String doc;

    DocType(String doc) {
        this.doc = doc;
    }

    public String value() {
        return doc;
    }

}
