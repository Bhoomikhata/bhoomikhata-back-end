package com.example.bhoomikhata.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "otp")
public class OTPModel {
    @Id
    private String mobileNumber;
    private String countryCode;
    private String otp;
    private Long createdDate;
    private Long updatedDate;
    private Integer validity;
}
