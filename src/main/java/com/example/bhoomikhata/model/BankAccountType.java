package com.example.bhoomikhata.model;

public enum BankAccountType {
    CURRENT("current"), SAVINGS("savings");

    String value;

    BankAccountType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
