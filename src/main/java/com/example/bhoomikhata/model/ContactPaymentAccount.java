package com.example.bhoomikhata.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactPaymentAccount {
	
	private String fundAccountId;
	private String validationId;
	private String accountDescription;
	private Boolean isValidationRequested;
	private AccStatus validationStatus;
	private Boolean isDefault;
	private String accountType;
	private Boolean isBankOrUpiActive;
	private Boolean isFundAccountActive;
	private RazPayBankAccountResponseModel bankDetails;
	private RazPayUPIModelResponse upiDetails;
}
