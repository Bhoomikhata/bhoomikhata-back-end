package com.example.bhoomikhata.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazPayFundAccountValidationResponse {
	private String id;
	private String entity;
	private RazPayFundAccountResponseModel fund_account;
	private String status;
	private Integer amount;
	private String currency;
	private RazPayAccountValidationResult results;
	private Integer created_at;
	private String utr;

}
