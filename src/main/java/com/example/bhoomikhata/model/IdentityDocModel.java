package com.example.bhoomikhata.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "documents")
public class IdentityDocModel extends BaseModel {
    @Id
    private String id;
    private String docType;
    private String docRef;
}
