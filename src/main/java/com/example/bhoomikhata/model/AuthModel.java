package com.example.bhoomikhata.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "authentication")
public class AuthModel {
    @Id
    private String id;
    private String password;
}
