package com.example.bhoomikhata.model;

import lombok.Data;

import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "paymentMode")
public class PaymentModeModel extends BaseModel {
    private String id;
    private AccountType paymentModeType;
    private String accountNumber;
    private String name;
    private String ifsc;
    private String accountType;
}
