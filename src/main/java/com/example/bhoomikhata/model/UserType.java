package com.example.bhoomikhata.model;

public enum UserType {
    SUPERADMIN("super-admin"), ADMIN("admin"), SALES("sales");

    String user;

    UserType(String user) {
        this.user = user;
    }

    public String value() {
        return user;
    }

}
