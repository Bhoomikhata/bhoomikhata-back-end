package com.example.bhoomikhata.model;

import java.util.List;

import lombok.Data;

@Data
public class OrderDto {
	private String customerId;
	private String bussinessId;
	private List<OrderDetails> orderDetails;	
}
