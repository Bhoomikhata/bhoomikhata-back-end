package com.example.bhoomikhata.model;

import java.time.OffsetDateTime;

import lombok.Data;

@Data
public class BaseOfferModel {
    private String offerName;
    private Float formula;
    private String remarks;
    private Long expiryDuration;
    private OffsetDateTime expiryDate;
    OffsetDateTime createdDate;
    OffsetDateTime updatedDate;
}
