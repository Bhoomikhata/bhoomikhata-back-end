package com.example.bhoomikhata.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RazPayTrxResult {
	private Boolean status;
	private String details;

	public static RazPayTrxResult tXResult(Boolean status, String details) {
		return new RazPayTrxResult(status, details);
	}

}
