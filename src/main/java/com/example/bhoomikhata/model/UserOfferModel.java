package com.example.bhoomikhata.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "userOfferModel")
public class UserOfferModel {
    @Id
    String bussinessId;
    List<String> whiteListedCashBackOffers;
    List<String> whiteListedCoinOffers;
    List<String> whiteListedCouponOffers;
    List<String> blackListedOffers;
}
