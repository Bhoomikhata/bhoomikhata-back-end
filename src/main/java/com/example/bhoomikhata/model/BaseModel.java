package com.example.bhoomikhata.model;

import java.time.OffsetDateTime;

import lombok.Data;

@Data
public class BaseModel {
    private String bussinessId;
    private String contactId;
    OffsetDateTime createdDate;
    OffsetDateTime updatedDate;
}
