package com.example.bhoomikhata.model;

public enum TransactionType {

    OTHER("order"), INVOICE("invoice"), PURCHASEORDER("purchaseorder");

    String value;

    TransactionType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
