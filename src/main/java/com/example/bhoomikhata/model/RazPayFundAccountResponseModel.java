package com.example.bhoomikhata.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazPayFundAccountResponseModel {
	private String id;
	private String entity;
	private String contact_id;
	private String account_type;
	private RazPayBankAccountResponseModel bank_account;
	private RazPayUPIModelResponse vpa;
	private Boolean active;
	private Integer created_at;
	private RazPayFundAccountValidationResponse accountValidation;
}
