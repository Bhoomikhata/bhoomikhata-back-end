package com.example.bhoomikhata.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatNotificationModel {
	private String title;
	private String messageBody;
	private String imageUrl;
	private ChatNotificationDataModel fromAndTo;
}
