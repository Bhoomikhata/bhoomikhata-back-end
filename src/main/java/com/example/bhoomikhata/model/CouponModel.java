package com.example.bhoomikhata.model;

import java.time.OffsetDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "coupon")
public class CouponModel extends BaseModel {
    private String couponCode;
    private TransactionStatus status;
    private Boolean isJackpot;
    private OffsetDateTime couponExpiryDate;
    private String remarks;
}
