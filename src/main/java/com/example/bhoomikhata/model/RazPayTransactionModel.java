package com.example.bhoomikhata.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "razorpay_transactions")
public class RazPayTransactionModel {	
	
	@Id
	private String transactionRefId;
	private String transactionStart;
	private String fromBid;
	private String toBid;
	private String razorPayOrderId;
	private String razorPayOrderStatus;
	private RazPayOrderModel paymentOrder;
	
	
	
	private String razorPayPaymentId;
	private Integer razorPayPayInAmount;
	private String razorPayPaymentMethod;
	private String razorPayPaymentStatus;
	private RazPayPaymentModel paymentInResponse;
	
	private String razorPayPaymentRefundId;
	private String razorPayPaymentRefundStatus;
	
	private String razorPayToContactId;
	private String razorPayToFundAccountId;
	private RazPayFundAccountResponseModel paymentToFundAccount;
	
	private String razorPaySignature;
	private Boolean razorPaySinatureVarified;
	private List<String> statusErrorLog;
	
	private String razorPayPayOutId;
	private String razorPayPayOutStatus;
	private Integer razorPayPayOutAmount;
	
	private String transactionEnd;
}
