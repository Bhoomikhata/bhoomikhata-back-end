package com.example.bhoomikhata.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class InvoiceItemModel {
	private Integer serialNo;
	private String itemName;
	private String hsnCode;
	private Integer quantity;
	private Double unitPrice;
	private Double amount;	
}
