package com.example.bhoomikhata.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "bussiness")
public class BussinesModel extends ContactDetails {
    @Id
    String id;
    String name;
}
