package com.example.bhoomikhata.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document("couponOffer")
public class CouponOffers extends BaseOfferModel {
    @Id
    private String id;
    private String vendorId;
    private Boolean isJackpot;
    private String couponCode;
}
