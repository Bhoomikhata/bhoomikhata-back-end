package com.example.bhoomikhata.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazPayPaymentModel {
	private String id;
	private String entity;
	private Integer amount;
	private String currency;
	private String status;
	private String order_id;
	private String method;
	private String card_id;
	private RazPayCardPaymentModel card;
	private String bank;
	private String wallet;
	private String vpa;
	private Integer fee;
	private Integer tax;
	private String description;
	private String international;
	private String refund_status;
	private String amount_refunded;
	private Boolean captured;
	private String email;
	private String contact;
	private String error_code;
	private String error_description;
	private String error_source;
	private String error_step;
	private String error_reason;
	private String created_at;

}
