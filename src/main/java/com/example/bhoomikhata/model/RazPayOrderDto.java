package com.example.bhoomikhata.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RazPayOrderDto {
	private Integer amount;
	private String fromBusinessId;
	private String toCustomerId;
}
