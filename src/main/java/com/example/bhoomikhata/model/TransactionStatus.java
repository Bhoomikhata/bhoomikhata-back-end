package com.example.bhoomikhata.model;

public enum TransactionStatus {
    SUCCESS("success"), FAILURE("failure"), ISSUED("issued");

    String status;

    public String value() {
        return status;
    }

    TransactionStatus(String status) {
        this.status = status;
    }

}
