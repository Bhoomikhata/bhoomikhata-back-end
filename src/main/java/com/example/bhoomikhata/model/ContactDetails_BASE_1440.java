package com.example.bhoomikhata.model;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;

import org.bson.types.Binary;

import lombok.Data;

@Data
public class ContactDetails {
    private String mobileNumber;
    private String phoneNumber;
    private String emailId;
    private String countryCode;
    private AdressModel adressModel;
    OffsetDateTime createdDate;
    OffsetDateTime updatedDate;
    private Binary image;
    private int strength;
    private String bussinessName;
    BussinessCategory category;
    BussinessType bussinessType;
    private String gstin;
    private String upiId;
    private Map<String, String> fundAccounts;
    BankAccountModel bankAccount;
    DocModel kyc;
    List<StaffModel> staffs;
}
