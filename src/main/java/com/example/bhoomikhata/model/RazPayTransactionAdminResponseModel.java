package com.example.bhoomikhata.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RazPayTransactionAdminResponseModel {
	private String id;
	private String entity;
	private String account_number;
	private Integer amount;
	private Integer credit;
	private Integer debit;
	private Integer balance;
	private RazPayTransactionAdminResponseSourceModel source;
	private String created_at;
	
}
